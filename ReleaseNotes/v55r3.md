2024-03-18 Moore v55r3
===

This version uses
Allen [v4r3](../../../../Allen/-/tags/v4r3),
Rec [v36r3](../../../../Rec/-/tags/v36r3),
Lbcom [v35r3](../../../../Lbcom/-/tags/v35r3),
LHCb [v55r3](../../../../LHCb/-/tags/v55r3),
Detector [v1r28](../../../../Detector/-/tags/v1r28),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `master` branch.
Built relative to Moore [v55r2](/../../tags/v55r2), with the following changes:

### New features ~"new feature"

- ~Tracking | New ghost prob inference and training infrastructure, !3026 (@mveghel)
- Add velo lumi counters to HLT2, !2976 (@dcraik)
- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- Upstream project highlights :star:


### Enhancements ~enhancement

- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Upstream project highlights :star:


### Documentation ~Documentation

### Other

- ~selection | BnoC HLT2 lines, !3052 (@zewen)
- ~hlt2 | Append "Full" to the name of all the full-stream QEE lines, !2743 (@rjhunter)
- ~hlt2 | Add Bc->3hmumu lines, !2998 (@rmwillia)
- ~hlt2 | RD: implement cone and vertex isolation, FT information and PV track persistence in B24l; rate reductions in B2ll, !2948 (@jagoodin) [#644]
- ~hlt2 | Fixes to lines for strange decays, !2442 (@mramospe) [#585,#597]
- ~"PV finding" | Test for Updated VertexCompare, !3118 (@mgiza)
- ~"PV finding" | Extend PatPVFuture to 3D seeding (beamSpot independent), !2686 (@mgiza) [Rec#380]
- ~PID | Tightened selections and prescale for the DstToD0Pi_DOToPiPiPi PID line, !3144 (@mbovill)
- ~PID | Remove redundant lines for electronPID, !3090 (@pvidrier)
- Update References for: Rec!3556, Moore!2665 based on lhcb-master-mr/11108, !3178 (@lhcbsoft)
- Update References for: Allen!1463 based on lhcb-master-mr/11090, !3175 (@lhcbsoft)
- Update References for: Allen!1494 based on lhcb-master-mr/11080, !3173 (@lhcbsoft)
- Update References for: Allen!1491 based on lhcb-master-mr/11107, !3171 (@lhcbsoft)
- Reduce the number of combinations the EW_Xic0ToPKKPi and EW_LcToPKPi by adding strict pt cut, !3140 (@jiuzhao)
- Global fixes to the bandwidth of the RD stream, !3053 (@mramospe) [#710]
- Add track selection for Velo hit efficiency options, !2665 (@mwaterla)
- Update References for: Moore!3081, MooreOnline!293 based on lhcb-master-mr/11056, !3166 (@lhcbsoft)
- Update References for: Allen!1442 based on lhcb-master-mr/11035, !3162 (@lhcbsoft)
- Update References for: LHCb!4305, Rec!3729, Moore!3026 based on lhcb-master-mr/11019, !3160 (@lhcbsoft)
- Update References for: Allen!1486 based on lhcb-master-mr/10980, !3153 (@lhcbsoft)
- B2OC: improving the selections in a few problematic builders, !3138 (@abertoli)
- [QEE] prompt_dielectron_with_brem make BPVFDCHI2 cut optional, !3134 (@lugrazet)
- Update charm codeowners, !3125 (@tpajero)
- Include mass monitors properly into recomon, !3081 (@tmombach)
- Update References for: Allen!1459 based on lhcb-master-mr/10959, !3150 (@lhcbsoft)
- Update References for: Allen!1431, Moore!3075 based on lhcb-master-mr/10925, !3145 (@lhcbsoft)
- Update References for: Rec!3612, Moore!2686 based on lhcb-master-mr/10914, !3139 (@lhcbsoft)
- Fix refs missed in !2928, !3136 (@lhcbsoft)
- Update file hlt2_with_hlt1_filtering.py, !3075 (@tevans)
- SL: updates on HbToHcTauNu_TauToPiPiPiNu.py, !3023 (@abrossag)
- Added basic reference-based test for BeamSpotMonitor algorithm, !2985 (@spradlin)
- Switching PersistReco off + add particles for isolation variables  + releasing tight IPCHI2 cut on D(s)+ candidates, !2741 (@smaccoli)
- Renaming new HLT and Spruce line: BsToDsstMuNu_DsstToDsGamma_DsToKKPi_Gamma2EE, !3127 (@cacochat)
- Add test option to disable GEC in standalone reco, !3121 (@gunther)
- Reduce DiMuonNoIP Rate, !3086 (@kaaricha)
- RD: btostautau non-LFV lines, !3016 (@mbirch)
