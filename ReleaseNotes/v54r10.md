2023-06-22 Moore v54r10
===

This version uses Allen [v3r10](../../../../Allen/-/tags/v3r10),
Rec [v35r10](../../../../Rec/-/tags/v35r10),
Lbcom [v34r10](../../../../Lbcom/-/tags/v34r10),
LHCb [v54r10](../../../../LHCb/-/tags/v54r10),
Detector [v1r14](../../../../Detector/-/tags/v1r14),
Gaudi [v36r14](../../../../Gaudi/-/tags/v36r14) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Moore [v54r9](/../../tags/v54r9), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- ~Monitoring | Default monitoring of filtered combiners, !2423 (@mstahl)
- Do not explicitly configure DecReportsDecoder - use get_decreports instead, !2392 (@graven)
- Upstream project highlights :star:


### Enhancements ~enhancement

- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Build | Fix sanitizer build, !2437 (@rmatev)
- ~Build | Functor cache for running online, !2333 (@mstahl)
- Fix test_hlt2_check_packed_data_checksums (follow !2334), !2450 (@rmatev)
- Fix typo in Hlt2Conf.profile_config test, !2441 (@rmatev)
- Relax limit of HLT 2 configuration timing test, !2428 (@rmatev)
- Move hlt2_pp_commissioning from MooreOnline and tune it, !2334 (@mstahl)
- Upstream project highlights :star:


### Documentation ~Documentation

- Update docs for MooreAnalysis!124, !2419 (@rjhunter)
- Docs update for MooreAnalysis!122 and MooreAnalysis!118, !2326 (@rjhunter)
### Other

- ~selection | Change BnoC flavour tagging to use GetFlavourTaggingParticles, !2175 (@mmonk)
- Clean up Sprucing options, !2446 (@nskidmor)
- Fix typo in docs, !2440 (@rmatev)
- Delete obsolete unit test, !2426 (@nskidmor)
- Change name of parameter to match Rec!3359, !2394 (@isanders)
- Update References for: Rec!3459, Rec!3457, Moore!2334, Moore!2333, MooreOnline!246, MooreOnline!258 based on lhcb-master-mr/8260, !2434 (@lhcbsoft)
- Fix code that caused crashes online, !2433 (@mstahl)
- Update References for: Moore!2175 based on lhcb-master-mr/8231, !2427 (@lhcbsoft)
- [QEE] Sprucing lines to filter on relevant hlt2 lines, !2397 (@lugrazet)
- Implement Sprucing cache, !2380 (@nskidmor)
- Add missing/update HLT1-HLT2 decoding comparison tests, !2152 (@samarian)
