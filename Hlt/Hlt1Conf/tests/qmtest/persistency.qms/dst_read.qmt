<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Run HLT1 on an HLT1-filtered DST file.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="prerequisites"><set>
  <tuple><text>persistency.dst_write</text><enumeral>PASS</enumeral></tuple>
</set></argument>
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="args"><set>
  <text>$MOOREROOT/tests/options/default_conds_hlt1.py</text>
  <text>$HLT1CONFROOT/tests/options/hlt1_dst_input.py</text>
  <text>$HLT1CONFROOT/options/hlt1_pp_default.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>

from Moore.qmtest.exclusions import remove_known_warnings
countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 0},
                stdout=remove_known_warnings(stdout))

import re

pattern = re.compile(r'\s+NONLAZY_OR: hlt_decision\s+#=(\d+)\s+Sum=(\d+)')

# Check that:
#   1. We processed the same number of events as filtered by the previous job
#   2. We made the same number of positive decisions (which should be 100% of input events)
nread = nselected = -1
for line in stdout.split('\n'):
    m = re.match(pattern, line)
    if m:
        nread, nselected = map(int, m.groups())
        break
else:
    causes.append('could not parse event statistics from reader stdout')

# We're running the same HLT1 configuration as the one that created the
# filtered data, so all input events should have a positive decision
if nread != nselected:
    causes.append('expected all input events to pass')

nread_writing = nselected_writing = -1
with open('test_hlt1_persistence_dst_write.stdout') as f_ref:
    for line in f_ref.readlines():
        m = re.match(pattern, line)
        if m:
            nread_writing, nselected_writing = map(int, m.groups())
            break
    else:
        causes.append('could not parse event statistics from writer stdout')

if nread != nselected_writing:
    causes.append('did not read the same number of events as written out')

</text></argument>
</extension>
