###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options file for running on expected-2024 signal simulation

If you copy this code to a file with path
    Moore/hlt1_pp_expected_24_without_UT.py

Then you'll be able to run it with
    Moore/run gaudirun.py Moore/hlt1_pp_expected_24_without_UT.py

Please note that until Moore!3219 is merged, HLT1 flagging mode is
not possible on the master branch - this HLT1 filters! If you notice
that !3219 has been merged, and no-one has yet changed this tutorial
to use it - please feel free to do so!
"""

from Moore.options import options
from Moore.config import run_allen
from RecoConf.hlt1_allen import allen_gaudi_config as allen_sequence

# Input-specific options
options.conddb_tag = "sim-20231017-vc-md100"
options.dddb_tag = "dddb-20231017"
options.input_files = [
    # 'LFN:/lhcb/MC/Dev/DIGI/00206084/0000/00206084_00000004_1.digi',
    # Note: lhcb GRID proxy will be required to access this file
    # lambda_b to lambda_c(2593) (->lambda_c pi pi ) mu mu
    # with HltEfficiencyChecker use decay = "${Lamdab}[Lambda_b0 => ( Lambda_c(2595)+ => ( Lambda_c+ => ${pplus}p+ ${Kminus}K- ${piplus1}pi+ ) ${piplus0}pi+ ${piminus0}pi- ) ${mu}mu-  nu_mu~ ]CC"
    "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/Dev/DIGI/00206084/0000/00206084_00000004_1.digi"
]
options.input_type = "ROOT"
options.input_raw_format = 0.5
options.simulation = True
options.data_type = "Upgrade"

# Output options
options.output_file = 'hlt1_output.mdf'
options.output_type = 'MDF'

# Misc options
options.scheduler_legacy_mode = False
options.evt_max = 1000

with allen_sequence.bind(sequence="hlt1_pp_matching_no_ut_1000KHz"):
    run_allen(options)
