###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#!/usr/bin/python

# The script for plotting PV efficinecy as the function
# of various distributions: nTracks, z, r.
# As input the NTuple created by hlt1_reco_pvchecker.py
# is needed.
#
# The efficency is calculated usig TGraphAsymmErrors
# and Bayesian error bars
#
# author: Agnieszka Dziurda (agnieszka.dziurda@cern.ch)
# date:   02/2020
#
# Example of usage:
# ../../../run python PrimaryVertexCheckerPull.py
# --file file1.root file2.root --label name1 name2
#

import os, sys
import argparse

from ROOT import gROOT, TLegend

parser = argparse.ArgumentParser()
parser.add_argument(
    '--file', dest='fileName', default="", nargs='+', help='filename to plot')
parser.add_argument(
    '--label', dest='label', default="", nargs='+', help='labels for files')
parser.add_argument(
    '--tree',
    dest='treeName',
    default="",
    nargs='+',
    help='tree name to plot',
)
parser.add_argument(
    '--smog',
    dest='smog',
    default=False,
    action='store_true',
    help='set true for SMOG')
parser.add_argument(
    '--multi',
    dest='multi',
    default=False,
    action='store_true',
    help='add multiplicity plots')
parser.add_argument(
    '--isol',
    dest='isol',
    default=False,
    action='store_true',
    help='add isolated/closed plots')
parser.add_argument(
    '--dist',
    dest='dist',
    default=False,
    action='store_true',
    help='plot distributions in the canvas')
parser.add_argument(
    '--prefix',
    dest='prefix',
    default="pv_pull",
    help='prefix for the plot name',
)
parser.add_argument(
    '--dir',
    dest='directory',
    default=os.getcwd(),
    help='tree name to plot',
)

parser.add_argument(
    '--offset',
    dest='offset',
    default=0,
    help='offset for plot colors',
)


def get_labels(number_of_files):
    label = []
    for i in range(0, number_of_files):
        label.append("PV Checker {number}".format(number=str(i + 1)))
    return label


if __name__ == '__main__':
    args = parser.parse_args()
    path = args.directory + "/utils/"
    sys.path.append(os.path.abspath(path))
    offset = int(args.offset)
    gROOT.SetBatch()

    from pvutils import get_files, get_trees
    from pvutils import get_global, plot_comparison
    from pvutils import get_dependence
    from pvutils import plot

    from pvconfig import get_variable_ranges
    from pvconfig import set_legend, get_style, get_categories

    ranges = get_variable_ranges(args.smog)
    style = get_style()

    cat = get_categories(args.multi, args.isol, args.smog)
    label = args.label
    if args.label == "":
        label = get_labels(len(args.fileName))

    tr = {}
    tf = {}
    tf = get_files(tf, label, args.fileName)
    tr = get_trees(tf, tr, label, args.treeName, True)

    hist_x = {}
    hist_y = {}
    hist_z = {}
    norm = True  #to-do
    hist_x = get_global(hist_x, tr, "pullx", "#Delta x / #sigma_{x}",
                        "Candidates Normalized", style, ranges, cat, label,
                        offset)
    hist_y = get_global(hist_y, tr, "pully", "#Delta y / #sigma_{y}",
                        "Candidates Normalized", style, ranges, cat, label,
                        offset)
    hist_z = get_global(hist_z, tr, "pullz", "#Delta z / #sigma_{z}",
                        "Candidates Normalized", style, ranges, cat, label,
                        offset)

    plot_comparison(hist_x, args.prefix, "pullx", cat, label, style, norm,
                    offset)
    plot_comparison(hist_y, args.prefix, "pully", cat, label, style, norm,
                    offset)
    plot_comparison(hist_z, args.prefix, "pullz", cat, label, style, norm,
                    offset)

    from ROOT import gEnv
    gEnv.SetValue("Hist.Binning.1D.x", "100")

    graph = {}
    graph["tracks"] = {}
    graph["tracks"]["pullx"] = {}
    graph["tracks"]["pully"] = {}
    graph["tracks"]["pullz"] = {}
    graph["tracks"]["pullx"] = get_dependence(graph["tracks"]["pullx"], tr,
                                              "pullx", "nrectrmc", ranges,
                                              style, cat, label, offset)
    graph["tracks"]["pully"] = get_dependence(graph["tracks"]["pully"], tr,
                                              "pully", "nrectrmc", ranges,
                                              style, cat, label, offset)
    graph["tracks"]["pullz"] = get_dependence(graph["tracks"]["pullz"], tr,
                                              "pullz", "nrectrmc", ranges,
                                              style, cat, label, offset)

    legend = TLegend(0.15, 0.86, 0.88, 0.98)
    legend = set_legend(legend, label, graph["tracks"]["pullz"], "sigma")

    lhcbtextpos = (0.9, 0.75)
    lhcbtextposdown = (0.9, 0.25)
    plot(
        graph["tracks"]["pullx"],
        "mean",
        args.prefix + "_ntracks_mean",
        "pullx",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)
    plot(
        graph["tracks"]["pullx"],
        "sigma",
        args.prefix + "_ntracks_sigma",
        "pullx",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextposdown)

    plot(
        graph["tracks"]["pully"],
        "mean",
        args.prefix + "_ntracks_mean",
        "pully",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)
    plot(
        graph["tracks"]["pully"],
        "sigma",
        args.prefix + "_ntracks_sigma",
        "pully",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextposdown)

    plot(
        graph["tracks"]["pullz"],
        "mean",
        args.prefix + "_ntracks_mean",
        "pullz",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)
    plot(
        graph["tracks"]["pullz"],
        "sigma",
        args.prefix + "_ntracks_sigma",
        "pullz",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextposdown)

    graph["z"] = {}
    graph["z"]["pullx"] = {}
    graph["z"]["pully"] = {}
    graph["z"]["pullz"] = {}
    graph["z"]["pullx"] = get_dependence(graph["z"]["pullx"], tr, "pullx",
                                         "zMC", ranges, style, cat, label,
                                         offset)
    graph["z"]["pully"] = get_dependence(graph["z"]["pully"], tr, "pully",
                                         "zMC", ranges, style, cat, label,
                                         offset)
    graph["z"]["pullz"] = get_dependence(graph["z"]["pullz"], tr, "pullz",
                                         "zMC", ranges, style, cat, label,
                                         offset)

    plot(
        graph["z"]["pullx"],
        "mean",
        args.prefix + "_z_mean",
        "pullx",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)
    plot(
        graph["z"]["pullx"],
        "sigma",
        args.prefix + "_z_sigma",
        "pullx",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextposdown)

    plot(
        graph["z"]["pully"],
        "mean",
        args.prefix + "_z_mean",
        "pully",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)
    plot(
        graph["z"]["pully"],
        "sigma",
        args.prefix + "_z_sigma",
        "pully",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextposdown)

    plot(
        graph["z"]["pullz"],
        "mean",
        args.prefix + "_z_mean",
        "pullz",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextpos)
    plot(
        graph["z"]["pullz"],
        "sigma",
        args.prefix + "_z_sigma",
        "pullz",
        cat,
        label,
        legend,
        lhcbtextpos=lhcbtextposdown)
