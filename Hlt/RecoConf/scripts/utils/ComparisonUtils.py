###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file 'COPYING'.   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
''' Utils for comparison of clustering performance with CPU and FPGAs.

@author by Tommaso Pajero (tommaso.pajero@cern.ch)
@date 03/2020
'''

from ROOT import TFile, TLatex, kRed, kBlack, kFullSquare, kOpenCircle


def get_colors():
    return [kBlack, kRed]


def get_markers():
    return [kFullSquare, kOpenCircle]


def get_fillstyles():
    return [3345, 3354]


def get_files(tf, filename, label):
    for i, f in enumerate(filename):
        tf[label[i]] = TFile(f, "read")
    return tf


def get_trees(tr, tf, label, treename):
    for lab in label:
        tr[lab] = tf[lab].Get(treename)
    return tr


def draw_sample(canvas, x, y, textsize=0.06, cut="", nevents=1000):
    canvas.cd()
    latex = TLatex()
    latex.SetTextSize(textsize)
    latex.SetTextFont(132)
    latex.SetTextAlign(12)
    latex.DrawLatexNDC(x, y + 0.12 * (textsize / 0.06), "LHCb Simulation")
    latex.DrawLatexNDC(x, y + 0.06 * (textsize / 0.06),
                       "{:.0f} min. bias".format(nevents))
    latex.DrawLatexNDC(x, y, "#it{L} = 2#times10^{33} cm^{#minus2}s^{#minus1}")
    latex.DrawLatexNDC(x, y - 0.06, cut)


def make_axis_label(var, var_dict):
    label = var_dict[var]["label"]
    unit = var_dict[var]["unit"]
    unit = "" if unit is None else " [{}]".format(unit)
    return label + unit
