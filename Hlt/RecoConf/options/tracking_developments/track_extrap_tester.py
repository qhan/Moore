###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.hlt2_tracking import make_hlt2_tracks

from RecoConf.core_algorithms import make_unique_id_generator

from PyConf.Algorithms import TrackExtrapolatorTesterSOA
from PyConf.Algorithms import LHCb__Converters__Track__SOA__fromV1Track as fromV1TrackFittedGenericTrack
from PyConf.Tools import (TrackRungeKuttaExtrapolator, TrackMasterExtrapolator,
                          TrackParabolicExtrapolator, TrackLinearExtrapolator,
                          TrackSTEPExtrapolator)
from Gaudi.Configuration import ERROR

options.evt_max = 5
options.ntuple_file = "track_extrap_tester.root"


def track_extrapolators_test():
    track_type = "BestDownstream"
    fitted = make_hlt2_tracks(
        fast_reco=True, use_pr_kf=True, light_reco=True)[track_type]["v1"]
    fitted_v3 = fromV1TrackFittedGenericTrack(
        name='TrackConverter_v1_to_v3',
        InputTracks=fitted,
        InputUniqueIDGenerator=make_unique_id_generator(),
        RestrictToType="Downstream",
        OutputLevel=ERROR).OutputTracks

    ETester = TrackExtrapolatorTesterSOA(
        Extrapolators=[
            TrackRungeKuttaExtrapolator(name="AdaptiveRK"),
            TrackMasterExtrapolator(
                name="Master",
                ApplyMultScattCorr=False,
                ApplyEnergyLossCorr=False,
                ApplyElectronEnergyLossCorr=False),
            TrackParabolicExtrapolator(name="Parabolic"),
            TrackLinearExtrapolator(name="Linear"),
            TrackSTEPExtrapolator(name="STEP")
        ],
        ReferenceExtrapolator=TrackRungeKuttaExtrapolator(
            name="ReferenceRK", MaxStep=20., InitialStep=10.),
        TrackContainer=fitted_v3)
    return Reconstruction('track_extrapolators_test', [ETester])


run_reconstruction(options, track_extrapolators_test)
