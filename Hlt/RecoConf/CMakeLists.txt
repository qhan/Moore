###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

gaudi_install(PYTHON)

gaudi_add_tests(QMTest)

if(BINARY_TAG MATCHES "san$")
    # When running in sanitizer platforms, the tests take longer and
    # the test job gets killed at the 10h mark. Here we disable some
    # test to make the runtime more reasonable.

    # Disable mcchecking and resolution, tests and tests of examples, as
    # they are less interesting for production.
    get_property(recoconf_tests DIRECTORY PROPERTY TESTS)
    foreach(test IN ITEMS ${recoconf_tests})
        if(test MATCHES "mcchecking|efficiency|plots|examples")
            set_property(TEST ${test} PROPERTY DISABLED TRUE)
        endif()
    endforeach()
endif()

if (BINARY_TAG MATCHES ".*cuda.*")
    # When compiling for GPU, Gaudi-Allen algorithm wrappers call the device algorithms
    # since these are incompatible with calling from Moore / Gaudi, disable Gaudi-Allen tests
    get_property(recoconf_tests DIRECTORY PROPERTY TESTS)
    foreach(test IN ITEMS ${recoconf_tests})
        if(test MATCHES "allen")
	    #message(STATUS "Disabling ${test}")
            set_property(TEST ${test} PROPERTY DISABLED TRUE)
        endif()
    endforeach()
endif()


if(BUILD_TESTING AND USE_DD4HEP)
    # Disable some tests that are not yet dd4hep ready
    set_property(
        TEST
           # These need muon geometry v2
           RecoConf.decoding.compare_hlt1_hlt2_calo_decoding_v2bank_v3geometry
           # Not a proper DD4hep conditions, Error Muon Invalid add
           RecoConf.hlt2_lead_lead_fast_reco_pr_kf_without_UT_gec_25000_with_mcchecking_velo_open_2023 #PbPb VELO open sample
           RecoConf.hlt2_light_reco_pr_kf_velo_open_without_UT_with_mcchecking #using VELO open samples
           # see https://gitlab.cern.ch/lhcb/Moore/-/issues/698
           RecoConf.allen_gaudi_downstream_with_mcchecking
           # disabled due to TransportSvc usage
           RecoConf.examples.mc_matching_example
        PROPERTY
            DISABLED TRUE
    )
endif()

if(BUILD_TESTING AND NOT USE_DD4HEP)
    # Disable some tests that are not detdesc compatible
    set_property(
        TEST
           RecoConf.phoenix_event_data_dump
           RecoConf.hlt2_reco_data_2023
           RecoConf.hlt1_pvs_with_beamspotmoni_2023
           RecoConf.decoding.compare_hlt1_hlt2_muon_decoding_v3geometry_data_2023
        PROPERTY
            DISABLED TRUE
    )
endif()
