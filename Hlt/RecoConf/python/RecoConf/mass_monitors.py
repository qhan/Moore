###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import Reconstruction
from SelAlgorithms.monitoring import monitor, histogram_1d
from PyConf.packing import persistable_location
from RecoConf.reconstruction_objects import reconstruction
from GaudiKernel.SystemOfUnits import (GeV, MeV, mm, micrometer as um)
from RecoConf.legacy_rec_hlt1_tracking import make_pvs
from RecoConf.event_filters import require_pvs
from Hlt2Conf.standard_particles import (make_photons, make_ismuon_long_muon,
                                         make_long_pions, make_long_kaons,
                                         make_long_protons)
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter)
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_dielectron
import Functors as F


def _MIP_MIN(cut, pvs=make_pvs):
    return F.MINIPCUT(IPCut=cut, Vertices=pvs())


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


def _DZ_CHILD(i):
    return F.CHILD(i, F.END_VZ) - F.END_VZ


def _DRHO2_CHILD(i):
    return (F.CHILD(i, F.END_VX) - F.END_VX) * (F.CHILD(
        i, F.END_VX) - F.END_VX) + (F.CHILD(i, F.END_VY) - F.END_VY) * (
            F.CHILD(i, F.END_VY) - F.END_VY)


def _filter_photons():
    return ParticleFilter(
        make_photons(),
        F.FILTER(
            F.require_all(F.PT > 300 * MeV,
                          F.CALO_NEUTRAL_1TO9_ENERGY_RATIO > 0.9,
                          F.IS_PHOTON > 0.7, F.IS_NOT_H > 0.5)))


def _filter_long_pions_for_strange():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV, _MIPCHI2_MIN(24.))))


def _filter_long_pions_for_charm():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(F.PT > 200 * MeV, _MIP_MIN(120 * um))))


def _filter_long_pions_for_dst():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV, _MIP_MIN(100 * um))))


def _filter_long_kaons_for_strange():
    return ParticleFilter(
        make_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 100 * MeV, _MIPCHI2_MIN(12.), F.PID_K > 0.0)))


def _filter_long_kaons_for_charm():
    return ParticleFilter(
        make_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 200 * MeV, _MIP_MIN(80. * um),
                          F.PID_K > 5.0)))


def _filter_long_protons_for_strange():
    return ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(F.PT > 500 * MeV, F.P > 9 * GeV, _MIPCHI2_MIN(8.),
                          F.PID_P > 0.0)))


def _filter_long_protons_for_charm():
    return ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(F.PT > 600 * MeV, F.P > 9 * GeV, _MIP_MIN(80. * um),
                          F.PID_P > 5.0)))


def _filter_long_muons_for_jpsi():
    cut = F.require_all(F.PT > 500 * MeV,
                        F.MINIP(make_pvs()) < 120 * um, F.PID_MU > 0.)
    return ParticleFilter(make_ismuon_long_muon(), F.FILTER(cut))


def _diphoton():
    return ParticleCombiner(
        name="RecoMon_DiPhoton",
        Inputs=[_filter_photons(), _filter_photons()],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor="eta -> gamma gamma",
        CombinationCut=F.require_all(
            F.math.in_range(0 * MeV, F.MASS, 1200 * MeV), F.PT > 1.6 * GeV),
        CompositeCut=F.ALL,
    )


def _gg_mass_mon():
    return monitor(
        data=_diphoton(),
        histograms=[
            histogram_1d(
                name="gg_m",
                title="DiPhotonMassMon",
                label="MASS",
                functor=F.MASS,
                bins=240,
                range=(0 * MeV, 1200 * MeV))
        ])


def _ks():
    return ParticleCombiner(
        [_filter_long_pions_for_strange(),
         _filter_long_pions_for_strange()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name="RecoMon_KS0_LL",
        CombinationCut=F.require_all(
            F.math.in_range(380 * MeV, F.MASS, 610 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 300 * MeV,
            F.P > 3.5 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(410 * MeV, F.MASS, 590 * MeV),
            F.PT > 350 * MeV,
            F.P > 4 * GeV,
            F.CHI2DOF < 12.,
            F.BPVVDZ(make_pvs()) > 4 * mm,
            F.BPVVDRHO(make_pvs()) > 1.5 * mm,
            F.BPVDIRA(make_pvs()) > 0.999,
        ),
    )


def _ks_mass_mon():
    return monitor(
        data=_ks(),
        histograms=[
            histogram_1d(
                name="ks_m",
                title="KShortMassMon",
                label="MASS",
                functor=F.MASS,
                bins=180,
                range=(410 * MeV, 590 * MeV))
        ])


def _lambda():
    return ParticleCombiner(
        [_filter_long_protons_for_strange(),
         _filter_long_pions_for_strange()],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="RecoMon_Lambda_LL",
        CombinationCut=F.require_all(
            F.MASS < 1180 * MeV,
            F.MAXDOCACUT(500 * um),
            F.PT > 650 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1140 * MeV,
            F.PT > 700 * MeV,
            F.CHI2DOF < 16.,
            F.BPVVDZ(make_pvs()) > 4 * mm,
            F.BPVVDRHO(make_pvs()) > 2 * mm,
            F.BPVFDCHI2(make_pvs()) > 180.,
            F.math.in_range(-80 * mm, F.END_VZ, 640 * mm),
            F.BPVDIRA(make_pvs()) > 0.999,
        ),
    )


def _filter_lambda0():
    return ParticleFilter(_lambda(), F.FILTER(
        F.require_all(F.PARTICLE_ID > 0)))


def _filter_lambda0bar():
    return ParticleFilter(_lambda(), F.FILTER(
        F.require_all(F.PARTICLE_ID < 0)))


def _lambda_mass_mon():
    return monitor(
        data=_lambda(),
        histograms=[
            histogram_1d(
                name="lambda_m",
                title="LambdaMassMon",
                label="MASS",
                functor=F.MASS,
                bins=120,
                range=(1080 * MeV, 1140 * MeV))
        ])


def _lambda0_mass_mon():
    return monitor(
        data=_filter_lambda0(),
        histograms=[
            histogram_1d(
                name="lambda0_m",
                title="Lambda0MassMon",
                label="MASS",
                functor=F.MASS,
                bins=120,
                range=(1080 * MeV, 1140 * MeV))
        ])


def _lambda0bar_mass_mon():
    return monitor(
        data=_filter_lambda0bar(),
        histograms=[
            histogram_1d(
                name="lambda0bar_m",
                title="Lambda0barMassMon",
                label="MASS",
                functor=F.MASS,
                bins=120,
                range=(1080 * MeV, 1140 * MeV))
        ])


def _xi():
    return ParticleCombiner(
        [_lambda(), _filter_long_pions_for_strange()],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="RecoMon_Xim_LLL",
        CombinationCut=F.require_all(
            F.math.in_range(1110 * MeV, F.CHILD(1, F.MASS), 1121 * MeV),
            F.MASS < 1400 * MeV,
            F.PT > 700 * MeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1380 * MeV,
            F.PT > 800 * MeV,
            F.CHI2DOF < 12.,
            _DZ_CHILD(1) > 8 * mm,
            _DRHO2_CHILD(1) > 50 * 50 * um * um,
            F.BPVVDZ(make_pvs()) > 4 * mm,
            F.BPVFDCHI2(make_pvs()) > 80.,
            F.math.in_range(-120 * mm, F.END_VZ, 630 * mm),
            F.BPVDIRA(make_pvs()) > 0.999,
        ),
    )


def _xi_mass_mon():
    return monitor(
        data=_xi(),
        histograms=[
            histogram_1d(
                name="xi_m",
                title="XiMassMon",
                label="MASS",
                functor=(F.MASS - F.CHILD(1, F.MASS) + 1115.683 * MeV),
                bins=120,
                range=(1260 * MeV, 1380 * MeV))
        ])


def _omega():
    return ParticleCombiner(
        [_lambda(), _filter_long_kaons_for_strange()],
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        name="RecoMon_Omega_LLL",
        CombinationCut=F.require_all(
            F.math.in_range(1110 * MeV, F.CHILD(1, F.MASS), 1121 * MeV),
            F.MASS < 1800 * MeV,
            F.PT > 700 * MeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1760 * MeV,
            F.PT > 800 * MeV,
            F.CHI2DOF < 12.,
            _DZ_CHILD(1) > 4 * mm,
            _DRHO2_CHILD(1) > 50 * 50 * um * um,
            F.BPVVDZ(make_pvs()) > 2 * mm,
            F.BPVFDCHI2(make_pvs()) > 64.,
            F.math.in_range(-120 * mm, F.END_VZ, 630 * mm),
            F.BPVDIRA(make_pvs()) > 0.999,
        ),
    )


def _omega_mass_mon():
    return monitor(
        data=_omega(),
        histograms=[
            histogram_1d(
                name="omega_m",
                title="OmegaMassMon",
                label="MASS",
                functor=(F.MASS - F.CHILD(1, F.MASS) + 1115.683 * MeV),
                bins=80,
                range=(1600 * MeV, 1760 * MeV))
        ])


def _d0():
    return ParticleCombiner(
        [_filter_long_kaons_for_charm(),
         _filter_long_pions_for_charm()],
        DecayDescriptor="[D0 -> K- pi+]cc",
        name="RecoMon_D0ToKmPip",
        CombinationCut=F.require_all(
            F.math.in_range(1665 * MeV, F.MASS, 2065 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 10 * GeV,
            F.SUM(F.PT) > 1.2 * GeV,
            F.MAXSDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1700 * MeV, F.MASS, 2030 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 12 * GeV,
            F.CHI2DOF < 10.,
            F.BPVIP(make_pvs()) < 0.8 * mm,
            F.BPVVDZ(make_pvs()) > 1 * mm,
            F.BPVVDRHO(make_pvs()) < 2 * mm,
            F.BPVFDCHI2(make_pvs()) > 24.,
            F.BPVDIRA(make_pvs()) > 0.9999,
        ),
    )


def _d0_mass_mon():
    return monitor(
        data=_d0(),
        histograms=[
            histogram_1d(
                name="d0_m",
                title="D0MassMon",
                label="MASS",
                functor=F.MASS,
                bins=150,
                range=(1715 * MeV, 2015 * MeV))
        ])


def _dst():
    return ParticleCombiner(
        [_d0(), _filter_long_pions_for_dst()],
        DecayDescriptor="[D*(2010)+ -> D0 pi+]cc",
        name="RecoMon_Dstar",
        CombinationCut=F.require_all(
            F.math.in_range(1845 * MeV, F.CHILD(1, F.MASS), 1885 * MeV),
            F.MASS - F.CHILD(1, F.MASS) < 195 * MeV,
            F.MAXDOCACUT(150 * um),
            F.PT > 1800 * MeV,
        ),
        CompositeCut=F.require_all(F.MASS - F.CHILD(1, F.MASS) < 180 * MeV,
                                   F.CHI2DOF < 12., F.PT > 2000 * MeV),
    )


def _dst_mass_mon():
    return monitor(
        data=_dst(),
        histograms=[
            histogram_1d(
                name="dst_m",
                title="DstMassMon",
                label="MASS",
                functor=(F.MASS - F.CHILD(1, F.MASS)),
                bins=80,
                range=(140 * MeV, 180 * MeV))
        ])


def _dp():
    return ParticleCombiner(
        [
            _filter_long_kaons_for_charm(),
            _filter_long_pions_for_charm(),
            _filter_long_pions_for_charm()
        ],
        DecayDescriptor="[D+ -> K- pi+ pi+]cc",
        name="RecoMon_DpToKmPipPip",
        Combination12Cut=F.MASS < 1960 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(1640 * MeV, F.MASS, 2100 * MeV),
            F.PT > 1.2 * GeV,
            F.P > 10 * GeV,
            F.SUM(F.PT) > 1.2 * GeV,
            F.MAXSDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1670 * MeV, F.MASS, 2070 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 12 * GeV,
            F.CHI2DOF < 12.,
            F.BPVIP(make_pvs()) < 0.8 * mm,
            F.BPVVDZ(make_pvs()) > 1.5 * mm,
            F.BPVVDRHO(make_pvs()) < 2 * mm,
            F.BPVFDCHI2(make_pvs()) > 28.,
            F.BPVDIRA(make_pvs()) > 0.9999,
        ),
    )


def _filter_dp():
    return ParticleFilter(_dp(), F.FILTER(F.require_all(F.PARTICLE_ID > 0)))


def _filter_dm():
    return ParticleFilter(_dp(), F.FILTER(F.require_all(F.PARTICLE_ID < 0)))


def _dp_mass_mon():
    return monitor(
        data=_dp(),
        histograms=[
            histogram_1d(
                name="dp_m",
                title="DpMassMon",
                label="MASS",
                functor=F.MASS,
                bins=100,
                range=(1670 * MeV, 2070 * MeV))
        ])


def _dplus_mass_mon():
    return monitor(
        data=_filter_dp(),
        histograms=[
            histogram_1d(
                name="dplus_m",
                title="DplusMassMon",
                label="MASS",
                functor=F.MASS,
                bins=100,
                range=(1670 * MeV, 2070 * MeV))
        ])


def _dminus_mass_mon():
    return monitor(
        data=_filter_dm(),
        histograms=[
            histogram_1d(
                name="dminus_m",
                title="DminusMassMon",
                label="MASS",
                functor=F.MASS,
                bins=100,
                range=(1670 * MeV, 2070 * MeV))
        ])


def _ds():
    return ParticleCombiner(
        [
            _filter_long_kaons_for_charm(),
            _filter_long_kaons_for_charm(),
            _filter_long_pions_for_charm()
        ],
        DecayDescriptor="[D_s+ -> K+ K- pi+]cc",
        name="RecoMon_DspToPhiPip",
        Combination12Cut=F.MASS < 1050 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(1665 * MeV, F.MASS, 2165 * MeV),
            F.PT > 1.2 * GeV,
            F.P > 10 * GeV,
            F.SUM(F.PT) > 1.2 * GeV,
            F.MAXSDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1700 * MeV, F.MASS, 2100 * MeV),
            F.PT > 600 * MeV,
            F.CHI2DOF < 12.,
            F.BPVIP(make_pvs()) < 0.8 * mm,
            F.BPVVDZ(make_pvs()) > 1 * mm,
            F.BPVVDRHO(make_pvs()) < 2.5 * mm,
            F.BPVFDCHI2(make_pvs()) > 24.,
            F.BPVDIRA(make_pvs()) > 0.9999,
        ),
    )


def _ds_mass_mon():
    return monitor(
        data=_ds(),
        histograms=[
            histogram_1d(
                name="ds_m",
                title="DsMassMon",
                label="MASS",
                functor=F.MASS,
                bins=100,
                range=(1700 * MeV, 2100 * MeV))
        ])


def _lc():
    return ParticleCombiner(
        [
            _filter_long_protons_for_charm(),
            _filter_long_kaons_for_charm(),
            _filter_long_pions_for_charm()
        ],
        DecayDescriptor="[Lambda_c+ -> p+ K- pi+]cc",
        name="RecoMon_LcpToPpKmPip",
        Combination12Cut=F.MASS < 2420 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(2200 * MeV, F.MASS, 2560 * MeV),
            F.PT > 1.7 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXSDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2130 * MeV, F.MASS, 2630 * MeV),
            F.PT > 2 * GeV,
            F.P > 16 * GeV,
            F.CHI2DOF < 12.,
            F.BPVIP(make_pvs()) < 0.8 * mm,
            F.BPVVDZ(make_pvs()) > 0.5 * mm,
            F.BPVVDRHO(make_pvs()) < 2 * mm,
            F.BPVFDCHI2(make_pvs()) > 16.,
            F.BPVDIRA(make_pvs()) > 0.9999,
        ),
    )


def _lc_mass_mon():
    return monitor(
        data=_lc(),
        histograms=[
            histogram_1d(
                name="lc_m",
                title="LcMassMon",
                label="MASS",
                functor=F.MASS,
                bins=125,
                range=(2130 * MeV, 2630 * MeV))
        ])


def _jpsi():
    return ParticleCombiner(
        [_filter_long_muons_for_jpsi(),
         _filter_long_muons_for_jpsi()],
        name="RecoMon_JpsiToMumMup",
        DecayDescriptor="J/psi(1S) -> mu+ mu-",
        CombinationCut=F.require_all(
            F.math.in_range(2.6 * GeV, F.MASS, 4.0 * GeV),
            F.SUM(F.PT) > 2. * GeV,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2.7 * GeV, F.MASS, 3.9 * GeV), F.CHI2DOF < 12.,
            F.BPVIP(make_pvs()) < 0.8 * mm),
    )


def _jpsi_mass_mon():
    return monitor(
        data=_jpsi(),
        histograms=[
            histogram_1d(
                name="jpsi_m",
                title="JpsiMassMon",
                label="MASS",
                functor=F.MASS,
                bins=120,
                range=(2.7 * GeV, 3.9 * GeV))
        ])


def _jpsi_ee():
    return make_rd_detached_dielectron(am_min=2000 * MeV, am_max=3500 * MeV)


def _jpsi_ee_mass_mon():
    return monitor(
        data=_jpsi_ee(),
        histograms=[
            histogram_1d(
                name="jpsi_ee_m",
                title="JpsiEEMassMon",
                label="MASS",
                functor=F.MASS,
                bins=150,
                range=(2.0 * GeV, 3.5 * GeV))
        ])


def make_mass_monitors():
    with persistable_location.bind(force=False),\
            reconstruction.bind(from_file=False):
        ks_mon_reco = Reconstruction(
            "KShortMassMonCF", [_ks_mass_mon()], filters=[_ks()])
        gg_mon_reco = Reconstruction(
            "DiPhotonMassMonCF", [_gg_mass_mon()], filters=[_diphoton()])
        lambda_mon_reco = Reconstruction(
            "LambdaMassMonCF", [_lambda_mass_mon()], filters=[_lambda()])
        lambda0_mon_reco = Reconstruction(
            "Lambda0MassMonCF", [_lambda0_mass_mon()],
            filters=[_filter_lambda0()])
        lambda0bar_mon_reco = Reconstruction(
            "Lambda0barMassMonCF", [_lambda0bar_mass_mon()],
            filters=[_filter_lambda0bar()])
        xi_mon_reco = Reconstruction(
            "XiMassMonCF", [_xi_mass_mon()], filters=[_xi()])
        omega_mon_reco = Reconstruction(
            "OmegaMassMonCF", [_omega_mass_mon()], filters=[_omega()])
        d0_mon_reco = Reconstruction(
            "D0MassMonCF", [_d0_mass_mon()], filters=[_d0()])
        dst_mon_reco = Reconstruction(
            "DstMassMonCF", [_dst_mass_mon()], filters=[_dst()])
        dp_mon_reco = Reconstruction(
            "DpMassMonCF", [_dp_mass_mon()], filters=[_dp()])
        dplus_mon_reco = Reconstruction(
            "DplusMassMonCF", [_dplus_mass_mon()], filters=[_filter_dp()])
        dminus_mon_reco = Reconstruction(
            "DminusMassMonCF", [_dminus_mass_mon()], filters=[_filter_dm()])
        ds_mon_reco = Reconstruction(
            "DsMassMonCF", [_ds_mass_mon()], filters=[_ds()])
        lc_mon_reco = Reconstruction(
            "LcMassMonCF", [_lc_mass_mon()], filters=[_lc()])
        jpsi_mon_reco = Reconstruction(
            "JpsiMassMonCF", [_jpsi_mass_mon()], filters=[_jpsi()])
        jpsiee_mon_reco = Reconstruction(
            "JpsiEEMassMonCF", [_jpsi_ee_mass_mon()], filters=[_jpsi_ee()])
        reco_with_require_pvs = Reconstruction(
            "MassMonWithPVsCF", [
                ks_mon_reco.node, lambda_mon_reco.node, lambda0_mon_reco.node,
                lambda0bar_mon_reco.node, xi_mon_reco.node,
                omega_mon_reco.node, d0_mon_reco.node, dst_mon_reco.node,
                dp_mon_reco.node, dplus_mon_reco.node, dminus_mon_reco.node,
                ds_mon_reco.node, lc_mon_reco.node, jpsi_mon_reco.node,
                jpsiee_mon_reco.node
            ],
            filters=[require_pvs(make_pvs())])
        return [gg_mon_reco.node, reco_with_require_pvs.node]
