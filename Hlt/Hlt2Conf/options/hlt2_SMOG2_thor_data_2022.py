###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration file to test running on data.
"""

from Moore import options, run_moore
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_light_reco_pr_kf_without_UT
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks)
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
from RecoConf.legacy_rec_hlt1_tracking import make_reco_pvs, make_PatPV3DFuture_pvs

from Hlt2Conf.lines.ift import all_lines  #Just test SMOG2 lines

options.set_input_and_conds_from_testfiledb('Run251995_ppandSMOG2_RealData')

options.input_type = 'RAW'
options.input_process = 'Hlt2'
options.evt_max = 1000
options.output_file = "data_SMOG2_hlt2_2022_Run251995_addedSMOG2lines.dst"
options.output_type = "ROOT"
options.histo_file = "data_SMOG2_hlt2_2022_Run251995_addedSMOG2lines_histos.root"
options.output_manifest_file = "data_SMOG2_hlt2_2022_Run251995_addedSMOG2lines.tck.json"

#================================
# Data tags
#================================

options.geometry_version = 'run3/trunk'
options.conditions_version = 'master'
options.simulation = False

#options.event_store = 'EvtStoreSvc'

#===============================
# DD4HEP settings
#===============================

dd4hepSvc = DD4hepSvc()
dd4hepSvc.DetectorList = [
    '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal', 'Muon'
]  #no UT
dd4hepSvc.ConditionsLocation = 'git:/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git'


def make_lines():
    return [builder() for builder in all_lines.values()]


public_tools = []

options.scheduler_legacy_mode = False

with reconstruction.bind(from_file=False),\
    make_light_reco_pr_kf_without_UT.bind(skipRich=False, skipCalo=False, skipMuon=False),\
    make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=8.0),\
    make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=8.0),\
    make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=8.0),\
    make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=8.0),\
    default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),\
    hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT),\
    make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs):
    config = run_moore(options, make_lines, public_tools)
