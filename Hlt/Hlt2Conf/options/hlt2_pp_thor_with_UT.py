###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example options file for running HLT2 lines in on expected-2024 min bias with the expected 2024 with-UT
reconstruction configuration.

If you copy this code to a file with path
    Moore/hlt2_pp_with_UT.py

Then you'll be able to run it with
    Moore/run gaudirun.py Moore/hlt2_pp_with_UT.py
"""

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_light_reco_pr_kf
from Hlt2Conf.lines.topological_b import all_lines as topo_lines
from Moore.streams import Stream, Streams
from DDDB.CheckDD4Hep import UseDD4Hep

if UseDD4Hep:
    raise RuntimeError(
        "Sorry, you'll need a detdesc stack to run over this MC. Please see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/running_over_mc.html#switching-to-a-detdesc-compatible-platform"
    )

# Input-specific options
options.set_input_and_conds_from_testfiledb(
    "expected_2024_min_bias_hlt1_filtered")

# Output options
options.output_file = 'hlt2_output__{stream}.mdf'
options.output_type = 'MDF'

# Misc options
options.scheduler_legacy_mode = False
options.n_threads = 1  # can be multi-threaded
options.evt_max = 100


def make_streams():
    streams = [
        Stream(
            "full",
            lines=[builder() for builder in topo_lines.values()],
            routing_bit=98,
            detectors=[]
            # detectors=DETECTORS - import from Moore.streams
        ),  # Turbo or Full case - no detector raw banks, for TurCal add them as above.
        # Add turbo lines as well if you like:
        # Would need to import e.g. from Hlt2Conf.lines.b2oc import all_lines as turbo_lines
        # Stream(
        #    "turbo",
        #    lines=[builder() for builder in turbo_lines.values()],
        #    routing_bit=99,
        #    detectors=[]
        # )
    ]
    return Streams(streams=streams)


public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom()
]
with reconstruction.bind(from_file=False),\
     hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf):
    config = run_moore(options, make_streams, public_tools)
