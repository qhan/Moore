###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration file of a throughput test using ThOr-based selection algorithms.
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_fastest_reconstruction
from Hlt2Conf.lines import all_lines
import re


def remove_lines(lines_dict, pattern_to_remove):
    filtered = {
        name: line
        for name, line in lines_dict.items()
        if re.search(pattern_to_remove, name) is None
    }
    return filtered


## explicitly remove lines that require hlt1_filter_code
to_remove = [
    'Hlt2IFT_SMOG2GECPassthrough', 'Hlt2IFT_SMOG2LumiPassthrough',
    'Hlt2IFT_SMOG2MBPassthrough', 'Hlt2IFT_Femtoscopy_InclLambdaLL',
    'Hlt2IFT_Femtoscopy_InclXiLLL', 'Hlt2IFT_Femtoscopy_InclOmegaLLL',
    'Hlt2IFT_Femtoscopy_LambdaP', 'Hlt2IFT_Femtoscopy_LambdaP_lowK',
    'Hlt2IFT_Femtoscopy_LambdaLambda', 'Hlt2IFT_Femtoscopy_LambdaLambda_lowK',
    'Hlt2IFT_Femtoscopy_XiP', 'Hlt2IFT_Femtoscopy_XiP_lowK',
    'Hlt2IFT_Femtoscopy_XiLambda', 'Hlt2IFT_Femtoscopy_XiLambda_lowK',
    'Hlt2IFT_Femtoscopy_XiXi', 'Hlt2IFT_Femtoscopy_OmegaP',
    'Hlt2IFT_Femtoscopy_OmegaP', 'Hlt2IFT_Femtoscopy_OmegaP_lowK',
    'Hlt2IFT_Femtoscopy_OmegaLambda', 'Hlt2IFT_Femtoscopy_OmegaXi',
    'Hlt2IFT_Femtoscopy_OmegaOmega', 'Hlt2QEE_DiElectronPrompt_PersistPhotons',
    'Hlt2QEE_DiElectronPrompt_PersistPhotons_FULL',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotons',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotons_FULL',
    'Hlt2QEE_DiElectronPrompt_PersistPhotonsSS',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotonsSS', 'Hlt2QEE_IncJet10GeV',
    'Hlt2QEE_IncDiJet10GeV', 'Hlt2QEE_IncDiJet15GeV', 'Hlt2QEE_IncDiJet20GeV',
    'Hlt2QEE_IncDiJet25GeV', 'Hlt2QEE_IncDiJet30GeV', 'Hlt2QEE_IncDiJet35GeV'
]
trunc_lines = all_lines

for remove in to_remove:
    trunc_lines = remove_lines(trunc_lines, remove)

print("Removed lines: ", all_lines.keys() - trunc_lines.keys())


def make_lines():
    return [builder() for builder in trunc_lines.values()]


public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom()
]

options.scheduler_legacy_mode = False
options.output_file = "hlt2_pp_thor.mdf"
options.output_type = "MDF"

with reconstruction.bind(from_file=False),\
     hlt2_reconstruction.bind(make_reconstruction=make_fastest_reconstruction):
    config = run_moore(options, make_lines, public_tools)
