###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
from RecoConf.reconstruction_objects import reconstruction

from Hlt2Conf.lines.bnoc.hlt2_bnoc import Bds_KstzKstzb_line

#from Hlt2Conf.lines.bnoc import all_lines


def make_lines():
    #lines = [builder() for builder in all_lines.values()]
    # to run a specific line
    lines = [Bds_KstzKstzb_line()]
    return lines


options.lines_maker = make_lines
options.set_input_and_conds_from_testfiledb('upgrade_DC19_01_MinBiasMD')
options.evt_max = 1000
options.print_freq = 100
options.ntuple_file = "rate_ntuple_bnoc_hlt1_and_hlt2.root"

# TODO stateProvider_with_simplified_geom must go away from option files
from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(from_file=False):
    run_chained_hlt_with_tuples(
        options, public_tools=[stateProvider_with_simplified_geom()])
