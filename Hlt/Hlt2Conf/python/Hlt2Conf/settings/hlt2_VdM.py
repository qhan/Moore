###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for the van der Meer scan in Nov. 2022
"""
from Moore.lines import Hlt2Line

from Moore.streams import Stream, Streams
from PyConf.Algorithms import OdinTypesFilter
from PyConf.application import make_odin


# has to be split in beam beam, eb, be, and ee
def _hlt2_lumi_bb_line(name='Hlt2LumiBeamBeam', prescale=1):
    odinFilter = OdinTypesFilter(ODIN=make_odin(), BXTypes=['BeamCrossing'])
    return Hlt2Line(
        name=name,
        algs=[odinFilter],
        hlt1_filter_code="Hlt1ODINLumiDecision",
        prescale=prescale)


def _hlt2_lumi_be_line(name='Hlt2LumiBeamEmpty', prescale=0.1):
    odinFilter = OdinTypesFilter(ODIN=make_odin(), BXTypes=['Beam1'])
    return Hlt2Line(
        name=name,
        algs=[odinFilter],
        hlt1_filter_code="Hlt1ODINLumiDecision",
        prescale=prescale)


def _hlt2_lumi_eb_line(name='Hlt2LumiEmptyBeam', prescale=0.1):
    odinFilter = OdinTypesFilter(ODIN=make_odin(), BXTypes=['Beam2'])
    return Hlt2Line(
        name=name,
        algs=[odinFilter],
        hlt1_filter_code="Hlt1ODINLumiDecision",
        prescale=prescale)


def _hlt2_lumi_ee_line(name='Hlt2LumiEmptyEmpty', prescale=1):
    odinFilter = OdinTypesFilter(ODIN=make_odin(), BXTypes=['NoBeam'])
    return Hlt2Line(
        name=name,
        algs=[odinFilter],
        hlt1_filter_code="Hlt1ODINLumiDecision",
        prescale=prescale)


def _hlt2_beamgas_line(name="Hlt2BeamGas", prescale=1):
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1BGI.*Decision",
        prescale=prescale)


def _hlt2_hlt1passthrough_line(name="Hlt2Hlt1PassThrough", prescale=1):
    '''
      Anything else than the beamgas or lumi lines
    '''
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code=r"^Hlt1(?!ODINLumi|BGI).*Decision",
        prescale=prescale)


def make_streams():
    beamgas_lines = [
        _hlt2_beamgas_line(),
        _hlt2_lumi_bb_line(name='Hlt2LumiBeamBeam_ForBGI', prescale=0.1),
        _hlt2_lumi_be_line(name='Hlt2LumiBeamEmpty_ForBGI', prescale=0.01),
        _hlt2_lumi_eb_line(name='Hlt2LumiEmptyBeam_ForBGI', prescale=0.01),
        _hlt2_lumi_ee_line(name='Hlt2LumiEmptyEmpty_ForBGI', prescale=0.1)
    ]
    full_lines = [_hlt2_hlt1passthrough_line()],
    lumi_lines = [
        _hlt2_lumi_bb_line(),
        _hlt2_lumi_be_line(),
        _hlt2_lumi_eb_line(),
        _hlt2_lumi_ee_line()
    ]

    beamgas_stream = Stream(
        name="beamgas_stream",
        lines=beamgas_lines,
        routing_bit=86,
        detectors=['VP', 'FT', 'Rich', 'Muon', 'Calo', 'Plume'])

    full_stream = Stream(
        name="full_stream",
        lines=full_lines,
        routing_bit=87,
        detectors=['VP', 'FT', 'Rich', 'Muon', 'Calo', 'Plume'])

    lumi_stream = Stream(
        name="lumi_stream",
        lines=lumi_lines,
        routing_bit=93,
        detectors=['Plume'])

    return Streams(streams=[beamgas_stream, full_stream, lumi_stream])
