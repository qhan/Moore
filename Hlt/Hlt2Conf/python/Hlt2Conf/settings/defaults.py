###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable


@configurable
def get_default_hlt1_filter_code_for_hlt2(code=""):
    """ Helper function to make default Hlt1 decision filter for Hlt2 lines configurable

    When set to a non-empty string a Hlt1 decision filter corresponding to the given regex is applied to every Hlt2 line, if the line does not explicitly define a filter.
    """
    return code
