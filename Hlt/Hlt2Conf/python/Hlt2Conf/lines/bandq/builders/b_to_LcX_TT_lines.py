###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define a set of Lb -> Lc X combinations, with Lc decaying into L, L reconstructed using T tracks
"""
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_ismuon_long_muon
from Functors import require_all
from Functors.math import in_range
from Hlt2Conf.standard_particles import make_LambdaTT_gated, make_KsTT_gated
from .charged_hadrons import make_detached_pions
from Hlt2Conf.lines.b_to_open_charm.builders.d_builder import make_dsplus_to_kpkmpip
from RecoConf.reconstruction_objects import make_pvs
from GaudiKernel.SystemOfUnits import GeV, MeV
import Functors as F
from PyConf import configurable


##############################################################
# Builders the Lambda and KS combiners with tighter PID cuts #
##############################################################
@configurable
def make_EMDM_LambdaTT_CutTTPID(make_particles=make_LambdaTT_gated,
                                PIDe_max=5.,
                                p_PIDp_min=-5,
                                pi_PIDK_max=10,
                                name="bandq_EMDM_LambdaTT_CutTTPIDe_{hash}"):
    """
        Cut the PIDe variable to remove gamma conversion BKGs. Use it to reduce the bandwidth tension of high-rate lines. 
        """
    code = F.require_all(
        F.CHILD(1, F.PID_E) < PIDe_max,
        F.CHILD(1, F.PID_P) > p_PIDp_min,
        F.CHILD(2, F.PID_E) < PIDe_max,
        F.CHILD(2, F.PID_K) < pi_PIDK_max)
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


@configurable
def make_EMDM_KsTT_CutTTPID(make_particles=make_KsTT_gated,
                            PIDe_max=5.,
                            pi_PIDK_max=10,
                            name="bandq_EMDM_KsTT_CutTTPIDe_{hash}"):
    """
        Cut the PIDe variable to remove gamma conversion BKGs. Use it to reduce the bandwidth tension of high-rate lines. 
        """
    code = F.require_all(
        F.CHILD(1, F.PID_E) < PIDe_max,
        F.CHILD(1, F.PID_K) < pi_PIDK_max,
        F.CHILD(2, F.PID_E) < PIDe_max,
        F.CHILD(2, F.PID_K) < pi_PIDK_max)
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


########################################################
# Builders for Long-tracks and Long-track combinations #
########################################################


@configurable
def make_EMDM_detached_Pion_base(make_particles=make_detached_pions,
                                 p_min=1.5 * GeV,
                                 pt_min=150. * MeV,
                                 mipchi2_min=6.,
                                 pidk_max=-1.,
                                 name="bandq_EMDM_detached_Pion_base_{hash}"):
    """
        The baseline for long-track pions used in EMDM measurements in Lb->LcX, Lc->LX decays 
        """
    pvs = make_pvs()
    code = F.require_all(F.P > p_min, F.PT > pt_min,
                         F.MINIPCHI2(pvs) > mipchi2_min, F.PID_K < pidk_max)
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


@configurable
def make_EMDM_detached_Muon_base(make_particles=make_ismuon_long_muon,
                                 p_min=2. * GeV,
                                 pt_min=300. * MeV,
                                 mipchi2_min=9.,
                                 pidmu_min=0.,
                                 name="bandq_EMDM_detached_Muon_base_{hash}"):
    """
        The baseline for long-track muons used in EMDM measurements in Lb->LcX, Lc->LX decays
        """
    pvs = make_pvs()
    code = F.require_all(F.P > p_min, F.PT > pt_min,
                         F.MINIPCHI2(pvs) > mipchi2_min, F.PID_MU > pidmu_min)
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


@configurable
def make_EMDM_detached_bachPion_fromCharm(
        make_particles=make_EMDM_detached_Pion_base,
        p_min=5. * GeV,
        pt_min=500. * MeV,
        mipchi2_min=16.,
        pidk_max=0.,
        name="bandq_EMDM_detached_bachPion_fromCharm_{hash}"):
    """
        Builder for a long-track detached Pion from charm decay and not vertexing with any other particles.
        """
    pvs = make_pvs()
    code = F.require_all(F.P > p_min, F.PT > pt_min,
                         F.MINIPCHI2(pvs) > mipchi2_min, F.PID_K < pidk_max)
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


@configurable
def make_EMDM_detached_bachPion_fromBeauty(
        make_particles=make_EMDM_detached_Pion_base,
        p_min=5. * GeV,
        pt_min=500. * MeV,
        mipchi2_min=9.,
        pidk_max=0.,
        name="bandq_EMDM_detached_bachPion_fromBeauty_{hash}"):
    """
        Builder for a long-track detached Pion from beauty decay and not vertexing with any other particles.
        """
    pvs = make_pvs()
    code = F.require_all(F.P > p_min, F.PT > pt_min,
                         F.MINIPCHI2(pvs) > mipchi2_min, F.PID_K < pidk_max)
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


@configurable
def make_EMDM_detached_bachMuon(make_particles=make_EMDM_detached_Muon_base,
                                p_min=4. * GeV,
                                pt_min=500. * MeV,
                                mipchi2_min=9.,
                                pidmu_min=0.,
                                name="bandq_EMDM_detached_bachMuon_{hash}"):
    """
        Builder for a long-track detached Muon to build b Semileptonic decays, not vertexing with any other particles. 
        """
    pvs = make_pvs()
    code = F.require_all(F.P > p_min, F.PT > pt_min,
                         F.MINIPCHI2(pvs) > mipchi2_min, F.PID_MU > pidmu_min)
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


@configurable
def make_EMDM_detached_PipPimMu(make_pions=make_EMDM_detached_Pion_base,
                                make_muons=make_EMDM_detached_Muon_base,
                                chi2_doca_max=25.,
                                vtx_chi2pdof_max=20.,
                                am_max=3800. * MeV,
                                m_max=3700. * MeV,
                                bpvdls_min=4.,
                                descriptor="[a_1(1260)+ -> mu+ pi+ pi-]cc",
                                name="bandq_EMDM_detached_PipPimMu_{hash}"):
    """
    Combiner for a detached PiPiMu vertex. 
    Require a good quality vertex and a mass below m(b) - m(c). Significant flight distance from associate PV.
    Use it to build candidates of beauty -> charm PiPiMu Nu SL decays
    """
    pvs = make_pvs()
    combination_code = require_all(F.MASS < am_max,
                                   F.MAXDOCACHI2CUT(chi2_doca_max))
    vertex_code = require_all(F.MASS < m_max, F.CHI2DOF < vtx_chi2pdof_max,
                              F.BPVDLS(pvs) > bpvdls_min)
    return ParticleCombiner(
        name=name,
        Inputs=[make_muons(), make_pions(),
                make_pions()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_EMDM_detached_PipPipPim_fromBeauty(
        make_pions=make_EMDM_detached_Pion_base,
        chi2_doca_max=25.,
        vtx_chi2pdof_max=20.,
        am_max=3800. * MeV,
        m_max=3700. * MeV,
        bpvdls_min=4.,
        descriptor="[a_1(1260)+ -> pi+ pi+ pi-]cc",
        name="bandq_EMDM_detached_PipPipPim_fromBeauty_{hash}"):
    """
    Combiner for a detached PiPiPi vertex. 
    Require a good quality vertex and a mass below m(b) - m(c). Significant flight distance from associate PV.
    Use it to build candidates of beauty -> charm PiPiPi hadronic decays
    """
    pvs = make_pvs()
    combination_code = require_all(F.MASS < am_max,
                                   F.MAXDOCACHI2CUT(chi2_doca_max))
    vertex_code = require_all(F.MASS < m_max, F.CHI2DOF < vtx_chi2pdof_max,
                              F.BPVDLS(pvs) > bpvdls_min)
    return ParticleCombiner(
        name=name,
        Inputs=[make_pions(), make_pions(),
                make_pions()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_EMDM_detached_PipPipPim_fromCharm(
        make_pions=make_EMDM_detached_Pion_base,
        chi2_doca_max=25.,
        vtx_chi2pdof_max=20.,
        am_max=1600. * MeV,
        m_max=1500. * MeV,
        bpvdls_min=5.,
        descriptor="[a_1(1260)+ -> pi+ pi+ pi-]cc",
        name="bandq_EMDM_detached_PipPipPim_fromCharm_{hash}"):
    """
        Combiner for a detached PiPiPi vertex. 
        Require a good quality vertex and a mass below m(lc) - m(lambda) and m(D) - m(Ks). Significant flight distance from associate PV.
        Use it to build candidates of beauty -> charm PiPiPi hadronic decays
        """
    pvs = make_pvs()
    combination_code = require_all(F.MASS < am_max,
                                   F.MAXDOCACHI2CUT(chi2_doca_max))
    vertex_code = require_all(F.MASS < m_max, F.CHI2DOF < vtx_chi2pdof_max,
                              F.BPVDLS(pvs) > bpvdls_min)
    return ParticleCombiner(
        name=name,
        Inputs=[make_pions(), make_pions(),
                make_pions()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


################################
# Builders of charm candidates #
################################


@configurable
def make_EMDM_Lc2LambdaPiTT(
        make_lambda=make_LambdaTT_gated,
        make_pions=make_EMDM_detached_bachPion_fromCharm,
        mass_combination_min=1.5 * GeV,  #combination cuts
        mass_combination_max=4 * GeV,
        docachi2_max=30.,
        mass_vertex_min=2. * GeV,  # vertex cuts
        mass_vertex_max=2.75 * GeV,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        name="bandq_EMDM_Lc2LambdaPiTT_{hash}"):
    '''
    Combiner for Lc->Lambda(->p pi) pi, where Lambda reconstructed using only Scifi tracks
    '''
    decay_descriptor = "[Lambda_c+ -> Lambda0 pi+]cc"

    lambdas = make_lambda(
    )  #To be updated when centrally-defined Long track Lambda becomes available

    pions = make_pions()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACHI2CUT(docachi2_max))

    vertex_cuts = F.require_all(
        in_range(mass_vertex_min, F.MASS, mass_vertex_max),
        F.CHI2 < vertex_chi2_max,
        F.PT > pt_min,
    )

    return ParticleCombiner(
        Inputs=[lambdas, pions],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_Lc2LambdaPiTT_CutTTPID(
        name="bandq_EMDM_Lc2LambdaPiTT_CutTTPID_{hash}"):
    return make_EMDM_Lc2LambdaPiTT(
        make_lambda=make_EMDM_LambdaTT_CutTTPID, name=name)


@configurable
def make_EMDM_Lc2Lambda3PiTT(
        make_lambda=make_LambdaTT_gated,
        make_3pions=make_EMDM_detached_PipPipPim_fromCharm,
        mass_combination_min=1.5 * GeV,  #combination cuts
        mass_combination_max=4 * GeV,
        docachi2_max=30.,
        mass_vertex_min=2. * GeV,  # vertex cuts
        mass_vertex_max=2.75 * GeV,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        name="make_bandq_Lc2Lambda3PiTT_{hash}"):
    '''
        Combiner for Lc->Lambda(->p pi) 3pi, where Lambda reconstructed using only Scifi tracks
        '''
    decay_descriptor = "[Lambda_c+ -> a_1(1260)+  Lambda0]cc"

    lambdas = make_lambda()

    my3pions = make_3pions()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACHI2CUT(docachi2_max))

    vertex_cuts = F.require_all(
        in_range(mass_vertex_min, F.MASS, mass_vertex_max),
        F.CHI2 < vertex_chi2_max,
        F.PT > pt_min,
    )

    return ParticleCombiner(
        Inputs=[my3pions, lambdas],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_Lc2Lambda3PiTT_CutTTPID(
        name="bandq_EMDM_Lc2Lambda3PiTT_CutTTPID_{hash}"):
    return make_EMDM_Lc2Lambda3PiTT(
        make_lambda=make_EMDM_LambdaTT_CutTTPID, name=name)


@configurable
def make_EMDM_D2KSPiTT(
        make_kshort=make_KsTT_gated,
        make_pions=make_EMDM_detached_bachPion_fromCharm,
        mass_combination_min=1.1 * GeV,  #combination cuts
        mass_combination_max=3.6 * GeV,
        docachi2_max=30.,
        mass_vertex_min=1.55 * GeV,  # vertex cuts
        mass_vertex_max=2.35 * GeV,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        name="bandq_EMDM_D2KSPiTT_{hash}"):
    '''
        Combiner for D->KS(->pi pi) pi, where Lambda reconstructed using only Scifi tracks
        '''
    decay_descriptor = "[D+ -> KS0 pi+]cc"

    kshorts = make_kshort()

    pions = make_pions()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACHI2CUT(docachi2_max))

    vertex_cuts = F.require_all(
        in_range(mass_vertex_min, F.MASS, mass_vertex_max),
        F.CHI2 < vertex_chi2_max,
        F.PT > pt_min,
    )

    return ParticleCombiner(
        Inputs=[kshorts, pions],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_D2KSPiTT_CutTTPID(name="bandq_EMDM_D2KSPiTT_CutTTPID_{hash}"):
    return make_EMDM_D2KSPiTT(make_kshort=make_EMDM_KsTT_CutTTPID, name=name)


@configurable
def make_EMDM_D2KS3PiTT(
        make_kshort=make_KsTT_gated,
        make_3pions=make_EMDM_detached_PipPipPim_fromCharm,
        mass_combination_min=1.1 * GeV,  #combination cuts
        mass_combination_max=3.6 * GeV,
        docachi2_max=30.,
        mass_vertex_min=1.55 * GeV,  # vertex cuts
        mass_vertex_max=2.35 * GeV,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        name="make_bandq_D2KS3PiTT_{hash}"):
    '''
        Combiner for D->KS(->pi pi) 3pi, where Lambda reconstructed using only Scifi tracks
        '''
    decay_descriptor = "[D+ -> a_1(1260)+  KS0]cc"

    kshorts = make_kshort()

    my3pions = make_3pions()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACHI2CUT(docachi2_max))

    vertex_cuts = F.require_all(
        in_range(mass_vertex_min, F.MASS, mass_vertex_max),
        F.CHI2 < vertex_chi2_max,
        F.PT > pt_min,
    )

    return ParticleCombiner(
        Inputs=[my3pions, kshorts],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_D2KS3PiTT_CutTTPID(name="bandq_EMDM_D2KS3PiTT_CutTTPID_{hash}"):
    return make_EMDM_D2KS3PiTT(make_kshort=make_EMDM_KsTT_CutTTPID, name=name)


######################################
# Builders of the b-decay candidates #
######################################


@configurable
def make_EMDM_Lb2LcPi_Lc2LambdaPi_TT(
        make_lc=make_EMDM_Lc2LambdaPiTT_CutTTPID,
        make_pion=make_EMDM_detached_bachPion_fromBeauty,
        make_pvs=make_pvs,
        mass_combination_min=4.9 * GeV,
        mass_combination_max=6.4 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_min=5.2 * GeV,
        mass_vertex_max=6.1 * GeV,
        name="bandq_EMDM_Lb2LcPi_Lc2LambdaPi_TT_{hash}"):
    '''
    Lb -> Lc(->Lambda Pi) Pi combiner, where the Lambda0 decays after the UT.
    '''
    decay_descriptor = "[Lambda_b0 -> Lambda_c+ pi-]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, in_range(mass_vertex_min, F.MASS, mass_vertex_max))

    return ParticleCombiner(
        Inputs=[make_lc(), make_pion()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_B2DPi_D2KSPi_TT(make_D=make_EMDM_D2KSPiTT_CutTTPID,
                              make_pion=make_EMDM_detached_bachPion_fromBeauty,
                              make_pvs=make_pvs,
                              mass_combination_min=4.5 * GeV,
                              mass_combination_max=6.0 * GeV,
                              bpvdira_min=0.985,
                              bpvip_max=0.2,
                              bpvipchi2_max=150.,
                              vertex_chi2_max=50.,
                              pt_min=2000 * MeV,
                              mass_vertex_min=4.8 * GeV,
                              mass_vertex_max=5.8 * GeV,
                              name="bandq_EMDM_B2DPi_D2KSPi_TT_{hash}"):
    '''
    B0 -> D+(->KS0 Pi+) Pi- combiner, where the KS0 decays after the UT.
    '''
    decay_descriptor = "[B0 -> D+ pi-]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, in_range(mass_vertex_min, F.MASS, mass_vertex_max))

    return ParticleCombiner(
        Inputs=[make_D(), make_pion()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_Lb2LcPi_Lc2Lambda3Pi_TT(
        make_lc=make_EMDM_Lc2Lambda3PiTT_CutTTPID,
        make_pion=make_EMDM_detached_bachPion_fromBeauty,
        make_pvs=make_pvs,
        mass_combination_min=4.9 * GeV,
        mass_combination_max=6.4 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_min=5.2 * GeV,
        mass_vertex_max=6.1 * GeV,
        name="make_EMDM_Lb2LcPi_Lc2Lambda3Pi_TT_{hash}"):
    '''
    Lb -> Lc(->Lambda 3Pi) Pi combiner, where the Lambda0 decays after the UT.
    '''
    decay_descriptor = "[Lambda_b0 -> Lambda_c+ pi-]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, in_range(mass_vertex_min, F.MASS, mass_vertex_max))

    return ParticleCombiner(
        Inputs=[make_lc(), make_pion()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_B2DPi_D2KS3Pi_TT(
        make_D=make_EMDM_D2KS3PiTT_CutTTPID,
        make_pion=make_EMDM_detached_bachPion_fromBeauty,
        make_pvs=make_pvs,
        mass_combination_min=4.5 * GeV,
        mass_combination_max=6.0 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_min=4.8 * GeV,
        mass_vertex_max=5.8 * GeV,
        name="bandq_EMDM_B2DPi_D2KS3Pi_TT_{hash}"):
    '''
    B0 -> D+(->KS0 3Pi) Pi- combiner, where the KS0 decays after the UT.
    '''
    decay_descriptor = "[B0 -> D+ pi-]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, in_range(mass_vertex_min, F.MASS, mass_vertex_max))

    return ParticleCombiner(
        Inputs=[make_D(), make_pion()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_Lb2Lc3Pi_Lc2LambdaPi_TT(
        make_lc=make_EMDM_Lc2LambdaPiTT_CutTTPID,
        make_3pion=make_EMDM_detached_PipPipPim_fromBeauty,
        make_pvs=make_pvs,
        mass_combination_min=4.9 * GeV,
        mass_combination_max=6.4 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_min=5.2 * GeV,
        mass_vertex_max=6.1 * GeV,
        name="bandq_EMDM_Lb2Lc3Pi_Lc2LambdaPi_TT_{hash}"):
    '''
    Lb -> Lc(->Lambda Pi) 3Pi combiner, where the Lambda0 decays after the UT.
    '''
    decay_descriptor = "[Lambda_b0 -> Lambda_c+ a_1(1260)-]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, in_range(mass_vertex_min, F.MASS, mass_vertex_max))

    return ParticleCombiner(
        Inputs=[make_3pion(), make_lc()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_B2D3Pi_D2KSPi_TT(
        make_D=make_EMDM_D2KSPiTT_CutTTPID,
        make_3pion=make_EMDM_detached_PipPipPim_fromBeauty,
        make_pvs=make_pvs,
        mass_combination_min=4.5 * GeV,
        mass_combination_max=6.0 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_min=4.8 * GeV,
        mass_vertex_max=5.8 * GeV,
        name="bandq_EMDM_B2D3Pi_D2KSPi_TT_{hash}"):
    '''
    B0 -> D+(->KS0 Pi+) 3Pi combiner, where the KS0 decays after the UT.
    '''
    decay_descriptor = "[B0 -> a_1(1260)- D+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, in_range(mass_vertex_min, F.MASS, mass_vertex_max))

    return ParticleCombiner(
        Inputs=[make_3pion(), make_D()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_Lb2Lc3Pi_Lc2Lambda3Pi_TT(
        make_lc=make_EMDM_Lc2Lambda3PiTT_CutTTPID,
        make_3pion=make_EMDM_detached_PipPipPim_fromBeauty,
        make_pvs=make_pvs,
        mass_combination_min=4.9 * GeV,
        mass_combination_max=6.4 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_min=5.2 * GeV,
        mass_vertex_max=6.1 * GeV,
        name="make_EMDM_Lb2Lc3Pi_Lc2Lambda3Pi_TT_{hash}"):
    '''
    Lb -> Lc(->Lambda 3Pi) 3Pi combiner, where the Lambda0 decays after the UT.
    '''
    decay_descriptor = "[Lambda_b0 -> a_1(1260)- Lambda_c+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, in_range(mass_vertex_min, F.MASS, mass_vertex_max))

    return ParticleCombiner(
        Inputs=[make_3pion(), make_lc()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_B2D3Pi_D2KS3Pi_TT(
        make_D=make_EMDM_D2KS3PiTT_CutTTPID,
        make_3pion=make_EMDM_detached_PipPipPim_fromBeauty,
        make_pvs=make_pvs,
        mass_combination_min=4.5 * GeV,
        mass_combination_max=6.0 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_min=4.8 * GeV,
        mass_vertex_max=5.8 * GeV,
        name="bandq_EMDM_B2D3Pi_D2KS3Pi_TT_{hash}"):
    '''
    B0 -> D+(->KS0 3Pi) 3Pi combiner, where the KS0 decays after the UT.
    '''
    decay_descriptor = "[B0 -> a_1(1260)- D+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, in_range(mass_vertex_min, F.MASS, mass_vertex_max))

    return ParticleCombiner(
        Inputs=[make_3pion(), make_D()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_Lb2LcDs_Lc2LambdaPi_TT(
        make_lc=make_EMDM_Lc2LambdaPiTT_CutTTPID,
        make_ds=make_dsplus_to_kpkmpip,  #taken from B2OC. A standard detached ds builder
        make_pvs=make_pvs,
        mass_combination_min=4.9 * GeV,
        mass_combination_max=6.4 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_min=5.2 * GeV,
        mass_vertex_max=6.1 * GeV,
        name="bandq_EMDM_Lb2LcDs_Lc2LambdaPi_TT_{hash}"):
    '''
    Lb -> Lc(->Lambda Pi) Ds+(->KKpi) combiner, where the Lambda0 decays after the UT.
    '''
    decay_descriptor = "[Lambda_b0 -> D_s- Lambda_c+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, in_range(mass_vertex_min, F.MASS, mass_vertex_max))

    return ParticleCombiner(
        Inputs=[make_ds(), make_lc()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_B2DDs_D2KSPi_TT(
        make_D=make_EMDM_D2KSPiTT_CutTTPID,
        make_ds=make_dsplus_to_kpkmpip,  #taken from B2OC. A standard detached ds builder
        make_pvs=make_pvs,
        mass_combination_min=4.5 * GeV,
        mass_combination_max=6.0 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_min=4.8 * GeV,
        mass_vertex_max=5.8 * GeV,
        name="bandq_EMDM_B2DDs_D2KSPi_TT_{hash}"):
    '''
    B -> D(->KS Pi) Ds+(->KKpi) combiner, where the KS decays after the UT.
    '''
    decay_descriptor = "[B0 -> D_s- D+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.CHI2 < vertex_chi2_max,
        F.PT > pt_min,
        in_range(mass_vertex_min, F.MASS, mass_vertex_max),
    )

    return ParticleCombiner(
        Inputs=[make_ds(), make_D()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_Lb2LcDs_Lc2Lambda3Pi_TT(
        make_lc=make_EMDM_Lc2Lambda3PiTT_CutTTPID,
        make_ds=make_dsplus_to_kpkmpip,  #taken from B2OC. A standard detached ds builder
        make_pvs=make_pvs,
        mass_combination_min=4.9 * GeV,
        mass_combination_max=6.4 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_min=5.2 * GeV,
        mass_vertex_max=6.1 * GeV,
        name="bandq_EMDM_Lb2LcDs_Lc2Lambda3Pi_TT_{hash}"):
    '''
    Lb -> Lc(->Lambda 3Pi) Ds+(->KKpi) combiner, where the Lambda0 decays after the UT.
    '''
    decay_descriptor = "[Lambda_b0 -> D_s- Lambda_c+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.CHI2 < vertex_chi2_max,
        F.PT > pt_min,
        in_range(mass_vertex_min, F.MASS, mass_vertex_max),
    )

    return ParticleCombiner(
        Inputs=[make_ds(), make_lc()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_B2DDs_D2KS3Pi_TT(
        make_D=make_EMDM_D2KS3PiTT_CutTTPID,
        make_ds=make_dsplus_to_kpkmpip,  #taken from B2OC. A standard detached ds builder
        make_pvs=make_pvs,
        mass_combination_min=4.5 * GeV,
        mass_combination_max=6.0 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_min=4.8 * GeV,
        mass_vertex_max=5.8 * GeV,
        name="bandq_EMDM_B2DDs_D2KS3Pi_TT_{hash}"):
    '''
    B -> D(->KS 3Pi) Ds+(->KKpi) combiner, where the KS decays after the UT.
    '''
    decay_descriptor = "[B0 -> D_s- D+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.CHI2 < vertex_chi2_max,
        F.PT > pt_min,
        in_range(mass_vertex_min, F.MASS, mass_vertex_max),
    )

    return ParticleCombiner(
        Inputs=[make_ds(), make_D()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_Lb2LcMuNu_Lc2LambdaPi_TT(
        make_lc=make_EMDM_Lc2LambdaPiTT_CutTTPID,
        make_muon=make_EMDM_detached_bachMuon,
        make_pvs=make_pvs,
        mass_combination_max=6.4 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_max=6.1 * GeV,
        name="bandq_EMDM_Lb2LcMuNu_Lc2LambdaPi_TT_{hash}"):
    ''' 
    Lb -> Lc(->Lambda Pi) Mu combiner, where the Lambda0 decays after the UT.
    '''
    decay_descriptor = "[Lambda_b0 -> mu- Lambda_c+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        F.MASS < mass_combination_max,
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, F.MASS < mass_vertex_max)

    return ParticleCombiner(
        Inputs=[make_muon(), make_lc()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_B2DMuNu_D2KSPi_TT(make_D=make_EMDM_D2KSPiTT_CutTTPID,
                                make_muon=make_EMDM_detached_bachMuon,
                                make_pvs=make_pvs,
                                mass_combination_max=6.0 * GeV,
                                bpvdira_min=0.985,
                                bpvip_max=0.2,
                                bpvipchi2_max=150.,
                                vertex_chi2_max=50.,
                                pt_min=2000 * MeV,
                                mass_vertex_max=5.8 * GeV,
                                name="bandq_EMDM_B2DMuNu_D2KSPi_TT_{hash}"):
    '''
    B0 -> D+(->KS0 Pi+) Mu- combiner, where the KS0 decays after the UT.
    '''
    decay_descriptor = "[B0 -> mu- D+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        F.MASS < mass_combination_max,
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, F.MASS < mass_vertex_max)

    return ParticleCombiner(
        Inputs=[make_muon(), make_D()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_Lb2LcPiPiMuNu_Lc2LambdaPi_TT(
        make_lc=make_EMDM_Lc2LambdaPiTT_CutTTPID,
        make_pipimu=make_EMDM_detached_PipPimMu,
        make_pvs=make_pvs,
        mass_combination_max=6.4 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_max=6.1 * GeV,
        name="bandq_EMDM_Lb2LcPiPiMuNu_Lc2LambdaPi_TT_{hash}"):
    ''' 
    Lb -> Lc(->Lambda Pi) Pi Pi Mu combiner, where the Lambda0 decays after the UT.
    '''
    decay_descriptor = "[Lambda_b0 -> a_1(1260)- Lambda_c+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        F.MASS < mass_combination_max,
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, F.MASS < mass_vertex_max)

    return ParticleCombiner(
        Inputs=[make_pipimu(), make_lc()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_B2DPiPiMuNu_D2KSPi_TT(
        make_D=make_EMDM_D2KSPiTT_CutTTPID,
        make_pipimu=make_EMDM_detached_PipPimMu,
        make_pvs=make_pvs,
        mass_combination_max=6.0 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_max=5.8 * GeV,
        name="bandq_EMDM_B2DPiPiMuNu_D2KSPi_TT_{hash}"):
    '''
    B0 -> D+(->KS0 Pi+) Pi Pi Mu- combiner, where the KS0 decays after the UT.
    '''
    decay_descriptor = "[B0 -> a_1(1260)- D+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        F.MASS < mass_combination_max,
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, F.MASS < mass_vertex_max)

    return ParticleCombiner(
        Inputs=[make_pipimu(), make_D()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_Lb2LcMuNu_Lc2Lambda3Pi_TT(
        make_lc=make_EMDM_Lc2Lambda3PiTT_CutTTPID,
        make_muon=make_EMDM_detached_bachMuon,
        make_pvs=make_pvs,
        mass_combination_max=6.4 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_max=6.1 * GeV,
        name="bandq_EMDM_Lb2LcMuNu_Lc2Lambda3Pi_TT_{hash}"):
    ''' 
    Lb -> Lc(->Lambda 3Pi) Mu combiner, where the Lambda0 decays after the UT.
    '''
    decay_descriptor = "[Lambda_b0 -> mu- Lambda_c+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        F.MASS < mass_combination_max,
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, F.MASS < mass_vertex_max)

    return ParticleCombiner(
        Inputs=[make_muon(), make_lc()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_B2DMuNu_D2KS3Pi_TT(make_D=make_EMDM_D2KS3PiTT_CutTTPID,
                                 make_muon=make_EMDM_detached_bachMuon,
                                 make_pvs=make_pvs,
                                 mass_combination_max=6.0 * GeV,
                                 bpvdira_min=0.985,
                                 bpvip_max=0.2,
                                 bpvipchi2_max=150.,
                                 vertex_chi2_max=50.,
                                 pt_min=2000 * MeV,
                                 mass_vertex_max=5.8 * GeV,
                                 name="bandq_EMDM_B2DMuNu_D2KS3Pi_TT_{hash}"):
    '''
    B0 -> D+(->KS0 Pi+) Mu- combiner, where the KS0 decays after the UT.
    '''
    decay_descriptor = "[B0 -> mu- D+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        F.MASS < mass_combination_max,
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, F.MASS < mass_vertex_max)

    return ParticleCombiner(
        Inputs=[make_muon(), make_D()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_Lb2LcPiPiMuNu_Lc2Lambda3Pi_TT(
        make_lc=make_EMDM_Lc2Lambda3PiTT_CutTTPID,
        make_pipimu=make_EMDM_detached_PipPimMu,
        make_pvs=make_pvs,
        mass_combination_max=6.4 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_max=6.1 * GeV,
        name="bandq_EMDM_Lb2LcPiPiMuNu_Lc2Lambda3Pi_TT_{hash}"):
    ''' 
    Lb -> Lc(->Lambda Pi) Pi Pi Mu combiner, where the Lambda0 decays after the UT.
    '''
    decay_descriptor = "[Lambda_b0 -> a_1(1260)- Lambda_c+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        F.MASS < mass_combination_max,
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, F.MASS < mass_vertex_max)

    return ParticleCombiner(
        Inputs=[make_pipimu(), make_lc()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_EMDM_B2DPiPiMuNu_D2KS3Pi_TT(
        make_D=make_EMDM_D2KS3PiTT_CutTTPID,
        make_pipimu=make_EMDM_detached_PipPimMu,
        make_pvs=make_pvs,
        mass_combination_max=6.0 * GeV,
        bpvdira_min=0.985,
        bpvip_max=0.2,
        bpvipchi2_max=150.,
        vertex_chi2_max=50.,
        pt_min=2000 * MeV,
        mass_vertex_max=5.8 * GeV,
        name="bandq_EMDM_B2DPiPiMuNu_D2KS3Pi_TT_{hash}"):
    '''
    B0 -> D+(->KS0 Pi+) Pi Pi Mu- combiner, where the KS0 decays after the UT.
    '''
    decay_descriptor = "[B0 -> a_1(1260)- D+]cc"

    pvs = make_pvs()

    combination_cuts = F.require_all(
        F.MASS < mass_combination_max,
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.CHI2 < vertex_chi2_max,
        F.PT > pt_min, F.MASS < mass_vertex_max)

    return ParticleCombiner(
        Inputs=[make_pipimu(), make_D()],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)
