###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
A standalone builder for exclusive B+->Jpsi K+, B0->Jpsi K+ pi-, Bs->Jpsi K+ K- decays. 
Do not cut on MUON-station-based MuonID info. 
Use the line to perform data-driven check of MuonID performance.
Tight kinematic cuts to make rate under control. 
"""
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Functors import require_all
from PyConf import configurable

from Hlt2Conf.standard_particles import make_long_muons

import Functors as F
from Functors.math import in_range

from Hlt2Conf.lines.bandq.builders.charged_hadrons import make_detached_kaons, make_detached_pions


@configurable
def make_NoMuonID_muons(
        make_particles=make_long_muons,
        name="bandq_NoMuonID_muons_{hash}",
        pt_min=300. * MeV,
        p_min=4 * GeV,
        p_max=200. * GeV,
        eta_min=2.,
        eta_max=5.,
        mipchi2dvprimary_min=9,
        #pid=None):
        pid=require_all(F.RICH_DLL_MU > -10,
                        F.RICH_DLL_MU - F.RICH_DLL_K > -5.)):

    pvs = make_pvs()

    code = require_all(F.PT > pt_min, in_range(p_min, F.P, p_max),
                       in_range(eta_min, F.ETA, eta_max),
                       F.MINIPCHI2(pvs) > mipchi2dvprimary_min)

    if (pid is not None):
        code &= pid

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_detached_kaons_forB2JpsiX_NoMuonID(
        make_particles=make_detached_kaons,
        name="bandq_detached_kaons_forB2JpsiX_NoMuonID_{hash}",
        pt_min=300. * MeV,
        p_min=4 * GeV,
        p_max=200. * GeV,
        eta_min=2.,
        eta_max=5.,
        mipchi2dvprimary_min=9,
        pid=(F.PID_K > 5.)):
    pvs = make_pvs()

    code = require_all(F.PT > pt_min, in_range(p_min, F.P, p_max),
                       in_range(eta_min, F.ETA, eta_max),
                       F.MINIPCHI2(pvs) > mipchi2dvprimary_min)

    if (pid is not None):
        code &= pid

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_detached_pions_forB2JpsiX_NoMuonID(
        make_particles=make_detached_pions,
        name="bandq_detached_pions_forB2JpsiX_NoMuonID_{hash}",
        pt_min=200. * MeV,
        p_min=3 * GeV,
        p_max=200. * GeV,
        eta_min=2.,
        eta_max=5.,
        mipchi2dvprimary_min=9,
        pid=(F.PID_K < -5.)):
    pvs = make_pvs()

    code = require_all(F.PT > pt_min, in_range(p_min, F.P, p_max),
                       in_range(eta_min, F.ETA, eta_max),
                       F.MINIPCHI2(pvs) > mipchi2dvprimary_min)

    if (pid is not None):
        code &= pid

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_detachedKPi_forB2JpsiX_NoMuonID(
        kaons=make_detached_kaons_forB2JpsiX_NoMuonID,
        pions=make_detached_pions_forB2JpsiX_NoMuonID,
        name="bandq_detachedKPi_forB2JpsiX_NoMuonID_{hash}",
        descriptor='[K*(892)0 -> K+ pi-]cc',
        am_max=2500 * MeV,
        maxdocachi2=20.,
        m_max=2450. * MeV,
        bpvdls_min=3):

    pvs = make_pvs()

    combination_code = F.require_all(F.MASS < am_max,
                                     F.MAXDOCACHI2CUT(maxdocachi2))

    vertex_code = F.require_all(F.MASS < m_max, F.BPVDLS(pvs) > bpvdls_min)

    return ParticleCombiner(
        name=name,
        Inputs=[kaons(), pions()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_detachedKst_forB2JpsiX_NoMuonID(
        KPi_comb=make_detachedKPi_forB2JpsiX_NoMuonID,
        name="bandq_detachedKst_forB2JpsiX_NoMuonID_{hash}",
        m_min=800. * MeV,
        m_max=1000. * MeV):

    code = in_range(m_min, F.MASS, m_max)

    return ParticleFilter(KPi_comb(), name=name, Cut=F.FILTER(code))


@configurable
def make_detachedKK_forB2JpsiX_NoMuonID(
        kaons=make_detached_kaons_forB2JpsiX_NoMuonID,
        name="bandq_detachedKK_forB2JpsiX_NoMuonID_{hash}",
        descriptor='phi(1020) -> K+ K-',
        am_max=2500 * MeV,
        maxdocachi2=20.,
        m_max=2450. * MeV,
        bpvdls_min=3):

    pvs = make_pvs()

    combination_code = F.require_all(F.MASS < am_max,
                                     F.MAXDOCACHI2CUT(maxdocachi2))

    vertex_code = F.require_all(F.MASS < m_max, F.BPVDLS(pvs) > bpvdls_min)

    return ParticleCombiner(
        name=name,
        Inputs=[kaons(), kaons()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_detachedPhi_forB2JpsiX_NoMuonID(
        KK_comb=make_detachedKK_forB2JpsiX_NoMuonID,
        name="bandq_detachedPhi_forB2JpsiX_NoMuonID_{hash}",
        m_max=1300. * MeV):

    code = (F.MASS < m_max)

    return ParticleFilter(KK_comb(), name=name, Cut=F.FILTER(code))


@configurable
def make_Jpsi_NoMuonID(muons=make_NoMuonID_muons,
                       name="bandq_Jpsi_NoMuonID_{hash}",
                       descriptor='J/psi(1S) -> mu+ mu-',
                       am_min=2996. * MeV,
                       am_max=3196. * MeV,
                       apt_min=600 * MeV,
                       maxdocachi2=20.,
                       m_min=3036. * MeV,
                       m_max=3156. * MeV,
                       pt_min=1000 * MeV,
                       bpvdls_min=3):

    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(maxdocachi2),
        (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), (F.PT > pt_min),
        F.BPVDLS(pvs) > bpvdls_min)

    return ParticleCombiner(
        name=name,
        Inputs=[muons(), muons()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_Bp2JpsiKp_NoMuonID(Jpsi=make_Jpsi_NoMuonID,
                            kaons=make_detached_kaons_forB2JpsiX_NoMuonID,
                            name="bandq_Bp2JpsiKp_NoMuonID_{hash}",
                            descriptor='[B+ -> J/psi(1S) K+]cc',
                            am_min=5190. * MeV,
                            am_max=5370. * MeV,
                            apt_min=1200 * MeV,
                            m_min=5200. * MeV,
                            m_max=5360. * MeV,
                            pt_min=1400 * MeV,
                            vtx_chi2pdof_max=16,
                            bpvltime_min=0.2 * picosecond):

    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vtx_chi2pdof_max,
        (F.PT > pt_min),
        F.BPVLTIME(pvs) > bpvltime_min)

    return ParticleCombiner(
        name=name,
        Inputs=[Jpsi(), kaons()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_Bz2JpsiKpPim_NoMuonID(Jpsi=make_Jpsi_NoMuonID,
                               KPi_comb=make_detachedKPi_forB2JpsiX_NoMuonID,
                               name="bandq_Bz2JpsiKpPim_NoMuonID_{hash}",
                               descriptor='[B0 -> K*(892)0 J/psi(1S)]cc',
                               am_min=5190. * MeV,
                               am_max=5370. * MeV,
                               apt_min=1200 * MeV,
                               m_min=5200. * MeV,
                               m_max=5360. * MeV,
                               pt_min=1400 * MeV,
                               vtx_chi2pdof_max=16,
                               bpvltime_min=0.2 * picosecond):

    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vtx_chi2pdof_max,
        (F.PT > pt_min),
        F.BPVLTIME(pvs) > bpvltime_min)

    return ParticleCombiner(
        name=name,
        Inputs=[KPi_comb(), Jpsi()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_Bz2JpsiKst_NoMuonID(Jpsi=make_Jpsi_NoMuonID,
                             KPi_comb=make_detachedKst_forB2JpsiX_NoMuonID,
                             name="bandq_Bz2JpsiKst_NoMuonID_{hash}",
                             descriptor='[B0 -> K*(892)0 J/psi(1S)]cc',
                             am_min=5190. * MeV,
                             am_max=5370. * MeV,
                             apt_min=1200 * MeV,
                             m_min=5200. * MeV,
                             m_max=5360. * MeV,
                             pt_min=1400 * MeV,
                             vtx_chi2pdof_max=16,
                             bpvltime_min=0.2 * picosecond):

    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vtx_chi2pdof_max,
        (F.PT > pt_min),
        F.BPVLTIME(pvs) > bpvltime_min)

    return ParticleCombiner(
        name=name,
        Inputs=[KPi_comb(), Jpsi()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_Bs2JpsiKpKm_NoMuonID(Jpsi=make_Jpsi_NoMuonID,
                              KK_comb=make_detachedKK_forB2JpsiX_NoMuonID,
                              name="bandq_Bs2JpsiKpKm_NoMuonID_{hash}",
                              descriptor='B_s0 -> phi(1020) J/psi(1S) ',
                              am_min=5280. * MeV,
                              am_max=5460. * MeV,
                              apt_min=1200 * MeV,
                              m_min=5290. * MeV,
                              m_max=5450. * MeV,
                              pt_min=1400 * MeV,
                              vtx_chi2pdof_max=16,
                              bpvltime_min=0.2 * picosecond):

    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vtx_chi2pdof_max,
        (F.PT > pt_min),
        F.BPVLTIME(pvs) > bpvltime_min)

    return ParticleCombiner(
        name=name,
        Inputs=[KK_comb(), Jpsi()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_Bs2JpsiPhi_NoMuonID(Jpsi=make_Jpsi_NoMuonID,
                             KK_comb=make_detachedPhi_forB2JpsiX_NoMuonID,
                             name="bandq_Bs2JpsiPhi_NoMuonID_{hash}",
                             descriptor='B_s0 -> phi(1020) J/psi(1S)',
                             am_min=5280. * MeV,
                             am_max=5460. * MeV,
                             apt_min=1200 * MeV,
                             m_min=5290. * MeV,
                             m_max=5450. * MeV,
                             pt_min=1400 * MeV,
                             vtx_chi2pdof_max=16,
                             bpvltime_min=0.2 * picosecond):

    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vtx_chi2pdof_max,
        (F.PT > pt_min),
        F.BPVLTIME(pvs) > bpvltime_min)

    return ParticleCombiner(
        name=name,
        Inputs=[KK_comb(), Jpsi()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)
