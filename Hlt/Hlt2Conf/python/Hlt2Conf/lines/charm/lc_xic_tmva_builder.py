###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of MVA tools for XLL lines
"""
from PyConf import configurable

import Functors as F
from Functors.math import log
from Hlt2Conf.algorithms_thor import ParticleFilter


@configurable
def _lc_xicp_BDT_functor(pvs, mva_name):
    lc_xic_vars = {
        'log(Pp_PT)':
        log(F.CHILD(1, F.PT)),
        'log(Km_PT)':
        log(F.CHILD(2, F.PT)),
        'log(Pip_PT)':
        log(F.CHILD(3, F.PT)),
        'log(Pp_PT+Km_PT+Pip_PT)':
        log(F.CHILD(1, F.PT) + F.CHILD(2, F.PT) + F.CHILD(3, F.PT)),
        'log(Lc_PT)':
        log(F.PT),
        'Pp_BPVIPCHI2':
        F.CHILD(1, F.BPVIPCHI2(pvs)),
        'Km_BPVIPCHI2':
        F.CHILD(2, F.BPVIPCHI2(pvs)),
        'Pip_BPVIPCHI2':
        F.CHILD(3, F.BPVIPCHI2(pvs)),
        'Pp_BPVIPCHI2+Km_BPVIPCHI2+Pip_BPVIPCHI2':
        F.CHILD(1, F.BPVIPCHI2(pvs)) + F.CHILD(2, F.BPVIPCHI2(pvs)) + F.CHILD(
            3, F.BPVIPCHI2(pvs)),
        'log(Lc_BPVFDCHI2)':
        log(F.BPVFDCHI2(pvs)),
        'Lc_VCHI2DOF':
        F.CHI2DOF
    }

    bdt_vars = {'lc_xic': lc_xic_vars}

    xml_files = {"lc_xic": 'paramfile://data/lc_xicp.xml'}

    return F.MVA(
        MVAType='TMVA',
        Config={
            'XMLFile': xml_files[mva_name],
            'Name': 'BDT',
        },
        Inputs=bdt_vars[mva_name])


@configurable
def make_cbaryon(presel_b, pvs, mva_name, bdt_cut, filter_name=None):
    code = _lc_xicp_BDT_functor(pvs, mva_name) > bdt_cut
    return ParticleFilter(presel_b, F.FILTER(code), name=filter_name)
