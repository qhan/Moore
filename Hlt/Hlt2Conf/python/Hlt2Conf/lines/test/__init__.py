# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines the generic ThOr HLT2 lines
"""

from . import hlt2_test

# provide "hlt2_test_lines" for correct registration by the overall HLT2 lines module
hlt2_test_lines = {}

hlt2_test_lines.update(hlt2_test.hlt2_test_lines)
