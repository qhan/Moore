# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This tightware is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of BnoC B -> p pbar (p pbar) lines 
"""
from Hlt2Conf.lines.bnoc.builders.basic_builder import (make_tight_protons)
from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders.b_builder import (make_bds2ppbarppbar,
                                                    make_bds2ppbar)


@check_process
def make_BdsToPpPmPpPm(process):
    proton = make_tight_protons()
    return make_bds2ppbarppbar([proton, proton, proton, proton],
                               name="BNOC_BdsToPpPmPpPmCombiner",
                               descriptor="B0 -> p+ p+ p~- p~-")


@check_process
def make_BdsToPpPm(process):
    proton = make_tight_protons()
    return make_bds2ppbar([proton, proton],
                          name="BNOC_BdsToPpPmCombiner",
                          descriptor="B0 -> p+ p~-")
