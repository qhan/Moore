###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf import configurable

from Hlt2Conf.standard_particles import make_long_pions, make_up_pions, make_down_pions, make_ttrack_pions, make_photons, make_merged_pi0s
from Hlt2Conf.isolation import extra_outputs_for_isolation
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleContainersMerger

import Functors as F


@configurable
def make_two_body_combination(candidate, head):
    """
    Make two body combination of head and extra particles (long tracks with pion id).

    Args:
    candidate: Containers of reference particles
    head: The head of the decay descriptor to build vertex isolation for.
        e.g. for [B0 -> K*(892)~0 rho(770)0]cc' use head='B0'. Required to build the
        '[X+ -> B0 pi+]cc' candidates correctly.

    Returns: TES location of two body combination of candidate and extra_particles
    """

    extra_particles = make_long_pions()

    #first combiner: X pi+
    comb_1 = ParticleCombiner(
        Inputs=[candidate, extra_particles],
        name='BNOC_vtxiso_combiner_onetrack_1_{hash}',
        DecayDescriptor=f'[B*+ -> {head} pi+]cc',
        CombinationCut=F.ALL,
        CompositeCut=F.ALL)
    #second combiner: X pi-
    comb_2 = ParticleCombiner(
        Inputs=[candidate, extra_particles],
        name='BNOC_vtxiso_combiner_onetrack_2_{hash}',
        DecayDescriptor=f'[B*- -> {head} pi-]cc',
        CombinationCut=F.ALL,
        CompositeCut=F.ALL)
    #merge two combiners
    comb = ParticleContainersMerger(
        [comb_1, comb_2], name='BNOC_vtxiso_combiner_onetrack_{hash}')

    return comb


@configurable
def make_three_body_combination(candidate, head):
    """
    Make three body combination of B0 and two extra particles (long tracks with pion id).

    Args:
    candidate: Containers of reference particles
    head: The head of the decay descriptor to build vertex isolation for.
        e.g. for [B0 -> K*(892)~0 rho(770)0]cc' use head='B0'. Required to build the
        '[X -> B0 pi+ pi-]cc' candidates correctly.

    Returns: TES location of three body combination of candidate and two extra_particles
    """

    extra_particles = make_long_pions()

    #combiner: X pi+ pi-
    comb = ParticleCombiner(
        Inputs=[candidate, extra_particles, extra_particles],
        name='BNOC_vtxiso_combiner_twotracks_{hash}',
        DecayDescriptor=f'[B*0 -> {head} pi+ pi-]cc',
        CombinationCut=F.ALL,
        CompositeCut=F.ALL)

    return comb


@configurable
def select_parts_for_isolation(
        names=[],
        candidates=[],
        cut=(F.DR2 < .5),
        LongTrackIso=True,
        TTrackIso=False,
        DownstreamTrackIso=False,
        UpstreamTrackIso=False,
        NeutralIso=True,
        PiZerosIso=False,
):
    """
    Add to the extra_outputs different kind of isolations by properly setting the given flag 
    Args:
        candidates: List of containers of reference particles to relate extra particles
        cut: Predicate to select extra information to persist. Be default: cone geometry with max dr2=1 and extra particles not signal
        LongTrackIso: Boolean value to make isolation with long tracks
        TTrackIso: Boolean value to make isolation with tt tracks
        DownstreamTrackIso: Boolean value to make isolation with downstream tracks
        UpstreamTrackIso: Boolean value to make isolation with upstream tracks
        NeutralIso: Boolean value to make isolation with neutral particles
        PiZerosIso: Boolean value to make isolation with merged pi0 -> gamma gamma
    """
    extra_outputs = []
    assert (
        len(names) == len(candidates)
    ), 'Different number of names and candidate containers for particle isolation!'

    for name, cand in zip(names, candidates):
        if LongTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_LongTrackIsolation",
                extra_particles=make_long_pions(),
                ref_particles=cand,
                selection=cut)
        if TTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_TTrackIsolation",
                extra_particles=make_ttrack_pions(),
                ref_particles=cand,
                selection=cut)
        if DownstreamTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_DownstreamTrackIsolation",
                extra_particles=make_down_pions(),
                ref_particles=cand,
                selection=cut)
        if UpstreamTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_UpstreamTrackIsolation",
                extra_particles=make_up_pions(),
                ref_particles=cand,
                selection=cut)
        if NeutralIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_NeutralIsolation",
                extra_particles=make_photons(),
                ref_particles=cand,
                selection=cut)
        if PiZerosIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_PiZerosIsolation",
                extra_particles=make_merged_pi0s(),
                ref_particles=cand,
                selection=cut)
    return extra_outputs


def select_combinations_for_vertex_isolation(candidate,
                                             head,
                                             name='B',
                                             max_two_body_vtx_cut=9.,
                                             max_three_body_vtx_cut=15.):
    """
    Add to the extra_outputs vertex isolations when adding one track or two tracks to the vertex fit.
    By default only two(three)-body combinations with vertex fit chi2 smaller than 9.(15.) are selected.
    Returns TES location with two and three-body combinations for vertex isolation

    Args:
        name: String with prefix of the TES location for extra selection 
        candidate: Container of reference particles to relate extra particles
        head: The head of the decay descriptor to build vertex isolation for.
            e.g. for [B0 -> K*(892)~0 rho(770)0]cc' use head='B0'.
    """

    two_body_comb = make_two_body_combination(candidate, head)
    three_body_comb = make_three_body_combination(candidate, head)

    extra_outputs = extra_outputs_for_isolation(
        name=name + "_OneTrackCombination_VertexIsolation",
        extra_particles=two_body_comb,
        ref_particles=candidate,
        selection=(
            F.ABS @ (F.CHI2() @ F.FORWARDARG1() - F.CHI2() @ F.FORWARDARG0()) <
            max_two_body_vtx_cut))

    extra_outputs += extra_outputs_for_isolation(
        name=name + "_TwoTracksCombination_VertexIsolation",
        extra_particles=three_body_comb,
        ref_particles=candidate,
        selection=(
            F.ABS @ (F.CHI2() @ F.FORWARDARG1() - F.CHI2() @ F.FORWARDARG0()) <
            max_three_body_vtx_cut))

    return extra_outputs


@configurable
def make_iso_particles(line_alg,
                       name='B',
                       coneangle=.5,
                       LongTrackIso=True,
                       TTrackIso=False,
                       DownstreamTrackIso=False,
                       UpstreamTrackIso=True,
                       NeutralIso=True,
                       PiZerosIso=False,
                       cone_for_each_track=False):

    candidate = line_alg
    cut = ((F.DR2 < coneangle) & ~F.FIND_IN_TREE())

    iso_parts = select_parts_for_isolation(
        names=[name],
        candidates=[candidate],
        cut=cut,
        LongTrackIso=LongTrackIso,
        TTrackIso=TTrackIso,
        DownstreamTrackIso=DownstreamTrackIso,
        UpstreamTrackIso=UpstreamTrackIso,
        NeutralIso=NeutralIso,
        PiZerosIso=PiZerosIso,
    )

    return iso_parts
