###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builders for strange decays. In this file we define constructors for particles
originating from strange decays. We provide two sets of requirements for basic
particles, normal and tight, which are chosen for rare and more abundant
decay signatures, respectively. For combinations, we distinguish between
those in which the parent particle comes from a primary vertex, and those
in which the combination of the decay products do not point to any primary
vertex (tipically semileptonic decays). The mass bounds are centralized in
dedicated configurable functions in order to consistently modify lines where
the mass bounds are complementary.

Contact: Miguel Ramos Pernas (miguel.ramos.pernas@cern.ch)
"""
import functools
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ns

from Hlt2Conf import standard_particles
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Hlt2Conf.lines.rd.builders import rd_isolation
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import FunctionalDiElectronMaker
from PyConf import configurable

# Monitoring variables in different scenarios
MONITORING_VARIABLES_CONTROL_NO_VERTEX = ('m', 'pt', 'eta', 'n_candidates')
MONITORING_VARIABLES_CONTROL = ('m', 'pt', 'eta', 'vchi2', 'ipchi2',
                                'n_candidates')
MONITORING_VARIABLES_SEARCH = ('pt', 'eta', 'vchi2', 'ipchi2', 'n_candidates')


def optional_functor(function):
    """ Wrap a function so it returns None if the first argument is None """

    def wrapper(value, *args):
        return None if value is None else function(value, *args)

    return wrapper


# empty requirements generator
NO_REQUIREMENTS = lambda: {}

# functors used for filtering
MIN_P = optional_functor(lambda v: F.P > v)
MIN_PT = optional_functor(lambda v: F.PT > v)
MAX_CHI2DOF = optional_functor(lambda v: F.CHI2DOF < v)
MAX_GHOSTPROB = optional_functor(lambda v: F.GHOSTPROB < v)
MAX_MINIP = optional_functor(lambda v, pvs: F.MINIP(pvs) < v)
MIN_MINIP = optional_functor(lambda v, pvs: F.MINIP(pvs) > v)
MAX_MINIPCHI2 = optional_functor(lambda v, pvs: F.MINIPCHI2(pvs) < v)
MIN_MINIPCHI2 = optional_functor(lambda v, pvs: F.MINIPCHI2(pvs) > v)
MIN_PID_MU = optional_functor(lambda v: F.PID_MU > v)
MIN_PID_P = optional_functor(lambda v: F.PID_P > v)
MIN_PID_P_OVER_K = optional_functor(lambda v: (F.PID_P - F.PID_K) > v)
MIN_PID_K = optional_functor(lambda v: F.PID_K > v)
MIN_PID_K_OVER_P = optional_functor(lambda v: (F.PID_K - F.PID_P) > v)
MIN_PID_E = optional_functor(lambda v: F.PID_E > v)
MAX_DOCA = optional_functor(lambda v: F.MAXDOCACUT(v))
MAX_DOCACHI2 = optional_functor(lambda v: F.MAXDOCACHI2CUT(v))
MIN_BPVVDZ = optional_functor(lambda v, pvs: F.BPVVDZ(pvs) > v)
MAX_IPBPVVDZ_RATIO = optional_functor(
    lambda v, pvs: F.MINIP(pvs) / F.BPVVDZ(pvs) < v)
MIN_BPVVDRHO = optional_functor(lambda v, pvs: F.BPVVDRHO(pvs) > v)
MIN_RHO = optional_functor(lambda v: F.END_VX**2 + F.END_VY**2 > v**2)
MIN_BPVLTIME = optional_functor(lambda v, pvs: F.BPVLTIME(pvs) > v)
MIN_DIRA = optional_functor(lambda v, pvs: F.BPVDIRA(pvs) > v)
IS_MUON = optional_functor(lambda v: F.ISMUON if v else not F.ISMUON)
MIN_IS_NOT_H = optional_functor(lambda v: F.IS_NOT_H > v)
MIN_IS_PHOTON = optional_functor(lambda v: F.IS_PHOTON > v)
# needed due to https://gitlab.cern.ch/lhcb/MooreAnalysis/-/issues/36
# TODO: remove once the issue is solved
CHECK_PVS = lambda pvs: F.SIZE(pvs) > 0


def used_functors(*args):
    """ Filter optional functors """
    return list(filter(lambda f: f is not None, args))


@configurable
def filter_decay_products(particles,
                          *,
                          p_min=None,
                          pt_min=None,
                          track_chi2dof_max=None,
                          ghostprob_max=None,
                          ip_max=None,
                          ip_min=None,
                          ipchi2_max=None,
                          ipchi2_min=None,
                          pid_mu_min=None,
                          pid_p_min=None,
                          pid_p_over_k_min=None,
                          pid_k_min=None,
                          pid_k_over_p_min=None,
                          pid_e_min=None,
                          is_not_h_min=None,
                          is_photon_min=None,
                          is_muon=None,
                          name):
    """ Filter the products of strange decays """
    pvs = make_pvs()
    selection = used_functors(
        CHECK_PVS(pvs),  # TODO: remove once MooreAnalysis#36 is fixed
        MIN_P(p_min),
        MIN_PT(pt_min),
        MAX_CHI2DOF(track_chi2dof_max),
        MAX_GHOSTPROB(ghostprob_max),
        MAX_MINIP(ip_max, pvs),
        MIN_MINIP(ip_min, pvs),
        MAX_MINIPCHI2(ipchi2_max, pvs),
        MIN_MINIPCHI2(ipchi2_min, pvs),
        IS_MUON(is_muon),
        MIN_PID_MU(pid_mu_min),
        MIN_PID_P(pid_p_min),
        MIN_PID_P_OVER_K(pid_p_over_k_min),
        MIN_PID_K(pid_k_min),
        MIN_PID_K_OVER_P(pid_k_over_p_min),
        MIN_PID_E(pid_e_min),
        MIN_IS_NOT_H(is_not_h_min),
        MIN_IS_PHOTON(is_photon_min),
    )
    return ParticleFilter(
        particles,
        F.FILTER(F.ALL if not selection else F.require_all(*selection)),
        name=name)


@configurable
def combination_requirements(
        *,
        # combination
        mass_range,
        doca_max=None,
        docachi2_max=None,
        pt_min=None,
        # vertex selection
        vchi2pdof_max=None,
        bpvvdz_min=None,
        ip_bpvvdz_ratio_max=None,
        bpvvdrho_min=None,
        rho_min=None,
        ip_max=None,
        ip_min=None,
        ipchi2_max=None,
        ipchi2_min=None,
        dira_min=None,
        lifetime_min=None):
    """ Build the combination and vertex requirements for a trigger line """
    comb_mass_min, comb_mass_max = mass_range

    # account for up to a 10% difference in the invariant mass
    # measurement before and after the vertex fit
    combination_selection = [
        in_range(0.9 * comb_mass_min, F.MASS, 1.1 * comb_mass_max)
    ] + used_functors(
        MAX_DOCA(doca_max),
        MAX_DOCACHI2(docachi2_max),
        MIN_PT(pt_min),
    )

    pvs = make_pvs()

    vertex_selection = used_functors(
        CHECK_PVS(pvs),  # TODO: remove once MooreAnalysis#36 is fixed
        in_range(comb_mass_min, F.MASS, comb_mass_max),
        MAX_CHI2DOF(vchi2pdof_max),
        MIN_BPVVDZ(bpvvdz_min, pvs),
        MAX_IPBPVVDZ_RATIO(ip_bpvvdz_ratio_max, pvs),
        MIN_BPVVDRHO(bpvvdrho_min, pvs),
        MIN_RHO(rho_min),
        MAX_MINIP(ip_max, pvs),
        MIN_MINIP(ip_min, pvs),
        MAX_MINIPCHI2(ipchi2_max, pvs),
        MIN_MINIPCHI2(ipchi2_min, pvs),
        MIN_DIRA(dira_min, pvs),
        MIN_BPVLTIME(lifetime_min, pvs),
    )

    return F.require_all(*combination_selection), (
        F.ALL if not vertex_selection else F.require_all(*vertex_selection))


@configurable
def make_combination(decay_descriptor,
                     inputs,
                     *,
                     build_requirements=None,
                     mass_range,
                     can_reco_vertex=True,
                     name):
    """ General combiner """
    combination_selection, vertex_selection = combination_requirements(
        mass_range=mass_range,
        **({} if build_requirements is None else build_requirements()))
    return ParticleCombiner(
        inputs,
        ParticleCombiner=('ParticleVertexFitter'
                          if can_reco_vertex else 'ParticleAdder'),
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_selection,
        CompositeCut=vertex_selection,
        name=name,
    )


@configurable
def filter_combination(combination,
                       *,
                       build_requirements=None,
                       mass_range,
                       name):
    """ Filter a combination candidate """
    combination_selection, vertex_selection = combination_requirements(
        mass_range=mass_range,
        **({} if build_requirements is None else build_requirements()))
    return ParticleFilter(
        combination,
        F.FILTER(F.require_all(combination_selection, vertex_selection)),
        name=name)


@configurable
def make_dielectron(particle_id,
                    *,
                    mass_range,
                    dielectron_pt_min=0. * MeV,
                    brem_adder='SelectiveBremAdder',
                    opposite_sign=True,
                    electron_requirements='standard',
                    combiner='dielectron-maker'):
    """
    Create a dielectron candidate where we avoid using the
    same radiation photon more than once
    """
    dielectron_mass_min, dielectron_mass_max = mass_range

    if electron_requirements == 'standard':
        electrons_builder = make_electrons
    elif electron_requirements == 'tight':
        electrons_builder = make_tight_electrons
    elif electron_requirements == 'loose':
        electrons_builder = make_loose_electrons
    else:
        raise ValueError(
            f'Unknown electron requirements "{electron_requirements}"; choose between "standard", "tight" or "loose"'
        )

    if combiner == 'dielectron-maker':
        return FunctionalDiElectronMaker(
            InputParticles=electrons_builder(add_brem=False),
            MinDiElecPT=dielectron_pt_min,
            MinDiElecMass=dielectron_mass_min,
            MaxDiElecMass=dielectron_mass_max,
            DiElecID=particle_id,
            OppositeSign=opposite_sign,
            BremAdder=brem_adder,
            ParticleCombiner='ParticleVertexFitter',
            name='DiElectronForStrangeDecays_DiElectronMaker_{hash}').Particles
    elif combiner == 'combiner':
        electrons = electrons_builder(add_brem=False)
        return make_combination(
            f'{particle_id} -> e+ e-' if opposite_sign else
            f'[{particle_id} -> e+ e+]cc', [electrons, electrons],
            mass_range=(dielectron_mass_min, dielectron_mass_max),
            build_requirements=lambda: dict(pt_min=dielectron_pt_min),
            name='DiElectronForStrangeDecays_Combiner_{hash}')
    else:
        raise ValueError(
            f'Unknown combiner type "{combiner}"; choose between "dielectron-maker" or "combiner"'
        )


def requirements_builder(function):
    """
    Wrap a function to update the configuration returned by it with the
    provided keyword arguments. This allows to keep having a configurable
    function defining a set of requirements but in such a way that we do
    not need to propagate by hand the different keyword arguments.
    """

    @configurable
    @functools.wraps(function)
    def wrapper(**kwargs):
        requirements = function()
        requirements.update(kwargs)
        return requirements

    return wrapper


#
# Requirements on the decay products
#
@requirements_builder
def loose_requirements_for_decay_products():
    return dict(
        p_min=3 * GeV,
        pt_min=80 * MeV,
        track_chi2dof_max=16.,
        ip_min=0.5 * mm,
        ipchi2_min=16.,
    )


@requirements_builder
def requirements_for_decay_products():
    return loose_requirements_for_decay_products(
        p_min=3 * GeV,
        pt_min=80 * MeV,
        track_chi2dof_max=9.,
        ip_min=1. * mm,
        ipchi2_min=25.,
    )


@requirements_builder
def tight_requirements_for_decay_products():
    return requirements_for_decay_products(
        p_min=3 * GeV,
        pt_min=80 * MeV,
        track_chi2dof_max=4.,
        ip_min=5. * mm,
        ipchi2_min=144.,
    )


#
# Requirements on the vertex for any combination involving a neutral particle. These
# selections can not contain vertexing requirements unless the combination involves
# at least two particles with information on the direction of motion.
#
@requirements_builder
def loose_requirements_for_neutral_combination_no_vertex():
    return dict(pt_min=500 * MeV, )


@requirements_builder
def requirements_for_neutral_combination_no_vertex():
    return loose_requirements_for_neutral_combination_no_vertex()


@requirements_builder
def tight_requirements_for_neutral_combination_no_vertex():
    return requirements_for_neutral_combination_no_vertex()


#
# Requirements on the vertex for any loose combination
#
@requirements_builder
def loose_requirements_for_combination():
    return dict(
        bpvvdz_min=0. * mm,
        docachi2_max=25.,
        doca_max=0.3 * mm,
        vchi2pdof_max=16.,
        ip_bpvvdz_ratio_max=1. / 60,
    )


@requirements_builder
def requirements_for_combination():
    return loose_requirements_for_combination(
        docachi2_max=16.,
        doca_max=0.2 * mm,
        vchi2pdof_max=9.,
    )


@requirements_builder
def tight_requirements_for_combination():
    return requirements_for_combination(
        docachi2_max=9.,
        doca_max=0.1 * mm,
        vchi2pdof_max=4.,
    )


#
# Selection for a combination that is assumed to come from a
# primary vertex. We add requirements on the maximum distance
# and the minimum displacement in the perpendicular plane with
# respect to any primary vertex.
#
default_additional_requirements_for_prompt_combination = dict(
    ip_max=0.4 * mm, bpvvdrho_min=3. * mm, dira_min=0.999, ipchi2_max=25.)


@requirements_builder
def loose_requirements_for_prompt_combination():
    return loose_requirements_for_combination(
        **default_additional_requirements_for_prompt_combination)


@requirements_builder
def requirements_for_prompt_combination():
    return requirements_for_combination(
        **default_additional_requirements_for_prompt_combination)


@requirements_builder
def tight_requirements_for_prompt_combination():
    return tight_requirements_for_combination(
        **default_additional_requirements_for_prompt_combination)


#
# Requirements for a combination that does not come from
# a primary vertex (predominantly semileptonic decays). We
# simply add a requirement on the vertex position with respect
# to the Z axis. The lack of a requirement on the impact
# parameter allows to use this requirement also on the
# normalization modes.
#
default_additional_requirements_for_detached_combination_no_ip = dict(
    rho_min=3. * mm)


@requirements_builder
def loose_requirements_for_detached_combination_no_ip():
    return loose_requirements_for_combination(
        **default_additional_requirements_for_detached_combination_no_ip)


@requirements_builder
def requirements_for_detached_combination_no_ip():
    return requirements_for_combination(
        **default_additional_requirements_for_detached_combination_no_ip)


@requirements_builder
def tight_requirements_for_detached_combination_no_ip():
    return tight_requirements_for_combination(
        **default_additional_requirements_for_detached_combination_no_ip)


#
# Requirements for a combination that does not come from
# a primary vertex (predominantly semileptonic decays).
#
@requirements_builder
def loose_requirements_for_detached_combination():
    return loose_requirements_for_detached_combination_no_ip(ip_min=0.5 * mm)


@requirements_builder
def requirements_for_detached_combination():
    return requirements_for_detached_combination_no_ip(ip_min=1. * mm)


@requirements_builder
def tight_requirements_for_detached_combination():
    return tight_requirements_for_detached_combination_no_ip(
        ip_min=5. * mm, ipchi2_min=100., ipchi2_max=900.)


#
# Requirements for a prompt KS0 selection
#
default_additional_requirements_for_prompt_ks0 = dict(
    lifetime_min=0.0045 * ns  # eff. ~ 95 %
)


@requirements_builder
def loose_requirements_for_prompt_ks0():
    return loose_requirements_for_prompt_combination(
        **default_additional_requirements_for_prompt_ks0)


@requirements_builder
def requirements_for_prompt_ks0():
    return requirements_for_prompt_combination(
        **default_additional_requirements_for_prompt_ks0)


#
# Basic particle makers (decay products)
#
@configurable
def make_loose_electrons(*, pid_e_min=0, add_brem=True, **kwargs):
    return filter_decay_products(
        standard_particles.make_long_electrons_with_brem()
        if add_brem else standard_particles.make_long_electrons_no_brem(),
        pid_e_min=pid_e_min,
        name='LooseElectronsForStrangeDecays_{hash}',
        **loose_requirements_for_decay_products(**kwargs))


@configurable
def make_electrons(*, pid_e_min=2, add_brem=True, **kwargs):
    return filter_decay_products(
        make_loose_electrons(add_brem=add_brem),
        pid_e_min=pid_e_min,
        name='ElectronsForStrangeDecays_{hash}',
        **requirements_for_decay_products(**kwargs))


@configurable
def make_tight_electrons(*, pid_e_min=5, add_brem=True, **kwargs):
    return filter_decay_products(
        make_electrons(add_brem=add_brem),
        pid_e_min=pid_e_min,
        name='TightElectronsForStrangeDecays_{hash}',
        **tight_requirements_for_decay_products(**kwargs))


@configurable
def make_loose_muons(**kwargs):
    return filter_decay_products(
        standard_particles.make_long_muons(),
        name='LooseMuonsForStrangeDecays_{hash}',
        **loose_requirements_for_decay_products(**kwargs))


@configurable
def make_muons(*, pid_mu_min=-3, **kwargs):
    return filter_decay_products(
        make_loose_muons(),
        pid_mu_min=pid_mu_min,
        name='MuonsForStrangeDecays_{hash}',
        is_muon=True,
        **requirements_for_decay_products(**kwargs))


@configurable
def make_tight_muons(pid_mu_min=5., **kwargs):
    return filter_decay_products(
        make_muons(),
        pid_mu_min=pid_mu_min,
        name='TightMuonsForStrangeDecays_{hash}',
        **tight_requirements_for_decay_products(**kwargs))


@configurable
def make_loose_pions(**kwargs):
    return filter_decay_products(
        standard_particles.make_long_pions(),
        name='LoosePionsForStrangeDecays_{hash}',
        **loose_requirements_for_decay_products(**kwargs))


@configurable
def make_pions(**kwargs):
    return filter_decay_products(
        make_loose_pions(),
        name='PionsForStrangeDecays_{hash}',
        **requirements_for_decay_products(**kwargs))


@configurable
def make_tight_pions(**kwargs):
    return filter_decay_products(
        make_pions(),
        name='TightPionsForStrangeDecays_{hash}',
        **tight_requirements_for_decay_products(**kwargs))


@configurable
def make_loose_downstream_pions(**kwargs):
    return filter_decay_products(
        standard_particles.make_down_pions(),
        name='LooseDownstreamPionsForStrangeDecays_{hash}',
        **loose_requirements_for_decay_products(**kwargs))


@configurable
def make_downstream_pions(**kwargs):
    return filter_decay_products(
        make_loose_downstream_pions(),
        name='DownstreamPionsForStrangeDecays_{hash}',
        **requirements_for_decay_products(**kwargs))


@configurable
def make_tight_downstream_pions(**kwargs):
    return filter_decay_products(
        make_downstream_pions(),
        name='TightDownstreamPionsForStrangeDecays_{hash}',
        **tight_requirements_for_decay_products(**kwargs))


@configurable
def make_kaons(pid_k_min=3, pid_k_over_p_min=3, **kwargs):
    return filter_decay_products(
        standard_particles.make_long_kaons(),
        name='KaonsForStrangeDecays_{hash}',
        pid_k_min=pid_k_min,
        pid_k_over_p_min=pid_k_over_p_min,
        **requirements_for_decay_products(**kwargs))


@configurable
def make_tight_kaons(**kwargs):
    return filter_decay_products(
        make_kaons(),
        name='TightKaonsForStrangeDecays_{hash}',
        **tight_requirements_for_decay_products(**kwargs))


@configurable
def make_downstream_kaons(pid_k_min=3, pid_k_over_p_min=3, **kwargs):
    return filter_decay_products(
        standard_particles.make_down_kaons(),
        name='DownstreamKaonsForStrangeDecays_{hash}',
        pid_k_min=pid_k_min,
        pid_k_over_p_min=pid_k_over_p_min,
        **requirements_for_decay_products(**kwargs))


@configurable
def make_photons(pt_min=150 * MeV, is_not_h_min=None, is_photon_min=None):
    particles = standard_particles.make_photons(PtCut=pt_min)
    return filter_decay_products(
        particles,
        is_not_h_min=is_not_h_min,
        is_photon_min=is_photon_min,
        name='PhotonsForStrangeDecays_{hash}')


@configurable
def make_resolved_neutral_pions(*, mass_window=30. * MeV, pt_min=0. * MeV):
    return standard_particles.make_resolved_pi0s(
        particles=make_photons, mass_window=mass_window, PtCut=pt_min)


@configurable
def make_tight_resolved_neutral_pions(**kwargs):
    return make_resolved_neutral_pions(**kwargs)


@configurable
def make_merged_neutral_pions(*, mass_window=60 * MeV, pt_min=2 * GeV):
    return standard_particles.make_merged_pi0s(
        mass_window=mass_window, PtCut=pt_min)


@configurable
def make_tight_merged_neutral_pions(**kwargs):
    return make_merged_neutral_pions(**kwargs)


@configurable
def make_protons(*, pid_p_min=0, pid_p_over_k_min=0, **kwargs):
    return filter_decay_products(
        standard_particles.make_long_protons(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name='ProtonsForStrangeDecays_{hash}',
        **requirements_for_decay_products(**kwargs))


@configurable
def make_tight_protons(*, pid_p_min=+10, pid_p_over_k_min=+10, **kwargs):
    return filter_decay_products(
        make_protons(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name='TightProtonsForStrangeDecays_{hash}',
        **kwargs,
        **tight_requirements_for_decay_products(**kwargs))


#
# Definitions of invariant mass bounds
#
@configurable
def make_downstream_protons(*, pid_p_min=0, pid_p_over_k_min=0, **kwargs):
    return filter_decay_products(
        standard_particles.make_down_protons(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name='DownstreamProtonsForStrangeDecays_{hash}',
        **requirements_for_decay_products(**kwargs))


@configurable
def make_tight_downstream_protons(*,
                                  pid_p_min=+5,
                                  pid_p_over_k_min=+3,
                                  **kwargs):
    return filter_decay_products(
        make_downstream_protons(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name='TightDownstreamProtonsForStrangeDecays_{hash}',
        **kwargs,
        **tight_requirements_for_decay_products(**kwargs))


@configurable
def ks0_mass_bounds(mass_min=400 * MeV, mass_max=600 * MeV):
    """ Allow to define the same mass bounds for many lines that select KS0 decays """
    return mass_min, mass_max


@configurable
def ks0_tight_mass_bounds(mass_min=470 * MeV, mass_max=600 * MeV):
    """
    Allow to define the same mass bounds for many lines that select KS0 decays

    The lower mass bound is chosen to avoid the doubly misidentified KS0 -> pi+ pi-
    decays.
    """
    return mass_min, mass_max


@configurable
def ks0_semileptonic_mass_bounds(mass_min=0. * MeV, mass_max=360 * MeV):
    """
    Allow to define the same mass bounds for many lines that select semileptonic
    KS0 decays
    """
    return mass_min, mass_max


@configurable
def kplus_mass_bounds(mass_min=460 * MeV, mass_max=660 * MeV):
    """
    Allow to define the same mass bounds for many lines that select K+ decays

    The lower mass bound is chosen to avoid doubly-misidentified decays in
    leptonic modes.
    """
    return mass_min, mass_max


@configurable
def lambda0_mass_bounds(mass_min=1000 * MeV, mass_max=1200 * MeV):
    """ Allow to define the same mass bounds for many lines that select Lambda0 decays """
    return mass_min, mass_max


@configurable
def sigma_mass_bounds(mass_min=1000 * MeV, mass_max=1300 * MeV):
    """ Allow to define the same mass bounds for many lines that select Sigma+ decays """
    return mass_min, mass_max


@configurable
def xi_mass_bounds(mass_min=1200 * MeV, mass_max=1400 * MeV):
    """ Allow to define the same mass bounds for many lines that select Xi-/0 decays """
    return mass_min, mass_max


@configurable
def xi_tight_mass_bounds(mass_min=1275 * MeV, mass_max=1375 * MeV):
    """ Allow to define the same mass bounds for many lines that select Xi-/0 decays """
    return mass_min, mass_max


@configurable
def omega_mass_bounds(mass_min=1550 * MeV, mass_max=1750 * MeV):
    """ Allow to define the same mass bounds for many lines that select Omega- decays """
    return mass_min, mass_max


#
# Intermediate particle makers
#
@configurable
def build_lambda0():
    """ Build Lambda0 baryons """
    proton = make_protons()
    pion = make_pions()
    return make_combination(
        '[Lambda0 -> p+ pi-]cc', [proton, pion],
        build_requirements=requirements_for_detached_combination_no_ip,
        mass_range=lambda0_mass_bounds(),
        name='Lambda0ForStrangeDecays_Combiner_{hash}')


@configurable
def build_tight_lambda0():
    """ Build Lambda0 baryons """
    proton = make_tight_protons()
    pion = make_tight_pions()
    return make_combination(
        '[Lambda0 -> p+ pi-]cc', [proton, pion],
        build_requirements=tight_requirements_for_detached_combination_no_ip,
        mass_range=lambda0_mass_bounds(),
        name='TightLambda0ForStrangeDecays_Combiner_{hash}')


@configurable
def build_downstream_lambda0():
    """ Build downstream Lambda0 baryons """
    proton = make_downstream_protons()
    pion = make_downstream_pions()
    return make_combination(
        '[Lambda0 -> p+ pi-]cc', [proton, pion],
        build_requirements=requirements_for_detached_combination_no_ip,
        mass_range=lambda0_mass_bounds(),
        name='DownstreamLambda0ForStrangeDecays_Combiner_{hash}')


@configurable
def build_tight_downstream_lambda0():
    """ Build downstream Lambda0 baryons """
    proton = make_tight_downstream_protons()
    pion = make_tight_downstream_pions()
    return make_combination(
        '[Lambda0 -> p+ pi-]cc', [proton, pion],
        build_requirements=tight_requirements_for_detached_combination_no_ip,
        mass_range=lambda0_mass_bounds(),
        name='TightDownstreamLambda0ForStrangeDecays_Combiner_{hash}')


@configurable
def build_dimuon_intermediate_neutral(particle_name, *, mass_max,
                                      requirements):
    """ Build a neutral particle from two muons with loose requirements """
    if requirements == 'standard':
        muons = make_muons()
        head_requirements = requirements_for_detached_combination
    elif requirements == 'loose':
        muons = make_loose_muons()
        head_requirements = loose_requirements_for_detached_combination
    elif requirements == 'tight':
        muons = make_tight_muons()
        head_requirements = tight_requirements_for_detached_combination
    else:
        raise ValueError(
            f'Unknown requirements "{requirements}"; choose between "standard", "tight" or "loose"'
        )
    return make_combination(
        f'{particle_name} -> mu+ mu-', [muons, muons],
        build_requirements=head_requirements,
        mass_range=(dimuon_min_mass(), mass_max),
        name='DiMuon_IntermediateNeutral_Combiner_{hash}')


@configurable
def build_dielectron_intermediate_neutral(particle_name, *, mass_max,
                                          requirements):
    """ Build a neutral particle from two electrons with loose requirements """
    if requirements == 'standard':
        head_requirements = requirements_for_detached_combination
    elif requirements == 'loose':
        head_requirements = loose_requirements_for_detached_combination
    elif requirements == 'tight':
        head_requirements = tight_requirements_for_detached_combination
    else:
        raise ValueError(
            f'Unknown requirements "{requirements}"; choose between "standard", "tight" or "loose"'
        )
    mass_range = (dielectron_min_mass(), mass_max)
    return filter_combination(
        make_dielectron(
            particle_name,
            mass_range=mass_range,
            electron_requirements=requirements),
        build_requirements=head_requirements,
        mass_range=mass_range,
        name='DiElectron_IntermediateNeutral_Filter_{hash}')


#
# Basic diparticle minimum invariant mass values
#
@configurable
def dielectron_min_mass(mass_min=30. * MeV):
    """ Requirement to avoid a peak at low mass in the dielectron spectrum """
    return mass_min


@configurable
def dimuon_min_mass(mass_min=220. * MeV):
    """ Requirement to avoid a peak at low mass in the dimuon spectrum """
    return mass_min


@configurable
def pion_muon_min_mass(mass_min=250. * MeV):
    """ Requirement to avoid a peak at low mass in the pion-muon spectrum """
    return mass_min


@configurable
def pion_electron_min_mass(mass_min=160. * MeV):
    """ Requirement to avoid a peak at low mass in the pion-electron spectrum """
    return mass_min


@configurable
def muon_electron_min_mass(mass_min=105. * MeV):
    """ Requirement for [mu+, e-]cc combinations """
    return mass_min


#
# Definition of the isolation
#
@configurable
def define_isolation(names_and_candidates,
                     *,
                     cut,
                     tracks_with_velo_segment=False,
                     downstream_tracks=False):
    """
    Define the storage of additional particles based on requirements
    imposed with respect to the head of a decay.
    """
    names, candidates = list(zip(*names_and_candidates.items()))
    return rd_isolation.select_parts_for_isolation(
        names=names,
        candidates=candidates,
        cut=cut,
        LongTrackIso=tracks_with_velo_segment,
        DownstreamTrackIso=downstream_tracks,
        # we also persist upstream tracks when we work with long tracks
        UpstreamTrackIso=tracks_with_velo_segment,
        # we ensure the other isolation objects are disabled
        TTrackIso=False,
        NeutralIso=False,
        PiZerosIso=False)


@configurable
def define_impact_parameter_chi2_based_isolation(names_and_candidates,
                                                 *,
                                                 ipchi2_max=25,
                                                 **kwargs):
    """
    Define the isolation based on a requirement on the impact parameter chi2
    between the additional particles and the vertex of the composite objects.
    """
    # the first argument to the algorithm making the weighted related-info table
    # is the reference particles and the second is the candidates to persist
    functor = (F.IPCHI2.bind(F.FORWARDARG0, F.FORWARDARG1) <
               ipchi2_max) & ~F.FIND_IN_TREE()
    return define_isolation(names_and_candidates, cut=functor, **kwargs)


@configurable
def define_cone_based_isolation(names_and_candidates,
                                *,
                                delta_r_requirement=0.25,
                                **kwargs):
    """
    Define the isolation based on a requirement in the cone distance based on
    the azimuthal and pseudorapidity space.
    """
    functor = (F.DR2 <
               delta_r_requirement * delta_r_requirement) & ~F.FIND_IN_TREE()
    return define_isolation(names_and_candidates, cut=functor, **kwargs)
