###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

#RD isolation algorithm
from PyConf import configurable
from PyConf.Algorithms import ThOrParticleSelection
from Hlt2Conf.standard_particles import make_long_pions, make_up_pions, make_down_pions, make_ttrack_pions, make_photons, make_merged_pi0s
from Hlt2Conf.isolation import extra_outputs_for_isolation
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleContainersMerger
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_tauons_hadronic_decay

import Functors as F


@configurable
def find_in_decay(input=make_rd_tauons_hadronic_decay, id="pi+"):
    """
    Retrieve the selection of particles matching 'id' argument from 'input' decay tree
    Args:
        input: Composite particle
        id: String representation of the ParticleProperty to get
    """
    code = (F.FILTER(F.IS_ABS_ID(id)) @ F.GET_ALL_DESCENDANTS())

    return ThOrParticleSelection(
        InputParticles=input, Functor=code).OutputSelection


@configurable
def make_two_body_combination(candidate, descriptors):
    """
    Make two body combination of B0 and extra particles (long tracks with pion id).

    Args:
    candidate: Containers of reference particles
    descriptors: Decay descriptors of reference particle with extra particle.

    Returns: TES location of two body combination of candidate and extra_particles
    """

    extra_particles = make_long_pions()

    #first combiner: X pi+
    comb_1 = ParticleCombiner(
        Inputs=[candidate, extra_particles],
        name='rd_vtxiso_combiner_onetrack_1_{hash}',
        DecayDescriptor=descriptors[0],
        CombinationCut=F.ALL,
        CompositeCut=F.ALL)
    #second combiner: X pi-
    comb_2 = ParticleCombiner(
        Inputs=[candidate, extra_particles],
        name='rd_vtxiso_combiner_onetrack_2_{hash}',
        DecayDescriptor=descriptors[1],
        CombinationCut=F.ALL,
        CompositeCut=F.ALL)
    #merge two combiners
    comb = ParticleContainersMerger([comb_1, comb_2],
                                    name='rd_vtxiso_combiner_onetrack_{hash}')

    return comb


@configurable
def make_three_body_combination(candidate, descriptor):
    """
    Make three body combination of B0 and two extra particles (long tracks with pion id).

    Args:
    candidate: Containers of reference particles
    descriptors: Decay descriptor of reference particle with two extra particles.

    Returns: TES location of three body combination of candidate and two extra_particles
    """

    extra_particles = make_long_pions()

    #combiner: X pi+ pi-
    comb = ParticleCombiner(
        Inputs=[candidate, extra_particles, extra_particles],
        name='rd_vtxiso_combiner_twotracks_{hash}',
        DecayDescriptor=descriptor,
        CombinationCut=F.ALL,
        CompositeCut=F.ALL)

    return comb


@configurable
def select_parts_for_isolation(names=[],
                               candidates=[],
                               cut=(F.DR2 < 0.25),
                               LongTrackIso=True,
                               TTrackIso=False,
                               DownstreamTrackIso=False,
                               UpstreamTrackIso=False,
                               NeutralIso=True,
                               PiZerosIso=False):
    """
    Add to the extra_outputs different kind of isolations by properly setting the given flag.
    Returns selection of extra particles for isolation.
    
    Args:
        names: List of strings with prefix of the TES location for extra selection 
        candidates: List of containers of reference particles to relate extra particles
        cut: Predicate to select extra information to persist. By default: cone geometry with max dr2=1
        LongTrackIso: Boolean flag to make isolation with long tracks. By default True.
        TTrackIso: Boolean flag to make isolation with tt tracks. By default False.
        DownstreamTrackIso: Boolean flag to make isolation with downstream tracks. By default False.
        UpstreamTrackIso: Boolean flag to make isolation with upstream tracks. By default False.
        NeutralIso: Boolean flag to make isolation with neutral particles. By default True.
    """
    extra_outputs = []
    assert (
        len(names) == len(candidates)
    ), 'Different number of names and candidate containers for particle isolation!'

    for name, cand in zip(names, candidates):
        if LongTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_LongTrackIsolation",
                extra_particles=make_long_pions(),
                ref_particles=cand,
                selection=cut)
        if TTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_TTrackIsolation",
                extra_particles=make_ttrack_pions(),
                ref_particles=cand,
                selection=cut)
        if DownstreamTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_DownstreamTrackIsolation",
                extra_particles=make_down_pions(),
                ref_particles=cand,
                selection=cut)
        if UpstreamTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_UpstreamTrackIsolation",
                extra_particles=make_up_pions(),
                ref_particles=cand,
                selection=cut)
        if NeutralIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_NeutralIsolation",
                extra_particles=make_photons(),
                ref_particles=cand,
                selection=cut)
        if PiZerosIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_PiZerosIsolation",
                extra_particles=make_merged_pi0s(),
                ref_particles=cand,
                selection=cut)
    return extra_outputs


def select_combinations_for_vertex_isolation(name,
                                             candidate,
                                             descriptors,
                                             max_two_body_vtx_cut=9.,
                                             max_three_body_vtx_cut=15.):
    """
    Add to the extra_outputs vertex isolations when adding one track or two tracks to the vertex fit.
    By default only two(three)-body combinations with vertex fit chi2 smaller than 9.(15.) are selected.
    Returns TES location with two and three-body combinations for vertex isolation
    
    Args:
        name: String with prefix of the TES location for extra selection 
        candidate: Container of reference particles to relate extra particles
        descriptors: List of string for the decay descriptors of the two(three)-body combinations combination when adding one(two) tracks
    """

    two_body_comb = make_two_body_combination(candidate, descriptors)
    three_body_comb = make_three_body_combination(candidate, descriptors[2])

    extra_outputs = extra_outputs_for_isolation(
        name=name + "_OneTrackCombination_VertexIsolation",
        extra_particles=two_body_comb,
        ref_particles=candidate,
        selection=(
            F.ABS @ (F.CHI2() @ F.FORWARDARG1() - F.CHI2() @ F.FORWARDARG0()) <
            max_two_body_vtx_cut))

    extra_outputs += extra_outputs_for_isolation(
        name=name + "_TwoTracksCombination_VertexIsolation",
        extra_particles=three_body_comb,
        ref_particles=candidate,
        selection=(
            F.ABS @ (F.CHI2() @ F.FORWARDARG1() - F.CHI2() @ F.FORWARDARG0()) <
            max_three_body_vtx_cut))

    return extra_outputs


def parent_isolation_output(name, candidate):
    """
    Add default isolation for parent particle
    Args:
        name: String with prefix of the TES location for extra selection
        candidate: Containers of reference particles to relate extra particles
    """

    return select_parts_for_isolation(
        names=[name],
        candidates=[candidate],
        cut=((F.DR2 < 0.25) & ~F.FIND_IN_TREE()))


def parent_and_children_isolation(parents,
                                  decay_products,
                                  parents_DR2_size=0.25,
                                  decay_products_DR2_size=0.25):
    """

    Add isolation information around the parent and children tracks
    
    Args:
        parents: Dictionary of TES location names (key) and container (value) of reference particles relate extra particles on parents
        decay_products: Dictionary of TES location names  (key) and container (value)  of reference particles to relate extra particles on children tracks
        DR2_size: a tuple of size two containing the size of the isolation cone to store extra info, see Functor.DR2
    """
    (p_names, p_candidates) = zip(*parents.items())
    (d_names, d_candidates) = zip(*decay_products.items())

    iso_parts = select_parts_for_isolation(
        names=p_names,
        candidates=p_candidates,
        cut=F.require_all(
            F.DR2 < parents_DR2_size,
            ~F.FIND_IN_TREE(),
        ))

    iso_parts += select_parts_for_isolation(
        names=d_names,
        candidates=d_candidates,
        cut=F.require_all(
            F.DR2 < decay_products_DR2_size,
            ~F.SHARE_TRACKS(),
        ))
    return iso_parts
