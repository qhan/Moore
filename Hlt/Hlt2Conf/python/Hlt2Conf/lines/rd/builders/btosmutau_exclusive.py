###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of builders for the b -> s mu tau exclusive lines

Current builders:
 - make_phimu: make combination of two kaons and one muon
 - make_kmu: make combination of one kaon and one muon
 - make_kstmu: make combination of one kaon, one pion and one muon
 - make_pkmu: make combination of one proton, one kaon and one muon
 - make_bs: combine a phimu and a muon
 - make_bu: combine a kmu and a muon
 - make_lb: combine a pkmu and a muon  
 - make_bd: combine a kstmu and a muon

Author: H. Tilquin
Contact: hanae.tilquin@cern.ch
"""

from GaudiKernel.SystemOfUnits import MeV, mm
import Functors as F

from Hlt2Conf.algorithms_thor import ParticleCombiner
from PyConf import configurable


def make_phimu(kaons1,
               kaons2,
               muons,
               pvs,
               comb_m_max=3650 * MeV,
               vchi2pdof_max=9,
               docachi2_max=9.0,
               doca_max=0.8 * mm,
               phi_docachi2_max=4.0,
               phi_doca_max=0.12 * mm,
               phi_mass_min=700 * MeV,
               decay_descriptor="[B0 -> K+ K- mu+]cc",
               name="rd_dikaon_muon_for_btosmutau_{hash}"):
    """Builder for X -> K K mu decays, used in XMuTau lines"""
    combination_code = F.require_all(F.MASS < comb_m_max,
                                     F.MAXSDOCACHI2CUT(docachi2_max),
                                     F.MAXSDOCACUT(doca_max))
    two_body_combination_code = F.require_all(
        F.MASS > phi_mass_min, F.MASS < comb_m_max,
        F.MAXSDOCACHI2CUT(phi_docachi2_max), F.MAXSDOCACUT(phi_doca_max))
    vertex_code = F.CHI2DOF < vchi2pdof_max
    phimu = ParticleCombiner([kaons1, kaons2, muons],
                             DecayDescriptor=decay_descriptor,
                             Combination12Cut=two_body_combination_code,
                             CombinationCut=combination_code,
                             CompositeCut=vertex_code,
                             name=name)
    return phimu


def make_kmu(kaons,
             muons,
             pvs,
             comb_m_max=3600 * MeV,
             vchi2pdof_max=3,
             docachi2_max=3.0,
             doca_max=0.5 * mm,
             decay_descriptor="[B0 -> mu+ K-]cc",
             name="rd_kaon_muon_for_btosmutau_{hash}"):
    """Builder for X -> K mu decays, used in XMuTau lines"""
    combination_code = F.require_all(F.MASS < comb_m_max,
                                     F.MAXSDOCACHI2CUT(docachi2_max),
                                     F.MAXSDOCACUT(doca_max))
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max)
    kmu = ParticleCombiner([muons, kaons],
                           DecayDescriptor=decay_descriptor,
                           CombinationCut=combination_code,
                           CompositeCut=vertex_code,
                           name=name)
    return kmu


def make_kstmu(kaons,
               pions,
               muons,
               pvs,
               comb_m_min=600 * MeV,
               comb_m_max=3550 * MeV,
               vchi2pdof_max=6,
               docachi2_max=6.0,
               doca_max=0.6 * mm,
               kst_docachi2_max=3.0,
               kst_doca_max=0.12 * mm,
               kst_mass_min=450 * MeV,
               decay_descriptor="[B0 -> K+ pi- mu+]cc",
               name="rd_kpi_muon_for_btosmutau_{hash}"):
    """Builder for X -> K pi mu decays, used in XMuTau lines"""
    combination_code = F.require_all(F.MASS > comb_m_min, F.MASS < comb_m_max,
                                     F.MAXSDOCACHI2CUT(docachi2_max),
                                     F.MAXSDOCACUT(doca_max))
    two_body_combination_code = F.require_all(
        F.MASS > kst_mass_min, F.MASS < comb_m_max,
        F.MAXSDOCACHI2CUT(kst_docachi2_max), F.MAXSDOCACUT(kst_doca_max))
    vertex_code = F.CHI2DOF < vchi2pdof_max
    kstmu = ParticleCombiner([kaons, pions, muons],
                             DecayDescriptor=decay_descriptor,
                             Combination12Cut=two_body_combination_code,
                             CombinationCut=combination_code,
                             CompositeCut=vertex_code,
                             name=name)
    return kstmu


def make_pkmu(kaons,
              protons,
              muons,
              pvs,
              comb_m_min=1400 * MeV,
              comb_m_max=5000 * MeV,
              vchi2pdof_max=8,
              docachi2_max=8.0,
              doca_max=0.8 * mm,
              pk_docachi2_max=4.0,
              pk_doca_max=0.12 * mm,
              pK_mass_min=900 * MeV,
              decay_descriptor="[B0 -> p+ K- mu+]cc",
              name="rd_pk_muon_for_btosmutau_{hash}"):
    """Builder for X -> p K mu decays, used in XMuTau lines"""
    combination_code = F.require_all(F.MASS > comb_m_min, F.MASS < comb_m_max,
                                     F.MAXSDOCACHI2CUT(docachi2_max),
                                     F.MAXSDOCACUT(doca_max))
    two_body_combination_code = F.require_all(
        F.MASS > pK_mass_min, F.MASS < comb_m_max,
        F.MAXSDOCACHI2CUT(pk_docachi2_max), F.MAXSDOCACUT(pk_doca_max))
    vertex_code = F.CHI2DOF < vchi2pdof_max
    pkmu = ParticleCombiner([protons, kaons, muons],
                            DecayDescriptor=decay_descriptor,
                            Combination12Cut=two_body_combination_code,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name=name)
    return pkmu


def make_bs(kkmus,
            muons,
            pvs,
            comb_m_max=7000 * MeV,
            vchi2pdof_max=75,
            dira_min=0.999,
            bpvfdchi2_min=16,
            decay_descriptor="[B_s0 -> B0 mu-]cc",
            name="rd_make_bs_to_kktaumu_{hash}"):
    """Builder for Bs -> X mu tau(-> mu nu nu) decays, where X is K K"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    bs = ParticleCombiner([kkmus, muons],
                          DecayDescriptor=decay_descriptor,
                          CombinationCut=combination_code,
                          CompositeCut=vertex_code,
                          name=name)
    return bs


def make_bu(kmus,
            muons,
            pvs,
            comb_m_max=6750 * MeV,
            vchi2pdof_max=50,
            dira_min=0.999,
            bpvfdchi2_min=50,
            decay_descriptor="[B_s0 -> B0 mu-]cc",
            name="rd_make_bu_to_kmutau_{hash}"):
    """Builder for Bs -> X mu tau(-> mu nu nu) decays, where X is K"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    bu = ParticleCombiner([kmus, muons],
                          DecayDescriptor=decay_descriptor,
                          CombinationCut=combination_code,
                          CompositeCut=vertex_code,
                          name=name)
    return bu


def make_lb(pkmus,
            muons,
            pvs,
            comb_m_max=7250 * MeV,
            vchi2pdof_max=75,
            dira_min=0.999,
            bpvfdchi2_min=16,
            decay_descriptor="[Lambda_b0 -> B0 mu-]cc",
            name="rd_make_lb_to_pkmutau_{hash}"):
    """Builder for Lb -> X mu tau(-> mu nu nu) decays, where X is p K"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    lb = ParticleCombiner([pkmus, muons],
                          DecayDescriptor=decay_descriptor,
                          CombinationCut=combination_code,
                          CompositeCut=vertex_code,
                          name=name)
    return lb


@configurable
def make_bd(kstmus,
            muons,
            pvs,
            comb_m_max=6750 * MeV,
            vchi2pdof_max=50,
            dira_min=0.999,
            bpvfdchi2_min=25,
            decay_descriptor="[B_s0 -> B0 mu+]cc",
            name="rd_make_bd_to_kpimutau_{hash}"):
    """Builder for Bd -> X mu tau(-> mu nu nu) decays, where X is K pi"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    bd = ParticleCombiner([kstmus, muons],
                          DecayDescriptor=decay_descriptor,
                          CombinationCut=combination_code,
                          CompositeCut=vertex_code,
                          name=name)
    return bd
