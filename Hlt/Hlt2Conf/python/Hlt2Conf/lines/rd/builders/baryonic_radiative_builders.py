###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of builders used for radiative b-decays.

List of the builders:
    make_rd_xibm - Builder for Xi_b- -> Xi- Gamma
    make_rd_obm - Builder for Omega_b- -> Omega- Gamma
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

import Functors as F
from Functors.math import in_range

from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Hlt2Conf.standard_particles import make_LambdaLL

from PyConf import configurable

# Define the masses of composite particles.
_Obm_M = 6046.1
_Xibm_M = 5797.0


@configurable
def make_rd_xibm(
        xims,
        photons,
        pvs,
        name='rd_xibm',
        comb_mass_window=1200. * MeV,
        mass_window=1000. * MeV,
        pt_min=1.5 * GeV,
        p_min=30 * GeV,
        sum_pt_min=5 * GeV,
):
    combination_cut = F.require_all(
        in_range(_Xibm_M - comb_mass_window, F.MASS,
                 _Xibm_M + comb_mass_window),
        F.SUM(F.PT) > sum_pt_min)

    composite_cut = F.require_all(
        in_range(_Xibm_M - mass_window, F.MASS, _Xibm_M + mass_window),
        F.PT > pt_min, F.P > p_min)

    return ParticleCombiner(
        [xims, photons],
        name=name,
        DecayDescriptor="[Xi_b- -> Xi- gamma]cc",
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
        ParticleCombiner="ParticleAdder",
    )


@configurable
def make_rd_obm(
        omegams,
        photons,
        pvs,
        name='rd_obm',
        comb_mass_window=1200. * MeV,
        mass_window=1000. * MeV,
        pt_min=1.5 * GeV,
        p_min=30 * GeV,
        sum_pt_min=5 * GeV,
):
    combination_cut = F.require_all(
        in_range(_Obm_M - comb_mass_window, F.MASS, _Obm_M + comb_mass_window),
        F.SUM(F.PT) > sum_pt_min)

    composite_cut = F.require_all(
        in_range(_Obm_M - mass_window, F.MASS, _Obm_M + mass_window),
        F.PT > pt_min, F.P > p_min)

    return ParticleCombiner(
        [omegams, photons],
        name=name,
        DecayDescriptor="[Omega_b- -> Omega- gamma]cc",
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
        ParticleCombiner="ParticleAdder",
    )


def LB_TMVA_BDT(pvs, topo="LL"):
    # ROOT TMVA GBDT
    xml_file_loc = f'paramfile://data/Hlt2RadiativeLbToL0Gamma{topo}_bdt_2024.xml'

    bdt_vars = {
        "Lb_PT": F.PT,
        "Lb_P": F.P,
        "Lb_MASS": F.MASS,
        "Lambda0_PT": F.CHILD(1, F.PT),
        "Lambda0_LOGBPVIPCHI2": F.CHILD(1, F.math.log(F.BPVIPCHI2(pvs))),
        "Lambda0_BPVIP": F.CHILD(1, F.BPVIP(pvs)),
        "Lambda0_BPVFD": F.CHILD(1, F.BPVFD(pvs)),
        "gamma_PT": F.CHILD(2, F.PT),
        "gamma_P": F.CHILD(2, F.P),
        "pi_minus_PT": F.CHILD(1, F.CHILD(2, F.PT)),
        "pi_minus_P": F.CHILD(1, F.CHILD(2, F.P)),
        "p_plus_PT": F.CHILD(1, F.CHILD(1, F.PT)),
        "p_plus_P": F.CHILD(1, F.CHILD(1, F.P))
    }

    return F.MVA(
        MVAType='TMVA',
        Config={
            'XMLFile': xml_file_loc,
            'Name': 'BDT'
        },
        Inputs=bdt_vars)


#  Filter Lambdas from standard particles
@configurable
def filter_lambdasLL_for_lb_to_lg(
        pvs,
        trghostprob_max=0.4,
        PIDp_min=0,
        p_pt_min=1000.0 * MeV,  # Can increase up to 1000 MeV
        p_ip_min=0.1 * mm,
        pi_pt_min=100.0 * MeV,
        L0_pt_min=0.5 * GeV,
        L0_p_min=5.0 * GeV,
        L0_ip_min=0.05 * mm,
        vz_min=-100 * mm,
        vz_max=600 * mm,
        vchi2=25):

    code = F.require_all(
        F.NINTREE(
            F.require_all(F.ISBASICPARTICLE,
                          F.GHOSTPROB < trghostprob_max)) == 2,
        F.MINTREE(F.IS_ABS_ID('pi+'), F.PT) > pi_pt_min,
        F.MINTREE(F.IS_ABS_ID('p+'), F.PT) > p_pt_min,
        F.MINTREE(F.IS_ABS_ID('p+'), F.PID_P) > PIDp_min,
        F.INTREE(F.require_all(F.IS_ABS_ID('p+'),
                               F.MINIP(pvs) > p_ip_min)),
        F.CHI2 < vchi2,  # "In principle"
        F.MINIP(pvs) > L0_ip_min,
        F.math.in_range(vz_min, F.END_VZ, vz_max),
        F.PT > L0_pt_min,
        F.P > L0_p_min,
        F.TWOBODY_TILT("Y") > 0)

    return ParticleFilter(make_LambdaLL(), F.FILTER(code))


# define Lb->LG builders
@configurable
def make_rd_lb_to_lgamma_LL(Lambda,
                            gamma,
                            pvs,
                            name="make_rd_lb_to_lgamma_LL",
                            Lb_UM=6119.6 * MeV,
                            Lb_LM=5119.6 * MeV,
                            Lb_PT_Min=1000 * MeV,
                            Lb_Sum_PT_Min=1000.0 * MeV):  # increase to 3000
    """Make the Lb --> Lambda Gamma candidate"""

    combination_cut = F.require_all(
        in_range(Lb_LM, F.MASS, Lb_UM),
        F.SUM(F.PT) > Lb_Sum_PT_Min)

    vertex_cut = F.require_all(
        in_range(Lb_LM, F.MASS, Lb_UM), F.PT > Lb_PT_Min)

    return ParticleCombiner(
        Inputs=[Lambda, gamma],
        DecayDescriptor="[Lambda_b0 -> Lambda0 gamma]cc",
        ParticleCombiner="MomentumCombiner",
        CombinationCut=combination_cut,
        CompositeCut=vertex_cut,
        name=name)
