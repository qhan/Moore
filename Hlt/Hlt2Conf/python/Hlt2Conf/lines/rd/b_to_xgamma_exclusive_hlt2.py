###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of exclusive radiative  B -> X gamma Hlt2 lines:
- B_s0 -> ( phi(1020) -> K K ) gamma
- B0 -> (K*(892) -> K pi ) gamma

author: Izaac Sanderswood
date: 25.05.22

PID-exclusive B -> XX gamma Hlt2 lines with LL converted gamma
- B0 -> K pi gamma
- B_s0 -> K K gamma
- B0 -> pi pi gamma
- L_b0 -> p K gamma

author: Fionn Bishop
date: 05.02.24

"""
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs

from GaudiKernel.SystemOfUnits import GeV, MeV
import Functors as F
from math import cos
from Hlt2Conf.algorithms_thor import ParticleFilter

from Hlt2Conf.lines.rd.builders.rdbuilder_thor import (
    make_rd_detached_kstar0s, make_rd_detached_phis, make_rd_photons,
    make_rd_detached_rho0)

from Hlt2Conf.lines.rd.builders.b_to_xgamma_exclusive_builders import (
    make_b2xgamma_excl, make_rd_detached_pK)

from Hlt2Conf.lines.rd.builders.rad_incl_builder import (make_gamma_ee)

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES

all_lines = {}


@register_line_builder(all_lines)
def bs_to_phigamma_line(name="Hlt2RD_BsToPhiGamma",
                        persistreco=False,
                        prescale=1.):

    pvs = make_pvs()
    # remember to also look at the definition of make_rd_detached_phis as only some cuts are changed here
    phis = make_rd_detached_phis(
        name="rd_BsToPhiGamma_detached_phis",
        k_p_min=2. * GeV,
        k_pt_min=500. * MeV,
        k_ipchi2_min=11.,
        phi_pt_min=1500. * MeV,
        vchi2pdof_max=15.)

    photons = make_rd_photons(
        et_min=2. * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2)

    b_s0 = make_b2xgamma_excl(
        intermediate=phis,
        photons=photons,
        pvs=pvs,
        descriptor="B_s0 -> phi(1020) gamma",
        dira_min=cos(0.1),
        name="rd_BsToPhiGamma_Combiner")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [phis, b_s0],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


@register_line_builder(all_lines)
def bd_to_kstgamma_line(name="Hlt2RD_BdToKstGamma",
                        persistreco=True,
                        prescale=1.):

    pvs = make_pvs()

    # remember to also look at the definition of make_rd_detached_kstar0s as only some cuts are changed here
    kst = make_rd_detached_kstar0s(
        name="rd_BdToKstGamma_detached_kstar0s_{hash}",
        pi_p_min=2. * GeV,
        pi_pt_min=500. * MeV,
        k_p_min=3. * GeV,
        k_pt_min=500. * MeV,
        k_ipchi2_min=11.,
        pi_ipchi2_min=11.,
        k_pid=((F.PID_K > 0.) & ((F.PID_K - F.PID_P) > 2)),
        kstar0_pt_min=1500. * MeV,
        vchi2pdof_max=15.,
        am_min=800. * MeV,
        am_max=1000. * MeV,
    )

    photons = make_rd_photons(
        et_min=2. * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2)

    b0 = make_b2xgamma_excl(
        intermediate=kst,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> K*(892)0 gamma]cc",
        dira_min=cos(0.045),
        name="rd_BdToKstGamma_Combiner_{hash}")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kst, b0],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


@register_line_builder(all_lines)
def bd_to_kpigamma_gammatoeeLL_line(name="Hlt2RD_BdToKpPimGamma_GammaToEELL",
                                    persistreco=False,
                                    prescale=1.):

    pvs = make_pvs()
    kpi = ParticleFilter(
        make_rd_detached_kstar0s(
            name="rd_detached_kstar0s_{hash}",
            am_max=2000. * MeV,
            k_pid=(F.PID_K > 2.),
            pi_pid=(F.PID_K < 2.),
            k_ipchi2_min=9.,
            pi_ipchi2_min=9.,
            k_p_min=2. * GeV,
            pi_p_min=2. * GeV,
            k_pt_min=250. * MeV,
            pi_pt_min=250. * MeV,
            vchi2pdof_max=10.,
            adocachi2cut=10.,
            kstar0_pt_min=800. * MeV,
        ),
        F.FILTER(F.require_all(F.MASS > 767. * MeV, F.MASS < 1825. * MeV)),
        name="rd_detached_kstar0s_tightermass_{hash}")

    photons = make_gamma_ee(pvs, pid_e_min=1)

    b = make_b2xgamma_excl(
        intermediate=kpi,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> K*(892)0 gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4. * GeV,
        bpv_fdchi2_min=25.,
        vchi2pdof_max=10.,
        name="rd_BdToKpiGamma_GammaToEELL_Combiner_{hash}")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kpi, b],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bs_to_kkgamma_gammatoeeLL_line(name="Hlt2RD_BsToKpKmGamma_GammaToEELL",
                                   persistreco=False,
                                   prescale=1.):

    pvs = make_pvs()
    kk = ParticleFilter(
        make_rd_detached_phis(
            name="rd_detached_phis_{hash}",
            am_max=2000. * MeV,
            k_pid=(F.PID_K > 2.),
            k_ipchi2_min=9.,
            k_p_min=2. * GeV,
            k_pt_min=250. * MeV,
            adocachi2cut=10.,
            vchi2pdof_max=10.,
            phi_pt_min=800. * MeV,
        ),
        F.FILTER(F.require_all(F.MASS < 1850. * MeV)),
        name="rd_detached_phis_tightermass_{hash}")

    photons = make_gamma_ee(pvs, pid_e_min=1)

    b = make_b2xgamma_excl(
        intermediate=kk,
        photons=photons,
        pvs=pvs,
        descriptor="[B_s0 -> phi(1020) gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4. * GeV,
        bpv_fdchi2_min=25.,
        vchi2pdof_max=10.,
        name="rd_BsToKKGamma_GammaToEELL_Combiner_{hash}")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kk, b],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bd_to_pipigamma_gammatoeeLL_line(name="Hlt2RD_BdToPipPimGamma_GammaToEELL",
                                     persistreco=False,
                                     prescale=1.):

    pvs = make_pvs()
    pipi = ParticleFilter(
        make_rd_detached_rho0(
            name="rd_detached_rho0_{hash}",
            am_max=2000. * MeV,
            pi_pid=(F.PID_K < 2.),
            pi_ipchi2_min=9.,
            pi_p_min=2. * GeV,
            pi_pt_min=250. * MeV,
            adocachi2cut=10.,
            vchi2pdof_max=10.,
            pt_min=800. * MeV,
        ),
        F.FILTER(F.require_all(F.MASS < 1850. * MeV)),
        name="rd_detached_rhos_tightermass_{hash}")

    photons = make_gamma_ee(pvs, pid_e_min=1)

    b = make_b2xgamma_excl(
        intermediate=pipi,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> rho(770)0 gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4. * GeV,
        bpv_fdchi2_min=25.,
        vchi2pdof_max=10.,
        name="rd_BdTopipiGamma_GammaToEELL_Combiner_{hash}")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [pipi, b],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lb_to_pkgamma_gammatoeeLL_line(name="Hlt2RD_LbToPpKmGamma_GammaToEELL",
                                   persistreco=False,
                                   prescale=1.):

    pvs = make_pvs()
    pk = ParticleFilter(
        make_rd_detached_pK(
            am_min=1300. * MeV,
            am_max=2800. * MeV,
            p_pid=F.require_all(F.PID_P > 2, (F.PID_P - F.PID_K > 2.)),
            k_pid=(F.PID_K > 2.),
            p_ipchi2_min=9.,
            k_ipchi2_min=9.,
            k_p_min=2. * GeV,
            p_p_min=2. * GeV,
            k_pt_min=250. * MeV,
            p_pt_min=250. * MeV,
            adocachi2cut=10.,
            vchi2pdof_max=10.,
            pK_pt_min=800. * MeV,
        ),
        F.FILTER(F.require_all(F.MASS > 1375. * MeV, F.MASS < 2625. * MeV)),
        name="rd_detached_pK_tightermass_{hash}")

    photons = make_gamma_ee(pvs, pid_e_min=1)

    b = make_b2xgamma_excl(
        intermediate=pk,
        photons=photons,
        pvs=pvs,
        descriptor="[Lambda_b0 -> Lambda(1520)0 gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4. * GeV,
        bpv_fdchi2_min=25.,
        vchi2pdof_max=10.,
        name="rd_LbTopKGamma_GammaToEELL_Combiner_{hash}")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [pk, b],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )
