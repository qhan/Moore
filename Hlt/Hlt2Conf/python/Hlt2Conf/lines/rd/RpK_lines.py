###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of lines for the R(pK) measurement

The following modes are included:

    - Lambdab -> P K Mu Mu
    - Lambdab -> P K Mu Mu (SS)
    - Lambdab -> P K E E
    - Lambdab -> P K E E (SS)

"""
from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from .builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES
from .builders.rdbuilder_thor import (make_rd_detached_dimuon,
                                      make_rd_detached_dielectron)
from .builders.RpK_builders import (make_lambdab_to_pkll,
                                    make_dihadron_from_pK)
from Hlt2Conf.lines.rd.builders.rd_isolation import parent_isolation_output

all_lines = {}


@register_line_builder(all_lines)
@configurable
def lambdab_to_pkmumu_line(name="Hlt2RD_LambdabToPKMuMu", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    dileptons = make_rd_detached_dimuon(vchi2pdof_max=10)
    dihadrons = make_dihadron_from_pK()
    lambdab = make_lambdab_to_pkll(dileptons, dihadrons, descriptor)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [lambdab],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Lb", lambdab),
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def lambdab_to_pkmumu_ss_line(name="Hlt2RD_LambdabToPKMuMu_SS", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    dileptons = make_rd_detached_dimuon(vchi2pdof_max=10)
    dihadrons = make_dihadron_from_pK(same_sign=True)
    lambdab = make_lambdab_to_pkll(dileptons, dihadrons, descriptor)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [lambdab],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Lb", lambdab),
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def lambdab_to_pkee_line(name="Hlt2RD_LambdabToPKEE", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    dileptons = make_rd_detached_dielectron()
    dihadrons = make_dihadron_from_pK()
    lambdab = make_lambdab_to_pkll(dileptons, dihadrons, descriptor)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [lambdab],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Lb", lambdab),
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def lambdab_to_pkee_ss_line(name="Hlt2RD_LambdabToPKEE_SS", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    dileptons = make_rd_detached_dielectron()
    dihadrons = make_dihadron_from_pK(same_sign=True)
    lambdab = make_lambdab_to_pkll(dileptons, dihadrons, descriptor)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [lambdab],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Lb", lambdab),
        monitoring_variables=_RD_MONITORING_VARIABLES)
