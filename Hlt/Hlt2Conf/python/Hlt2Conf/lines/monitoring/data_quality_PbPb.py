###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Postscaled (zero rate) monitoring lines for DQ for PbPb data-taking.
The lines are not registered in __init__.py of the monitoring lines as these are special lines for PbPb data-taking.

The module contains the following lines:
- Hlt2DQ_KsToPimPip_LL (KS0 -> pi+ pi-)
- Hlt2DQ_L0ToPpPim_LL ([Lambda0 -> p+ pi-]cc)
- Hlt2DQ_D0ToKmPip ([D0 -> K- pi+]cc)
- Hlt2DQ_DpToKmPipPip ([D+ -> K- pi+ pi+]cc)
- Hlt2DQ_JpsiToMumMup (J/psi(1S) -> mu+ mu-)
- Hlt2DQ_JpsiToEmEp (J/psi(1S) -> e+ e-)
"""
from GaudiKernel.SystemOfUnits import (GeV, MeV, mm, micrometer as um)
from PyConf.Algorithms import FunctionalDiElectronMaker
from SelAlgorithms.monitoring import histogram_1d, monitor
import Functors as F
from Functors.math import log

from RecoConf.event_filters import require_pvs, require_gec
from RecoConf.reconstruction_objects import make_pvs
from Moore.lines import Hlt2Line
from Moore.config import register_line_builder

from Hlt2Conf.standard_particles import (
    make_long_pions, make_has_rich_long_pions, make_has_rich_long_kaons,
    make_long_protons, make_ismuon_long_muon, make_long_electrons_no_brem)
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter


def _pbpb_prefilters():
    return [require_gec(cut=30_000, skipUT=True), require_pvs(make_pvs())]


def _MIP_MIN(cut, pvs=make_pvs):
    return F.MINIPCUT(IPCut=cut, Vertices=pvs())


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


def _filter_long_pions_for_strange():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 80 * MeV, _MIP_MIN(0.6 * mm),
                          _MIPCHI2_MIN(12))))


# take the charm pi/K filter from d0_to_hh
def _filter_long_pions_for_charm():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 600 * MeV,
                F.P > 5 * GeV,
                F.PID_K < 5.,
                _MIPCHI2_MIN(4),
            ), ),
    )


def _filter_long_kaons_for_charm():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 600 * MeV,
                F.P > 5 * GeV,
                F.PID_K > 5.,
                _MIPCHI2_MIN(4),
            ), ),
    )


all_lines = {}


@register_line_builder(all_lines)
def _kshort_ll_line(name="Hlt2DQ_KsToPimPip_LL"):
    ks = ParticleCombiner(
        [_filter_long_pions_for_strange(),
         _filter_long_pions_for_strange()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name='DQ_KS0_LL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(420 * MeV, F.MASS, 570 * MeV),
            F.PT > 300 * MeV,
            F.MAXDOCACUT(300 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(450 * MeV, F.MASS, 550 * MeV),
            F.PT > 350 * MeV,
            F.CHI2DOF < 12.,
            F.BPVVDZ(make_pvs()) > 2 * mm,
            F.math.in_range(-100 * mm, F.END_VZ, 700 * mm),
            F.BPVVDRHO(make_pvs()) > 1. * mm,
            F.BPVFDCHI2(make_pvs()) > 20.,
            F.BPVDIRA(make_pvs()) > 0.9995,
        ),
    )
    ks_fd_mon = monitor(
        data=ks,
        histograms=[
            histogram_1d(f"/{name}/Ks_FDlog", "Ks_FDlog",
                         log(F.BPVFD(make_pvs())), 60, (1, 8), "Ks_FDlog")
        ])
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + [ks, ks_fd_mon],
        postscale=0.,
        persistreco=True,
    )


@register_line_builder(all_lines)
def _lambda_ll_line(name="Hlt2DQ_L0ToPpPim_LL"):
    proton = ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.PID_P > -5., _MIP_MIN(80. * um),
                          _MIPCHI2_MIN(9))))
    lz = ParticleCombiner(
        [proton, _filter_long_pions_for_strange()],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="DQ_Lambda_LL",
        CombinationCut=F.require_all(
            F.MASS < 1180 * MeV,
            F.PT > 450 * MeV,
            F.MAXDOCACUT(300 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1080 * MeV, F.MASS, 1140 * MeV),
            F.PT > 500 * MeV,
            F.CHI2DOF < 16.,
            F.BPVVDZ(make_pvs()) > 12 * mm,
            F.math.in_range(-100 * mm, F.END_VZ, 700 * mm),
            F.BPVVDRHO(make_pvs()) > 1.5 * mm,
            F.BPVDIRA(make_pvs()) > 0.999,
            F.BPVFDCHI2(make_pvs()) > 20,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + [lz],
        postscale=0.,
        persistreco=True,
    )


@register_line_builder(all_lines)
def _d0_to_kpi_line(name="Hlt2DQ_D0ToKmPip"):
    d0 = ParticleCombiner(
        [_filter_long_kaons_for_charm(),
         _filter_long_pions_for_charm()],
        DecayDescriptor="[D0 -> K- pi+]cc",
        name="DQ_D0ToKmPip",
        CombinationCut=F.require_all(
            F.math.in_range(1685 * MeV, F.MASS, 2045 * MeV),
            F.PT > 1.5 * GeV,
            F.MAX(F.PT) > 1200 * MeV,
            F.MAXDOCACUT(0.1 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1715 * MeV, F.MASS, 2015 * MeV),
            F.CHI2DOF < 15.,
            F.BPVFDCHI2(make_pvs()) > 8.,
            F.BPVDIRA(make_pvs()) > 0.99985,
        ),
    )

    d_ipchi2_mon = histogram_1d(
        functor=log(F.MINIPCHI2(make_pvs())),
        name=f"/{name}/D0_logIPCHI2",
        title="D0_logIPCHI2",
        bins=60,
        range=(-6, 12),
        label="D0_logIPCHI2",
    )
    d_pasy_mon = histogram_1d(
        functor=(F.CHILD(1, F.P) - F.CHILD(2, F.P)) /
        (F.CHILD(1, F.P) + F.CHILD(2, F.P)),
        name=f"/{name}/D0_Daughters_Pasy",
        title="D0_Daughters_Pasy",
        bins=60,
        range=(-1, 1),
        label="D0_Daughters_Pasy",
    )
    d_ptasy_mon = histogram_1d(
        functor=(F.CHILD(1, F.PT) - F.CHILD(2, F.PT)) /
        (F.CHILD(1, F.PT) + F.CHILD(2, F.PT)),
        name=f"/{name}/D0_Daughters_PTasy",
        title="D0_Daughters_PTasy",
        bins=60,
        range=(-1, 1),
        label="D0_Daughters_PTasy",
    )

    pi_pidk_mon = histogram_1d(
        functor=F.CHILD(2, F.PID_K),
        name=f"/{name}/pi_PIDk",
        title="pi_PIDk",
        bins=60,
        range=(-150, 5),
        label="pi_PIDk",
    )

    K_pidk_mon = histogram_1d(
        functor=F.CHILD(1, F.PID_K),
        name=f"/{name}/K_PIDk",
        title="K_PIDk",
        bins=60,
        range=(5, 150),
        label="K_PIDk",
    )

    d0_monitor = monitor(
        Input=d0,
        Histograms=[
            d_ipchi2_mon, d_pasy_mon, d_ptasy_mon, pi_pidk_mon, K_pidk_mon
        ])

    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + [d0, d0_monitor],
        postscale=0.,
        persistreco=True,
    )


@register_line_builder(all_lines)
def _dp_to_kpipi_line(name="Hlt2DQ_DpToKmPipPip"):
    dp = ParticleCombiner(
        [
            _filter_long_kaons_for_charm(),
            _filter_long_pions_for_charm(),
            _filter_long_pions_for_charm()
        ],
        DecayDescriptor="[D+ -> K- pi+ pi+]cc",
        name="DQ_DpToKmPipPip",
        CombinationCut=F.require_all(
            F.math.in_range(1750 * MeV, F.MASS, 1990 * MeV),
            F.PT > 1.2 * GeV,
            F.P > 10 * GeV,
            F.SUM(F.PT) > 1.2 * GeV,
            F.MAXSDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1770 * MeV, F.MASS, 1970 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 12 * GeV,
            F.CHI2DOF < 12.,
            F.BPVVDZ(make_pvs()) > 1.5 * mm,
            F.BPVDIRA(make_pvs()) > 0.999,
            F.BPVFDCHI2(make_pvs()) > 32.,
        ),
    )
    d_m_plus_mon = histogram_1d(
        functor=F.MASS * F.CHARGE,
        name=f"/{name}/Dp_M",
        title="Dp_M",
        label="Dp_M",
        bins=60,
        range=(1779 * MeV, 1959 * MeV),
    )

    d_m_minus_mon = histogram_1d(
        functor=F.MASS * F.CHARGE * (-1),
        name=f"/{name}/Dm_M",
        title="Dm_M",
        label="Dm_M",
        bins=60,
        range=(1779 * MeV, 1959 * MeV),
    )

    dp_monitor = monitor(data=dp, histograms=[d_m_minus_mon, d_m_plus_mon])

    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + [dp, dp_monitor],
        postscale=0.,
        persistreco=True,
    )


@register_line_builder(all_lines)
def _jpsi_to_mumu_line(name="Hlt2DQ_JpsiToMumMup"):

    muons = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV,
                          F.MINIP(make_pvs()) < 120 * um, F.PID_MU > 0.)))
    jpsis = ParticleCombiner(
        [muons, muons],
        name="DQ_JpsiToMumMup",
        DecayDescriptor="J/psi(1S) -> mu+ mu-",
        CombinationCut=F.require_all(
            F.math.in_range(2.7 * GeV, F.MASS, 3.4 * GeV),
            F.SUM(F.PT) > 1.0 * GeV,
            F.MAXSDOCACUT(0.1 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2.8 * GeV, F.MASS, 3.3 * GeV),
            F.CHI2DOF < 15.,
            F.PT > 1 * GeV,
        ),
    )

    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + [jpsis],
        postscale=0.,
        persistreco=True,
    )


@register_line_builder(all_lines)
def _jpsi_to_ee_line(name="Hlt2DQ_JpsiToEmEp"):
    es = ParticleFilter(
        make_long_electrons_no_brem(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV,
                          F.MINIP(make_pvs()) < 120 * um, F.PID_E > 5.)))
    dielec = FunctionalDiElectronMaker(
        InputParticles=es,
        name="DQ_JpsiToEmEp",
        DiElecID="J/psi(1S)",
        MinDiElecPT=1 * GeV,
        MinDiElecMass=2.5 * GeV,
        MaxDiElecMass=3.4 * GeV).Particles
    jpsis = ParticleFilter(dielec, F.FILTER(F.CHI2DOF < 9.))

    e_hasbrem_mon = histogram_1d(
        functor=F.HASBREM,
        name=f"/{name}/e_hasBrem",
        title="e_hasBrem",
        label="e_hasBrem",
        bins=2,
        range=(-0.1, 1.1),
    )

    e_ecalpide_mon = histogram_1d(
        functor=F.VALUE_OR(F.NaN) @ F.ECALPIDE,
        name=f"/{name}/e_ECALPIDe",
        title="e_ECALPIDe",
        label="e_ECALPIDe",
        bins=60,
        range=(-10, 10),
    )

    e_brempide_mon = histogram_1d(
        functor=F.VALUE_OR(F.NaN) @ F.BREMPIDE,
        name=f"/{name}/e_BREMPIDe",
        title="e_BREMPIDe",
        label="e_BREMPIDe",
        bins=60,
        range=(-10, 10),
    )

    electron_monitor = monitor(
        data=es, histograms=[e_hasbrem_mon, e_ecalpide_mon, e_brempide_mon])

    jpsi_m_mon = monitor(
        data=jpsis,
        histograms=[
            histogram_1d(
                functor=F.MASS,
                name=f"/{name}/m",
                title="m",
                label="MASS",
                bins=45,
                range=(2.5 * GeV, 3.4 * GeV),
            )
        ])

    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + [jpsis, electron_monitor, jpsi_m_mon],
        postscale=0.,
        persistreco=True,
    )
