###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import (
    LumiCountersFromCalo,
    LumiCountersFromRich,
    LumiCountersFromVeloHits,
    LumiCountersFromVeloTracks,
    LumiCountersFromPVs,
    LumiCounterMerger,
    HltLumiSummaryMonitor,
    HltLumiWriter,
)

from PyConf.application import make_odin
from PyConf.filecontent_metadata import register_encoding_dictionary
from PyConf.utilities import ConfigurationError
from Moore.config import register_line_builder
from Moore.lines import Hlt2LuminosityLine, Hlt2LumiCountersLine, Hlt2Line
from AllenConf.lumi_schema_generator import LumiSchemaGenerator  # FIXME move this to LHCb

from RecoConf.calorimeter_reconstruction import make_digits
from RecoConf.rich_reconstruction import make_rich_pixels, default_rich_reco_options
from RecoConf.legacy_rec_hlt1_tracking import (
    make_legacy_rec_hlt1_tracks, make_pvs, make_VeloClusterTrackingSIMD_hits,
    make_PatPV3DFuture_pvs, all_velo_track_types)

calibration_lines = {}


# Intentionally do not put this line in a dict since it will be included
# in multiple streams and it can only be instantiated once.
def lumi_nanofy_line(name="Hlt2Lumi"):
    line = Hlt2LuminosityLine()
    assert line.name == name
    return line


def generate_lumi_encoding_table(lumi_counter_algs):
    counterSpecs = []
    counterFactors = {}

    # The order of iteration must be deterministic to ensure the same schema is generated
    for p in sorted(lumi_counter_algs, key=lambda p: p.name):
        props = p.properties
        defaults = p.type.getDefaultProperties()

        c_base = ""
        if "CounterBaseName" in props:
            c_base = props["CounterBaseName"]
        elif "CounterBaseName" in defaults:
            c_base = defaults["CounterBaseName"]

        if "CounterNames" in defaults:
            c_names = defaults["CounterNames"]
        elif "CounterName" in props:
            c_names = [defaults["CounterName"]]
        elif "CounterName" in defaults:
            c_names = [defaults["CounterName"]]
        else:
            continue

        if "CounterMax" in props:
            c_max = props["CounterMax"]
        else:
            c_max = defaults["CounterMax"]

        if len(c_names) != len(c_max):
            raise ConfigurationError(
                f"{p.name} has counter name and max value lists of different lengths"
            )
        counterSpecs += zip([c_base + n for n in c_names], c_max)

        c_shiftscale = {}
        if "CounterShiftAndScale" in props:
            c_shiftscale = props["CounterShiftAndScale"]
        else:
            c_shiftscale = defaults["CounterShiftAndScale"]

        for n in c_names:
            if n in c_shiftscale:
                counterFactors[c_base + n] = c_shiftscale[n]

    # Basic sanity checks on the counters
    # All counters should have unique, non-empty names and non-zero sizes
    if any(c[0] == "" for c in counterSpecs):
        raise ConfigurationError("Found lumi counter with empty name")
    if len(counterSpecs) != len(set(c[0] for c in counterSpecs)):
        raise ConfigurationError(
            "Found two lumi counters with identical names")
    if any(c[1] == 0 for c in counterSpecs):
        raise ConfigurationError("Found lumi counter with zero size")

    l = LumiSchemaGenerator(counterSpecs, shiftsAndScales=counterFactors)
    l.process()
    return l.getJSON()


@register_line_builder(calibration_lines)
def lumi_counters_line(name="Hlt2LumiCounters"):
    calo = make_digits()
    ecal = calo["digitsEcal"]
    hcal = calo["digitsHcal"]
    ecal_summary = LumiCountersFromCalo(
        name="LumiCountersFromECal",
        InputContainer=ecal,
        CounterBaseName="ECal")
    hcal_summary = LumiCountersFromCalo(
        name="LumiCountersFromHCal",
        InputContainer=hcal,
        CounterBaseName="HCal")

    rich = make_rich_pixels(
        options=default_rich_reco_options())["RichDecodedData"]
    rich_summary = LumiCountersFromRich(
        name="LumiCountersFromRich",
        InputContainer=rich,
        CounterBaseName="Rich")

    hlt1_tracks = make_legacy_rec_hlt1_tracks()
    pvs = make_pvs()
    velo_tracks = hlt1_tracks["Velo"]["Pr"]
    velo_backwards = hlt1_tracks["Velo"]["Pr::backward"]
    velo_hits = make_VeloClusterTrackingSIMD_hits()
    velo_track_summary = LumiCountersFromVeloTracks(
        name="LumiCountersFromVeloTracks",
        ForwardTracks=velo_tracks,
        BackwardTracks=velo_backwards,
        CounterBaseName="Velo")
    velo_hit_summary = LumiCountersFromVeloHits(
        name="LumiCountersFromVeloHits",
        InputContainer=velo_hits,
        CounterBaseName="Velo")
    pv_summary = LumiCountersFromPVs(
        name="LumiCountersFromPVs", InputContainer=pvs, CounterBaseName="Velo")
    with make_PatPV3DFuture_pvs.bind(use_3D_seeding=True):
        pvs_3d = make_PatPV3DFuture_pvs(
            all_velo_track_types(), location=None)["v3"]
        pv3d_summary = LumiCountersFromPVs(
            name="LumiCountersFromPVs3D",
            InputContainer=pvs_3d,
            CounterBaseName="Velo3D")

    summary_algs = [
        ecal_summary, hcal_summary, rich_summary, velo_track_summary,
        velo_hit_summary, pv_summary, pv3d_summary
    ]
    merger = LumiCounterMerger(InputSummaries=summary_algs)
    table = generate_lumi_encoding_table(summary_algs)
    lumi_key = int(
        register_encoding_dictionary(
            "counters", table, directory="luminosity_counters"), 16)

    monitor = HltLumiSummaryMonitor(
        name="Hlt2LumiSummaryMonitor", Input=merger, ODIN=make_odin())

    encoder = HltLumiWriter(
        SourceID="Hlt2",
        InputBank=merger.OutputSummary,
        EncodingKey=lumi_key,
    )

    line = Hlt2LumiCountersLine(
        algs=[encoder, monitor], output=encoder.OutputView)
    assert line.name == name
    return line


@register_line_builder(calibration_lines)
def _hlt2_lumi_default_raw_banks_line(name='Hlt2LumiDefaultRawBanks',
                                      prescale=1):
    """Propagate raw banks for (some) lumi events.

    Hlt2LuminosityLine only requests the minimal set of banks. In order to propagate the banks
    defined in streams_hlt2.DETECTOR_RAW_BANK_TYPES_PER_STREAM for the LUMI stream, we need
    another line that passes these events.

    """
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1ODINLumiDecision",
        prescale=prescale,
    )


@register_line_builder(calibration_lines)
def _hlt2_lumi_calibration_line(name='Hlt2LumiCalibration', prescale=1):
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1ODIN1kHzLumiDecision",
        raw_banks=['VP', 'UT', 'FT', 'Rich', 'Muon', 'Calo', 'Plume'],
        prescale=prescale,
    )
