###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from PyConf.Algorithms import CopyUnsignedInt
from PyConf.control_flow import CompositeNode, NodeLogic

all_lines = {}


@register_line_builder(all_lines)
def hlt2_velo_noise_line(name: str = 'Hlt2Calib_VeloNoise',
                         prescale: float = 0):
    """Passthrough line for Velo raw banks for Velo noise studies

    """
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="",
        prescale=prescale,
        raw_banks=['VP'],
    )


@register_line_builder(all_lines)
def hlt2_velo_tomography_line(name: str = 'Hlt2Calib_VeloTomography',
                              prescale: float = 0.0001):
    """Passthrough line for Velo tomography

    """
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code=r"Hlt1MaterialVertexSeeds(Downstreamz|_DWFS)Decision",
        prescale=prescale,
        raw_banks=['VP'],
    )


@register_line_builder(all_lines)
def hlt2_tae_line(name: str = 'Hlt2Calib_TAE', prescale: float = 0.33):
    """Passthrough line for TAE events

    """
    line = Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code=r"Hlt1TAE.*Decision",
        prescale=prescale,
        raw_banks=['VP', 'UT', 'FT', 'Rich', 'Muon', 'Calo', 'Plume'],
    )

    copy_alg = CopyUnsignedInt(
        Input="/Event/Banks/TAEHalfWindow",
        Output="/Event/Banks/TAEHalfWindowOutput",
        # see application.py:online_writer()
    )
    line.node = CompositeNode(
        name=line.name + "DecisionWithOutput",
        children=[line.node, copy_alg],
        combine_logic=NodeLogic.LAZY_AND,
    )
    return line


@register_line_builder(all_lines)
def hlt2_beamgas_line(name: str = "Hlt2Calib_BeamGas", prescale: float = 1):
    """Passthrough for beam-gas events

    """
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code=r"Hlt1BGI(?!VeloClustersMicroBias).*Decision",
        raw_banks=['VP', 'UT', 'FT', 'Rich', 'Muon', 'Calo', 'Plume'],
        prescale=prescale,
    )
