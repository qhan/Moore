###############################################################################
# Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration          #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of useful QEE filters and builders
"""

import Functors as F
from GaudiKernel.SystemOfUnits import meter, MeV, GeV

from PyConf import configurable

from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_ismuon_long_muon, make_long_muons
from Hlt2Conf.standard_particles import make_has_rich_long_pions
from Hlt2Conf.standard_particles import make_long_electrons_with_brem, make_down_electrons_no_brem
from Hlt2Conf.standard_particles import make_photons
from Hlt2Conf.standard_particles import make_mva_ttrack_muons, make_mva_ttrack_pions, make_mva_ttrack_kaons
from Hlt2Conf.lines.qee.high_mass_dimuon import make_dimuon_novxt
from Hlt2Conf.lines.qee.high_mass_dielec import make_dielec_novxt

from RecoConf.reconstruction_objects import make_pvs
from RecoConf.ttrack_selections_reco import PVF_with_single_extrapolation


@configurable
def make_filter_tracks(
        make_particles=make_has_rich_long_pions,
        make_pvs=make_pvs,
        name="qee_has_rich_long_pions",
        pt_min=0.25 * GeV,
        p_min=2. * GeV,
        trchi2dof_max=3,  #TBC with Reco
        trghostprob_max=0.4,  #TBC with Reco
        mipchi2dvprimary_min=None,
        pid=None,
        additionalCuts=None):
    """
    Build generic long tracks.
    """
    code = F.require_all(
        F.SIZE(make_pvs()) > 0,
        F.PT > pt_min,
        F.P > p_min,
    )
    if pid is not None:
        code &= pid
    if additionalCuts is not None:
        code &= additionalCuts
    if mipchi2dvprimary_min is not None:
        pvs = make_pvs()
        code &= F.MINIPCHI2(pvs) > mipchi2dvprimary_min

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def muon_filter(min_pt=0. * GeV, require_muID=True):
    #A muon filter: PT

    code = (F.PT > min_pt)
    particles = make_ismuon_long_muon() if require_muID else make_long_muons()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def elec_filter(min_pt=0. * GeV, min_electron_id=-1):
    #An electron filter: PT
    code = F.require_all(F.PT > min_pt, F.PID_E > min_electron_id)
    particles = make_long_electrons_with_brem()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def elec_filter_down(min_pt=0. * GeV, min_electron_id=-1):
    #An down type electron filter: PT

    code = F.require_all(F.PT > min_pt, F.PID_E > min_electron_id)
    particles = make_down_electrons_no_brem()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_qee_photons(name="qee_photons",
                     make_particles=make_photons,
                     pt_min=5. * GeV):
    code = (F.PT > pt_min)
    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_qee_gamma_DD(name="qee_gamma_DD",
                      descriptor="gamma -> e+ e-",
                      am_max=0.5 * GeV,
                      m_max=0.1 * GeV,
                      pt_min=5. * GeV,
                      min_elec_pt=0. * GeV,
                      min_elec_id=0.,
                      maxVertexChi2=16):

    electrons = elec_filter_down(
        min_pt=min_elec_pt, min_electron_id=min_elec_id)
    combination_code = (F.MASS < am_max)
    vertex_code = F.require_all(F.PT > pt_min, F.MASS < am_max,
                                F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[electrons, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_qee_gamma_LL(name="qee_gamma_LL",
                      descriptor="gamma -> e+ e-",
                      am_max=0.5 * GeV,
                      m_max=0.1 * GeV,
                      pt_min=5. * GeV,
                      min_elec_pt=0. * GeV,
                      min_elec_id=0.,
                      maxVertexChi2=16):

    electrons = elec_filter(min_pt=min_elec_pt, min_electron_id=min_elec_id)
    combination_code = (F.MASS < am_max)
    vertex_code = F.require_all(F.PT > pt_min, F.MASS < am_max,
                                F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[electrons, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_photons(photon_type="DD", pt_min=10. * GeV):

    if photon_type == "LL":
        gamma = make_qee_gamma_LL()
    elif photon_type == "DD":
        gamma = make_qee_gamma_DD()
    else:
        gamma = make_qee_photons()

    code = F.require_all(F.PT > pt_min)

    return ParticleFilter(gamma, F.FILTER(code))


@configurable
def lepton_filter(lepton_type="mu", min_pt=10. * GeV):

    if lepton_type == "mu":
        lepts = muon_filter(min_pt=min_pt)
    else:
        lepts = elec_filter(min_pt=min_pt)

    return lepts


@configurable
def Z_To_lepts_filter(lepton_type="mu", min_pt=10. * GeV):

    if lepton_type == "mu":
        muons_for_Z = muon_filter(min_pt=min_pt)
        zcand = make_dimuon_novxt(
            input_muon1=muons_for_Z,
            input_muon2=muons_for_Z,
            decaydescriptor="Z0 -> mu+ mu-",
            min_mass=40 * GeV)
    else:
        electrons_for_Z = elec_filter(min_pt=min_pt)
        zcand = make_dielec_novxt(
            input_elecs=electrons_for_Z,
            decaydescriptor="Z0 -> e+ e-",
            min_mass=40. * GeV)

    return zcand


# For HNL lines


@configurable
def filter_neutral_hadrons(particles,
                           pvs,
                           pt_min=0.5 * GeV,
                           ipchi2_min=0.0,
                           name='qee_rad_incl_neutral_hadrons_{hash}_{hash}'):
    """Returns extra neutral hadrons with the inclusive_radiative_b selections"""
    code = F.require_all(
        F.PT > pt_min,
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_min, Vertices=pvs),
    )
    return ParticleFilter(particles, F.FILTER(code), name=name)


def hnl_prefilter(require_GEC=False):
    from RecoConf.event_filters import require_pvs, require_gec
    from RecoConf.reconstruction_objects import (make_pvs,
                                                 upfront_reconstruction)
    """
    Args:
        require_GEC (bool, optional): require the General Event Cut.
    """
    gec = [require_gec()] if require_GEC else []
    return upfront_reconstruction() + gec + [require_pvs(make_pvs())]


@configurable
def make_majorana_lepton(leptons,
                         pvs,
                         name="standard_lepton_for_majorana",
                         pt_min=0.5 * GeV,
                         p_min=0 * GeV,
                         mipchi2dvprimary_min=25.0,
                         pid=None):
    """
    Filter default leptons for HNL
    """
    return make_filter_tracks(
        make_particles=leptons,
        make_pvs=pvs,
        name=name,
        pt_min=pt_min,
        p_min=p_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_majorana(child1,
                  child2,
                  name='Generic_Majorana',
                  descriptor='',
                  am_min=0.2 * GeV,
                  am_max=7. * GeV,
                  adocachi2=16.,
                  pt_min=0.7 * GeV,
                  vtxchi2_max=9.):
    """
    Make HNL -> lep +  pi.
    """
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXSDOCACHI2CUT(adocachi2))
    majorana_code = F.require_all(F.PT > pt_min, F.CHI2 < vtxchi2_max)

    return ParticleCombiner([child1, child2],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=majorana_code)


@configurable
def make_bhadron_majorana(majoranas,
                          bachelor,
                          pvs,
                          name='Generic_B_2_Majorana',
                          descriptor='',
                          am_min=4.3 * GeV,
                          am_max=7.2 * GeV,
                          m_min=4.5 * GeV,
                          m_max=6.8 * GeV,
                          adocachi2=25.,
                          vtxchi2_max=9.,
                          mipchi2_max=16.,
                          bpvdls_min=4.,
                          dira_min=0.):
    """
    Make B-> lep + HNL
    """
    #majoranas = make_majorana()
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(adocachi2))
    b_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.BPVDLS(pvs()) > bpvdls_min, F.CHI2 < vtxchi2_max,
        F.BPVDIRA(pvs()) > dira_min,
        F.MINIPCHI2(pvs()) < mipchi2_max)
    return ParticleCombiner([majoranas, bachelor],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=b_code)


@configurable
def make_long_lepton_forHNL(make_long,
                            name="long_lepton_forHNL_{hash}",
                            pt_min=500. * MeV,
                            p_min=3. * GeV,
                            ipchi2_min=25.,
                            pid=None):
    return make_filter_tracks(
        make_particles=make_long,
        name=name,
        pt_min=pt_min,
        p_min=p_min,
        mipchi2dvprimary_min=ipchi2_min,
        pid=pid)


@configurable
def make_ttrack_forHNL(make_ttrack,
                       name='filter_Ttrack_forHLN_{hash}',
                       pt_min=750. * MeV,
                       p_min=5. * GeV,
                       ipchi2_min=500.,
                       pid=None):
    return make_filter_tracks(
        make_particles=make_ttrack,
        name=name,
        pt_min=pt_min,
        p_min=p_min,
        mipchi2dvprimary_min=ipchi2_min,
        pid=pid)


@configurable
def make_HNL_TT(pions,
                leptons,
                name='Generic_HNL_TT',
                descriptor='',
                twobody_yz_intersection=1.5 * meter,
                adocachi2max=300.,
                vtx_zmin=2. * meter,
                vtx_zmax=8. * meter,
                vtxchi2_max=10.,
                mass_min=1.5 * GeV,
                mass_max=6.5 * GeV,
                pt_min=1. * GeV,
                p_min=30. * GeV):
    """
    Make HNL -> pi + lep (with TT combiner)
    """
    combination_cut = F.require_all(
        F.TWOBODY_YZ_INTERSECTION_Z > twobody_yz_intersection,
        F.MAXSDOCACHI2CUT(adocachi2max))

    vertex_cut = F.require_all(
        F.math.in_range(vtx_zmin, F.END_VZ, vtx_zmax), F.CHI2 < vtxchi2_max,
        F.math.in_range(mass_min, F.MASS, mass_max), F.PT > pt_min,
        F.P > p_min)
    return ParticleCombiner(
        Inputs=[leptons, pions],
        DecayDescriptor=descriptor,
        CombinationCut=combination_cut,
        CompositeCut=vertex_cut,
        ParticleCombiner=PVF_with_single_extrapolation(),
        name=name)


@configurable
def make_BToLHNL_TT(long_leptons,
                    hnl_tt,
                    name='Generic_B_LHNL',
                    descriptor='',
                    amass_min=4.0 * GeV,
                    amass_max=6.5 * GeV,
                    mass_min=4.2 * GeV,
                    mass_max=6.3 * GeV,
                    z_vtx_max=1.0 * meter,
                    pt_min=1.5 * GeV):
    """
    Make B-> lep + HNL (with TT combiner)
    """
    combination_cut = F.require_all(
        F.math.in_range(amass_min, F.MASS, amass_max))
    vertex_cut = F.require_all(F.END_VZ < z_vtx_max,
                               F.math.in_range(mass_min, F.MASS, mass_max),
                               F.PT > pt_min)
    return ParticleCombiner(
        Inputs=[long_leptons, hnl_tt],
        DecayDescriptor=descriptor,
        CombinationCut=combination_cut,
        CompositeCut=vertex_cut,
        ParticleCombiner=PVF_with_single_extrapolation(),
        name=name)


### basic particle filters for Dark Higgs searches
## T tracks
@configurable
def filter_ttrack_muons_for_high_mass(pvs,
                                      make_muons=make_mva_ttrack_muons,
                                      ghostprob_max=0.4,
                                      pt_min=750 * MeV,
                                      dll_p_max=0.,
                                      dll_k_max=0.,
                                      dll_mu_min=0.,
                                      track_min=10,
                                      chi2_max=5.,
                                      pid_mu_min=-1.,
                                      pid_k_max=0.,
                                      minipchi2_min=10000.,
                                      delta_dll_kp_max=20.):
    muons = make_muons()
    filter_code_high_mass = F.require_all(
        F.GHOSTPROB < ghostprob_max, F.PT > pt_min, F.RICH_DLL_P < dll_p_max,
        F.NFTHITS @ F.TRACK > track_min, F.CHI2 < chi2_max,
        F.PID_MU > pid_mu_min, F.PID_K < pid_k_max, F.RICH_DLL_K < dll_k_max,
        F.RICH_DLL_MU > dll_mu_min,
        (F.RICH_DLL_K - F.RICH_DLL_P) < delta_dll_kp_max,
        F.MINIPCHI2(pvs) > minipchi2_min)
    return ParticleFilter(muons, F.FILTER(filter_code_high_mass))


@configurable
def filter_ttrack_kaons_for_high_mass(pvs,
                                      make_kaons=make_mva_ttrack_kaons,
                                      ghostprob_max=0.4,
                                      dll_p_max=0.,
                                      dll_k_min=0.,
                                      track_min=10,
                                      pid_k_min=0.,
                                      minipchi2_min=5000.,
                                      delta_dll_muk_max=0.):
    kaons = make_kaons()
    filter_code_high_mass = F.require_all(
        F.GHOSTPROB < ghostprob_max, F.NFTHITS @ F.TRACK > track_min,
        F.PID_K > pid_k_min, F.RICH_DLL_K > dll_k_min,
        F.RICH_DLL_P < dll_p_max,
        (F.RICH_DLL_MU - F.RICH_DLL_K) < delta_dll_muk_max,
        F.MINIPCHI2(pvs) > minipchi2_min)
    return ParticleFilter(kaons, F.FILTER(filter_code_high_mass))


@configurable
def filter_ttrack_pions_for_high_mass(pvs,
                                      make_pions=make_mva_ttrack_pions,
                                      ghostprob_max=0.4,
                                      dll_p_max=0.,
                                      dll_mu_max=0.,
                                      dll_k_max=0.,
                                      track_min=10,
                                      minipchi2_min=5000.,
                                      delta_dll_kpi_max=0.):
    pions = make_pions()
    filter_code_high_mass = F.require_all(
        F.GHOSTPROB < ghostprob_max, F.NFTHITS @ F.TRACK > track_min,
        F.RICH_DLL_K < dll_k_max, F.RICH_DLL_P < dll_p_max,
        F.RICH_DLL_MU < dll_mu_max,
        (F.RICH_DLL_K - F.RICH_DLL_PI) < delta_dll_kpi_max,
        F.MINIPCHI2(pvs) > minipchi2_min)
    return ParticleFilter(pions, F.FILTER(filter_code_high_mass))


### Dimuon/kaon combiners
@configurable
def qee_ttrack_combiner_highmass(filtered_particle1,
                                 filtered_particle2,
                                 pvs,
                                 decay_descriptor,
                                 yz_intersection_z_min=1500.,
                                 maxdoca=50.,
                                 maxdocachi2=300.,
                                 vertex_z_min=2. * meter,
                                 bpv_dira_min=0.9995,
                                 max_chi2=150.,
                                 bpv_ip_max=100.,
                                 bpv_ip_chi2_max=200.,
                                 bpv_vdrho_min=80.,
                                 mass_min=2000.,
                                 mass_max=100000.):
    '''
    used for T track combinations with a mass range > 2 GeV
    '''
    combination_cut_high_mass = F.require_all(
        F.TWOBODY_YZ_INTERSECTION_Z > yz_intersection_z_min,
        F.MAXSDOCACUT(maxdoca), F.MAXSDOCACHI2CUT(maxdocachi2))

    vertex_cut = F.require_all(
        F.END_VZ > vertex_z_min,
        F.BPVDIRA(pvs) > bpv_dira_min, F.CHI2 < max_chi2,
        F.BPVIP(pvs) < bpv_ip_max,
        F.BPVIPCHI2(pvs) < bpv_ip_chi2_max,
        F.BPVVDRHO(pvs) > bpv_vdrho_min,
        F.math.in_range(mass_min, F.MASS, mass_max))  # TODO: upper limit?

    return ParticleCombiner(
        Inputs=[filtered_particle1, filtered_particle2],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cut_high_mass,
        CompositeCut=vertex_cut,
        ParticleCombiner=PVF_with_single_extrapolation(),
        name="qee_ttrack_combiner_highmass_{hash}")


@configurable
def make_XtoTT_muons(pvs):
    filtered_TT_muons = filter_ttrack_muons_for_high_mass(pvs)
    return qee_ttrack_combiner_highmass(filtered_TT_muons, filtered_TT_muons,
                                        pvs, "[KS0 -> mu+ mu-]cc")


@configurable
def make_XtoTT_kaons(pvs):
    filtered_TT_kaons = filter_ttrack_kaons_for_high_mass(pvs)
    return qee_ttrack_combiner_highmass(filtered_TT_kaons, filtered_TT_kaons,
                                        pvs, "[KS0 -> K+ K-]cc")


@configurable
def make_XtoTT_pimu(pvs):
    filtered_TT_pions = filter_ttrack_pions_for_high_mass(pvs)
    filtered_TT_muons = filter_ttrack_muons_for_high_mass(pvs)
    return qee_ttrack_combiner_highmass(filtered_TT_pions, filtered_TT_muons,
                                        pvs, "[KS0 -> pi+ mu-]cc")


@configurable
def qee_BtoLX_TT(long_particles,
                 ttrack_particles,
                 decay_descriptor,
                 vertex_z_max=1000.,
                 mass_min=4200.,
                 mass_max=6300.):
    combination_cut = F.require_all(
        F.math.in_range(mass_min, F.MASS, mass_max))
    vertex_cut = F.require_all(F.END_VZ < vertex_z_max,
                               F.math.in_range(mass_min, F.MASS, mass_max))
    return ParticleCombiner(
        Inputs=[long_particles, ttrack_particles],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cut,
        CompositeCut=vertex_cut,
        ParticleCombiner=PVF_with_single_extrapolation(),
        name="qee_BtoLX_TT_{hash}")
