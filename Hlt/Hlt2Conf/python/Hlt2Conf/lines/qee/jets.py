###############################################################################
# (c) Copyright 2020-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import upfront_reconstruction, make_pvs
from RecoConf.event_filters import require_pvs

from Hlt2Conf.algorithms_thor import ParticleCombiner
from Hlt2Conf.standard_jets import make_jets, jet_tag_maker

all_lines = {}

_hlt1_light_jet_filter = ["Hlt1ConeJet(15|30|50|100)GeVDecision"]


@configurable
def make_dijets(
        tagpair=(None, None),
        min_dijet_mass=0 * GeV,
        prod_pt_min=10 * GeV,
        min_dphi=0.0):
    """Make two-jet combinations
    """
    jet1 = make_jets(pt_min=prod_pt_min, tags=tagpair[0])
    jet2 = make_jets(pt_min=prod_pt_min, tags=tagpair[1])

    if (min_dphi > 0.0):
        delta = F.ADJUST_ANGLE @ (F.CHILD(1, F.PHI) - F.CHILD(2, F.PHI))
        combination_code = F.require_all(
            F.MASS > min_dijet_mass, F.ABS @ delta > min_dphi,
            F.CHILD(1, F.OWNPV) == F.CHILD(2, F.OWNPV))

    else:
        combination_code = F.require_all(
            F.MASS > min_dijet_mass,
            F.CHILD(1, F.OWNPV) == F.CHILD(2, F.OWNPV))

    return ParticleCombiner(
        Inputs=[jet1, jet2],
        DecayDescriptor="CLUSjet -> CELLjet CELLjet",
        CombinationCut=combination_code,
        CompositeCut=F.ALL,
        AllowDiffInputsForSameIDChildren=True,
        ParticleCombiner="ParticleAdder")


@configurable
def make_Trijets(tagpair=(None, None, None), prod_pt_min=10 * GeV):
    """Make three-jet combinations
    """
    jets = make_jets(pt_min=prod_pt_min, tags=tagpair[0])
    tagfilt0 = F.ALL
    tagfilt1 = F.ALL
    tagfilt2 = F.ALL

    combination_code = F.require_all(tagfilt0, tagfilt1, tagfilt2)

    return ParticleCombiner(
        Inputs=[jets, jets, jets],
        DecayDescriptor="CLUSjet -> CELLjet CELLjet CELLjet",
        CombinationCut=combination_code,
        CompositeCut=F.ALL,
        ParticleCombiner="ParticleAdder")


##### Functions for sprucing lines #####
@configurable
def make_SVTagDijets_cand():
    SV = jet_tag_maker("SV")
    line_alg = make_dijets(
        tagpair=(SV, SV), prod_pt_min=30 * GeV, min_dphi=1.5)
    return line_alg


@configurable
def make_Trijets_cand():
    line_alg = make_Trijets(tagpair=(None, None, None), prod_pt_min=30 * GeV)
    return line_alg


@configurable
def make_TrijetsTwoSVTag_cand():
    SV = jet_tag_maker("SV")
    line_alg = make_Trijets(tagpair=(SV, SV, None), prod_pt_min=30 * GeV)
    return line_alg


############ SV Tag ####################
@register_line_builder(all_lines)
@configurable
def diSVTagJet10GeV_line(name='Hlt2QEE_DiSVTagJet10GeVFull',
                         prescale=0.0025,
                         persistreco=True):
    SV = jet_tag_maker("SV")
    jets = make_dijets(tagpair=(SV, SV), prod_pt_min=10 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diSVTagJet15GeV_line(name='Hlt2QEE_DiSVTagJet15GeVFull',
                         prescale=0.01,
                         persistreco=True):
    SV = jet_tag_maker("SV")
    jets = make_dijets(tagpair=(SV, SV), prod_pt_min=15 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diSVTagJet20GeV_line(name='Hlt2QEE_DiSVTagJet20GeVFull',
                         prescale=0.05,
                         persistreco=True):
    SV = jet_tag_maker("SV")
    jets = make_dijets(tagpair=(SV, SV), prod_pt_min=20 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diSVTagJet25GeV_line(name='Hlt2QEE_DiSVTagJet25GeVFull',
                         prescale=0.1,
                         persistreco=True):
    SV = jet_tag_maker("SV")
    jets = make_dijets(tagpair=(SV, SV), prod_pt_min=25 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diSVTagJet30GeV_line(name='Hlt2QEE_DiSVTagJet30GeVFull',
                         prescale=0.25,
                         persistreco=True):
    SV = jet_tag_maker("SV")
    jets = make_dijets(tagpair=(SV, SV), prod_pt_min=30 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diSVTagJet35GeV_line(name='Hlt2QEE_DiSVTagJet35GeVFull',
                         prescale=1.0,
                         persistreco=True):
    SV = jet_tag_maker("SV")
    jets = make_dijets(tagpair=(SV, SV), prod_pt_min=35 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


############ Asymmetric dijet lines ################
@register_line_builder(all_lines)
@configurable
def DiJetIncSVTag_line(name='Hlt2QEE_DiJetIncSVTag_pT25M40Full',
                       prescale=1,
                       persistreco=True):
    SV = jet_tag_maker("SV")
    jets = make_dijets(
        tagpair=(None, SV),
        min_dijet_mass=40 * GeV,
        prod_pt_min=25 * GeV,
        min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


############ Topo Tag ####################


@register_line_builder(all_lines)
@configurable
def diTopoTagJet10GeV_line(name='Hlt2QEE_DiTopoTagJet10GeVFull',
                           prescale=0.001,
                           persistreco=True):
    TOPO = jet_tag_maker("TOPO")
    jets = make_dijets(
        tagpair=(TOPO, TOPO), prod_pt_min=10 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diTopoTagJet15GeV_line(name='Hlt2QEE_DiTopoTagJet15GeVFull',
                           prescale=0.001,
                           persistreco=True):
    TOPO = jet_tag_maker("TOPO")
    jets = make_dijets(
        tagpair=(TOPO, TOPO), prod_pt_min=15 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diTopoTagJet20GeV_line(name='Hlt2QEE_DiTopoTagJet20GeVFull',
                           prescale=0.005,
                           persistreco=True):
    TOPO = jet_tag_maker("TOPO")
    jets = make_dijets(
        tagpair=(TOPO, TOPO), prod_pt_min=20 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diTopoTagJet25GeV_line(name='Hlt2QEE_DiTopoTagJet25GeVFull',
                           prescale=0.05,
                           persistreco=True):
    TOPO = jet_tag_maker("TOPO")
    jets = make_dijets(
        tagpair=(TOPO, TOPO), prod_pt_min=25 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diTopoTagJet30GeV_line(name='Hlt2QEE_DiTopoTagJet30GeVFull',
                           prescale=0.05,
                           persistreco=True):
    TOPO = jet_tag_maker("TOPO")
    jets = make_dijets(
        tagpair=(TOPO, TOPO), prod_pt_min=30 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def diTopoTagJet35GeV_line(name='Hlt2QEE_DiTopoTagJet35GeVFull',
                           prescale=0.1,
                           persistreco=True):
    TOPO = jet_tag_maker("TOPO")
    jets = make_dijets(
        tagpair=(TOPO, TOPO), prod_pt_min=35 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


############# Inclusive jets ####################
@register_line_builder(all_lines)
@configurable
def IncJet15GeV_line(name='Hlt2QEE_IncJet15GeVFull',
                     prescale=0.05,
                     persistreco=True):
    jets = make_jets(pt_min=15 * GeV)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        hlt1_filter_code=_hlt1_light_jet_filter,
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def IncJet25GeV_line(name='Hlt2QEE_IncJet25GeVFull',
                     prescale=0.1,
                     persistreco=True):
    jets = make_jets(pt_min=25 * GeV)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        hlt1_filter_code=_hlt1_light_jet_filter,
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def IncJet35GeV_line(name='Hlt2QEE_IncJet35GeVFull',
                     prescale=0.5,
                     persistreco=True):
    jets = make_jets(pt_min=35 * GeV)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        hlt1_filter_code=_hlt1_light_jet_filter,
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def IncJet45GeV_line(name='Hlt2QEE_IncJet45GeVFull',
                     prescale=1.0,
                     persistreco=True):
    jets = make_jets(pt_min=45 * GeV)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        hlt1_filter_code=_hlt1_light_jet_filter,
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def IncDiJet15GeV_line(name='Hlt2QEE_IncDiJet15GeVFull',
                       prescale=0.1,
                       persistreco=True):
    jets = make_dijets(prod_pt_min=15 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        hlt1_filter_code=_hlt1_light_jet_filter,
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def IncDiJet20GeV_line(name='Hlt2QEE_IncDiJet20GeVFull',
                       prescale=0.25,
                       persistreco=True):
    jets = make_dijets(prod_pt_min=20 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        hlt1_filter_code=_hlt1_light_jet_filter,
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def IncDiJet25GeV_line(name='Hlt2QEE_IncDiJet25GeVFull',
                       prescale=0.5,
                       persistreco=True):
    jets = make_dijets(prod_pt_min=25 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        hlt1_filter_code=_hlt1_light_jet_filter,
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def IncDiJet30GeV_line(name='Hlt2QEE_IncDiJet30GeVFull',
                       prescale=0.75,
                       persistreco=True):
    jets = make_dijets(prod_pt_min=30 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        hlt1_filter_code=_hlt1_light_jet_filter,
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def IncDiJet35GeV_line(name='Hlt2QEE_IncDiJet35GeVFull',
                       prescale=1.0,
                       persistreco=True):
    jets = make_dijets(prod_pt_min=35 * GeV, min_dphi=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        hlt1_filter_code=_hlt1_light_jet_filter,
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco)
