###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 lines for T track LLP searches.
"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from PyConf import configurable
from RecoConf.event_filters import require_pvs
from Hlt2Conf.standard_particles import make_long_muons, make_long_electrons_with_brem, make_ttrack_pions, make_ttrack_muons
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_detached_kaons, make_rd_detached_kstar0s, make_rd_detached_muons
from Hlt2Conf.lines.qee.qee_builders import make_long_lepton_forHNL, make_HNL_TT, make_BToLHNL_TT, make_ttrack_forHNL
from Hlt2Conf.lines.qee.qee_builders import qee_BtoLX_TT, make_XtoTT_kaons, make_XtoTT_muons, make_XtoTT_pimu

import Functors as F

all_lines = {}

########################
### Dark Higgs modes ###
########################


@register_line_builder(all_lines)
@configurable
def qee_BtoKH_KK_exclTT(name="Hlt2QEE_BtoKH_KK_exclTT", prescale=1.):
    pvs = make_pvs()
    long_kaons = make_rd_detached_kaons()
    dikaon = make_XtoTT_kaons(pvs)
    b_candidate = qee_BtoLX_TT(
        long_particles=long_kaons,
        ttrack_particles=dikaon,
        decay_descriptor="[B+ -> K+ KS0]cc",
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), b_candidate],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def qee_BtoKstar0H_KK_exclTT(name="Hlt2QEE_BtoKstar0H_KK_exclTT", prescale=1.):
    pvs = make_pvs()
    long_kstars = make_rd_detached_kstar0s()
    dikaon = make_XtoTT_kaons(pvs)
    b_candidate = qee_BtoLX_TT(
        long_particles=long_kstars,
        ttrack_particles=dikaon,
        decay_descriptor="[B0 -> K*(892)0 KS0]cc",
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), b_candidate],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def qee_BtoKH_MuMu_exclTT(name="Hlt2QEE_BtoKH_MuMu_exclTT", prescale=1.):
    pvs = make_pvs()
    long_kaons = make_rd_detached_kaons()
    dimuon = make_XtoTT_muons(pvs)
    b_candidate = qee_BtoLX_TT(
        long_particles=long_kaons,
        ttrack_particles=dimuon,
        decay_descriptor="[B+ -> K+ KS0]cc",
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), b_candidate],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def qee_BtoKstar0H_MuMu_exclTT(name="Hlt2QEE_BtoKstar0H_MuMu_exclTT",
                               prescale=1.):
    pvs = make_pvs()
    long_kstars = make_rd_detached_kstar0s()
    dimuon = make_XtoTT_muons(pvs)
    b_candidate = qee_BtoLX_TT(
        long_particles=long_kstars,
        ttrack_particles=dimuon,
        decay_descriptor="[B0 -> K*(892)0 KS0]cc",
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), b_candidate],
        prescale=prescale)


#################
### HNL modes ###
#################


### HNL Exclusive Lines
@register_line_builder(all_lines)
@configurable
def qee_BtoMuN_PiMu_exclTT(name="Hlt2QEE_BtoMuN_PiMu_exclTT", prescale=1.):
    pvs = make_pvs()
    long_muons = make_rd_detached_muons()
    hnl = make_XtoTT_pimu(pvs)
    b_candidate = qee_BtoLX_TT(
        long_particles=long_muons,
        ttrack_particles=hnl,
        decay_descriptor="[B+ -> mu+ KS0]cc",
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), b_candidate],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def qee_BctoMuN_PiMu_exclTT(name="Hlt2QEE_BctoMuN_PiMu_exclTT", prescale=1.):
    pvs = make_pvs()
    long_muons = make_rd_detached_muons()
    hnl = make_XtoTT_pimu(pvs)

    b_candidate = qee_BtoLX_TT(
        long_particles=long_muons,
        ttrack_particles=hnl,
        decay_descriptor="[B_c+ -> mu+ KS0]cc",
        mass_min=5200.,
        mass_max=7300.)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), b_candidate],
        prescale=prescale)


## HNL Inclusive Lines
@register_line_builder(all_lines)
@configurable
def BtoMuN_MuPi_InclTT(name="Hlt2QEE_BtoMuN_MuPi_InclTT", prescale=0.1):
    pvs = make_pvs()

    long_muons = make_long_lepton_forHNL(
        make_long=make_long_muons,
        name='filter_long_muons_forHNL_{hash}',
        pid=F.require_all(F.ISMUON, F.PID_MU > 2., (F.PID_MU - F.PID_P) > 2.,
                          (F.PID_MU - F.PID_K) > 2.,
                          (F.PID_MU - F.PID_E) > 2.))
    filtered_Tpions = make_ttrack_forHNL(
        make_ttrack=make_ttrack_pions,
        name='filter_Ttrack_pions_forHLN_{hash}',
        pid=F.require_all(F.PID_P < 2., F.PID_K < 2., F.PID_E < 2.))
    filtered_Tmuons = make_ttrack_forHNL(
        make_ttrack=make_ttrack_muons,
        name='filter_Ttrack_muons_forHLN_{hash}',
        # F.ISMUON not inserted because the muon reconstruction does not yet include T tracks
        pid=F.require_all((F.PID_MU - F.PID_P) > 2., (F.PID_MU - F.PID_K) > 2.,
                          (F.PID_MU - F.PID_E) > 2.))
    hnl = make_HNL_TT(
        pions=filtered_Tpions,
        leptons=filtered_Tmuons,
        name='HNL_TT_combiner_MuPi_{hash}',
        descriptor='[KS0 -> mu- pi+]cc')
    bp = make_BToLHNL_TT(
        long_leptons=long_muons,
        hnl_tt=hnl,
        name='B_combiner_MuN_MuPiTT_{hash}',
        descriptor='[B+ -> mu+ KS0]cc')
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), long_muons, bp],
        prescale=prescale)


@register_line_builder(all_lines)
@configurable
def BtoEN_MuPi_InclTT(name="Hlt2QEE_BtoEN_MuPi_InclTT", prescale=0.1):
    pvs = make_pvs()
    long_electrons = make_long_lepton_forHNL(
        make_long=make_long_electrons_with_brem,
        name='filter_long_electrons_forHNL_{hash}',
        pid=F.require_all(F.PID_E > 2., (F.PID_E - F.PID_P) > 2.,
                          (F.PID_E - F.PID_K) > 2., (F.PID_E - F.PID_MU) > 2.,
                          ~F.ISMUON))
    filtered_Tpions = make_ttrack_forHNL(
        make_ttrack=make_ttrack_pions,
        name='filter_Ttrack_pions_forHLN_{hash}',
        pid=F.require_all(F.PID_P < 2., F.PID_K < 2., F.PID_E < 2.))
    filtered_Tmuons = make_ttrack_forHNL(
        make_ttrack=make_ttrack_muons,
        name='filter_Ttrack_muons_forHLN_{hash}',
        # F.ISMUON not inserted because the muon reconstruction does not yet include T tracks
        pid=F.require_all((F.PID_MU - F.PID_P) > 2., (F.PID_MU - F.PID_K) > 2.,
                          (F.PID_MU - F.PID_E) > 2.))
    hnl = make_HNL_TT(
        pions=filtered_Tpions,
        leptons=filtered_Tmuons,
        name='HNL_TT_combiner_MuPi_{hash}',
        descriptor='[KS0 -> mu- pi+]cc')
    bp = make_BToLHNL_TT(
        long_leptons=long_electrons,
        hnl_tt=hnl,
        name='B_combiner_EN_MuPiTT_{hash}',
        descriptor='[B+ -> e+ KS0]cc')
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), long_electrons, bp],
        prescale=prescale)
