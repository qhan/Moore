###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Options for Sprucing 2024 data - this is a living script
## which is compared to the output of hlt2_pp_2024 and should be updated by line authors when prompted to do so

from Moore import Options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from Hlt2Conf.lines import sprucing_lines
from Moore.lines import PassLine, SpruceLine

from PyConf.reading import get_particles
from Moore.streams import DETECTORS, Stream, Streams
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines

from Hlt2Conf.lines.b_to_charmonia import sprucing_lines as spruce_b2cc_lines
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines as spruce_b2oc_lines
from Hlt2Conf.lines.bandq import sprucing_lines as spruce_bandq_lines
from Hlt2Conf.lines.bnoc import sprucing_lines as spruce_bnoc_lines
from Hlt2Conf.lines.rd import sprucing_lines as spruce_rd_lines
from Hlt2Conf.lines.qee import sprucing_lines as spruce_qee_lines
from Hlt2Conf.lines.semileptonic import sprucing_lines as spruce_sl_lines
from Hlt2Conf.lines.ift import sprucing_lines as spruce_ift_lines

from Hlt2Conf.lines.b_to_open_charm import all_calib_lines as b_to_open_charm_calib_lines
from Hlt2Conf.lines.bandq import hlt2_full_lines as bandq_full_lines
from Hlt2Conf.lines.bnoc import full_lines as bnoc_full_lines
from Hlt2Conf.lines.ift import ift_full_lines
from Hlt2Conf.lines.inclusive_detached_dilepton import all_lines as inclusive_detached_dilepton_lines
from Hlt2Conf.lines.qee import hlt2_full_lines as qee_full_lines
from Hlt2Conf.lines.rd import full_lines as rd_full_lines
from Hlt2Conf.lines.semileptonic import all_lines as semileptonic_lines
from Hlt2Conf.lines.topological_b import all_lines as topological_b_lines
from Hlt2Conf.lines.charmonium_to_dimuon import full_lines as charmonium_to_dimuon_prompt_full_lines
from Hlt2Conf.lines.charmonium_to_dimuon_detached import all_lines as charmonium_to_dimuon_detached_lines

public_tools = [stateProvider_with_simplified_geom()]

###################### EXCLUSIVE SPRUCING ON FULL STREAM ####################

fulllinedict = {
    "b2oc": spruce_b2oc_lines,
    "bandq": spruce_bandq_lines,
    "b2cc": spruce_b2cc_lines,
    "rd": spruce_rd_lines,
    "sl": spruce_sl_lines,
    "qee": spruce_qee_lines,
    "bnoc": spruce_bnoc_lines,
    "ift": spruce_ift_lines,
}

## Following configures FULL HLT2 lines that need TISTOS info in exclusive Sprucing
full_modules_for_TISTOS = [
    topological_b_lines,
    inclusive_detached_dilepton_lines,
    charmonium_to_dimuon_detached_lines,
    charmonium_to_dimuon_prompt_full_lines,
    b_to_open_charm_calib_lines,
    rd_full_lines,
    qee_full_lines,
    semileptonic_lines,
    bnoc_full_lines,
    ift_full_lines,
    bandq_full_lines,
]


def lines_for_TISTOS(full_modules_for_TISTOS):
    """Configure FULL lines for TISTOS persistency in exclusive Sprucing
    Note there will hopefully be nicer ways to configure this in the future"""

    full_lines_for_TISTOS = [
        linename for module in full_modules_for_TISTOS
        for linename in module.keys()
    ]

    return full_lines_for_TISTOS


def lines_running(linedict):
    """ Return the full list of lines to be run"""

    return [
        item for sublist in [list(linedict[wg].keys()) for wg in linedict]
        for item in sublist
    ]


def lines_not_running(all_lines, lines_to_run):
    """Return the list of lines that are declared in the framework but that are not set to run"""
    return [item for item in list(all_lines) if item not in lines_to_run]


def make_excl_spruce_prod_streams():
    spruce_b2oc_lines.pop('SpruceB2OC_BdToDsmK_DsmToHHH_FEST')
    lines_to_run = lines_running(fulllinedict)
    missing_lines = lines_not_running(sprucing_lines, lines_to_run)

    print(f"Running {len(lines_to_run)} lines")
    for wg in fulllinedict.keys():
        lines = list(fulllinedict[wg].keys())
        print(f"Stream {wg} will contain the lines: {lines} \n")

    print(
        f"The following lines exist but are not appended to any stream : {missing_lines} \n end of missing lines."
    )

    streams = [
        Stream(
            wg,
            lines=[builder() for builder in fulllinedict[wg].values()],
            detectors=[]) for wg in fulllinedict
    ]

    return Streams(streams=streams)


def excl_spruce_production(options: Options):
    with list_of_full_stream_lines.bind(
            lines=lines_for_TISTOS(full_modules_for_TISTOS)):
        config = run_moore(
            options, make_excl_spruce_prod_streams, public_tools=[])
        return config


###################### PASSTHROUGH SPRUCING ON TURBO STREAM ####################

turbolinedict = {
    "b2oc": ["Hlt2B2OC.*Decision"],
    "bandq": [
        '^(?!.*Full)Hlt2BandQ.*Decision',
        '^(?!.*Full)Hlt2_JpsiToMuMu.*Decision',
        '^(?!.*Full)Hlt2_Psi2SToMuMu.*Decision'
    ],
    "b2cc": ["Hlt2B2CC.*Decision"],
    "charm": ["Hlt2Charm.*Decision", "Hlt2HadInt.*Decision"],
    "qee": ["^(?!.*Full)Hlt2QEE.*Decision"],
    "rd": ["^(?!.*Gamma.*Incl)Hlt2RD.*Decision"],
    "bnoc": ["^(?!.*Full)Hlt2BnoC.*Decision"],
    "ift": ["Hlt2IFTTurbo.*Decision"],
    "trackeff": ["Hlt2TurboVelo2Long.*Decision"]
}


def make_turbo_spruce_prod_streams():
    streams = [
        Stream(
            wg,
            lines=[
                PassLine(
                    name="Pass" + wg,
                    hlt2_filter_code=turbolinedict[wg],
                )
            ],
            detectors=DETECTORS) for wg in turbolinedict
    ]

    for wg in turbolinedict.keys():
        print(
            f"Stream {wg} will contain the lines matching the regex : {turbolinedict[wg]} \n"
        )

    return Streams(streams=streams)


def turbo_spruce_production(options: Options):

    config = run_moore(
        options, make_turbo_spruce_prod_streams, public_tools=[])
    return config


###################### MDST SPRUCING ON TURCAL STREAM ####################

nominal_lines = [
    "Hlt2PID_BToJpsiK_JpsiToMuMumTagged_Detached",
    "Hlt2PID_BToJpsiK_JpsiToMuMupTagged_Detached",
    "Hlt2PID_BToJpsiK_JpsiToPpPmTagged",
    "Hlt2PID_BToJpsiK_JpsiToPmPpTagged",
    "Hlt2PID_DsToPhiPi_PhiToMuMupTagged_Detached",
    "Hlt2PID_DsToPhiPi_PhiToMuMumTagged_Detached",
    "Hlt2PID_DstToD0Pi_D0ToKPiPiPi",
    "Hlt2PID_KsToPiPi_LL",
    "Hlt2PID_LbToLcMuNu_LcToPKPi",
    "Hlt2PID_LbToLcPi_LcToPKPi",
    "Hlt2PID_LcToPKPi",
    "Hlt2PID_OmegaToL0K_L0ToPPi_LLL",
    "Hlt2PID_PhiToKK_Unbiased_Detached",
    "Hlt2PID_PhiToKmKpTagged_Detached",
    "Hlt2PID_KsToPiPi_DD",
    "Hlt2PID_L0ToPPi_DD_LowPT",
    "Hlt2PID_L0ToPPi_DD_MidPT",
    "Hlt2PID_OmegaToL0K_L0ToPPi_DDD",
    "Hlt2PID_L0ToPPi_DD_HighPT",
    "Hlt2PID_PhiToKpKmTagged_Detached",
    "Hlt2PID_L0ToPPi_DD_VeryHighPT",
]

persist_reco_lines = [
    "Hlt2PID_BdToKstG",
    "Hlt2PID_Bs2PhiG",
    "Hlt2PID_EtaMuMuG",
    "Hlt2PID_Dsst2DsG",
    "Hlt2PID_D2EtapPi",
    "Hlt2PID_DstToD0Pi_D0ToKPiPi0Resolved",
    "Hlt2PID_DstToD0Pi_D0ToKPiPi0Merged",
]

rawbank_lines = [
    "Hlt2PID_JpsiToMuMumTagged_Detached",
    "Hlt2PID_JpsiToMuMupTagged_Detached",
    "Hlt2PID_BToJpsiK_JpsiToEmbremEpbremTagged_noPIDe",
    "Hlt2PID_BToJpsiK_JpsiToEpbremEmbremTagged_noPIDe",
    "Hlt2PID_BToJpsiK_JpsiToEpbremEmbremTagged",
    "Hlt2PID_BToJpsiK_JpsiToEmbremEpbremTagged",
    "Hlt2PID_DstToD0Pi_D0ToKPi",
    "Hlt2PID_L0ToPPi_LL_LowPT",
    "Hlt2PID_L0ToPPi_LL_MidPT",
    "Hlt2PID_L0ToPPi_LL_HighPT",
    "Hlt2PID_L0ToPPi_LL_VeryHighPT",
    "Hlt2TrackEff_DiMuon_SeedMuon_mup_Tag",
    "Hlt2TrackEff_DiMuon_SeedMuon_mum_Tag",
    "Hlt2TrackEff_DiMuon_SeedMuon_mum_Match",
    "Hlt2TrackEff_DiMuon_SeedMuon_mup_Match",
    "Hlt2TrackEff_DiMuon_VeloMuon_mup_Tag",
    "Hlt2TrackEff_DiMuon_VeloMuon_mum_Tag",
    "Hlt2TrackEff_DiMuon_VeloMuon_mum_Match",
    "Hlt2TrackEff_DiMuon_VeloMuon_mup_Match",
]

persist_reco_rawbank_lines = [
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mum_Match",
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mup_Match",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mum_Match",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mup_Match",
    "Hlt2TrackEff_Velo2Long_B2JpsiK_MuonProbe_VELO",
    "Hlt2TrackEff_Velo2Long_B2JpsiK_ElectronProbe_VELO",
    "Hlt2TurCalVelo2Long_KshortVSoft",
    "Hlt2TurCalVelo2Long_KshortHard",
    "Hlt2TurCalVelo2Long_Kshort",
    "Hlt2TurCalVelo2Long_KshortSoft",
    "Hlt2TurCalVelo2Long_KshortVHard",
]


def make_turcal_spruceline(hlt2_linename, persist_reco=False, prescale=1.):
    filter = f"{hlt2_linename}Decision"
    location = f"/Event/HLT2/{hlt2_linename}/Particles"
    spruce_linename = hlt2_linename.replace("Hlt2", "SpruceTurCal")
    print(f"{filter} {location} {spruce_linename}")
    hlt2_particles = get_particles(location)
    turcal_spruce_line = SpruceLine(
        name=spruce_linename,
        hlt2_filter_code=filter,
        algs=[hlt2_particles],
        persistreco=persist_reco,
        prescale=prescale)
    return turcal_spruce_line


def make_turcal_spruce_prod_streams():
    streams = [
        Stream(
            "Turcal_mDST",
            lines=[make_turcal_spruceline(line) for line in nominal_lines],
            detectors=[]),
        Stream(
            "Turcal_persistreco",
            lines=[
                make_turcal_spruceline(line, persist_reco=True)
                for line in persist_reco_lines
            ],
            detectors=[]),
        Stream(
            "Turcal_rawbanks",
            lines=[make_turcal_spruceline(line) for line in rawbank_lines],
            detectors=DETECTORS),
        Stream(
            "Turcal_persistrecorawbanks",
            lines=[
                make_turcal_spruceline(line, persist_reco=True)
                for line in persist_reco_rawbank_lines
            ],
            detectors=DETECTORS)
    ]
    print(streams)
    return Streams(streams=streams)


def turcal_spruce_production(options: Options):
    config = run_moore(
        options, make_turcal_spruce_prod_streams, public_tools=[])
    return config
