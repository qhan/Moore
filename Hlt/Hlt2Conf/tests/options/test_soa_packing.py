###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test the persistency of SOA objects
- Tracks
"""

from PyConf.Algorithms import HltPackedBufferWriter, HltPackedBufferDecoder, SOATrackPacker, SOAPVPacker
from PyConf.filecontent_metadata import register_encoding_dictionary
from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT, convert_tracks_to_v3_from_v1, get_persistable_tracks_per_type
from RecoConf.legacy_rec_hlt1_tracking import make_reco_pvs
from PyConf.packing import persistable_location
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.application import configure_input, configure, default_raw_banks
from Moore import options
from DDDB.CheckDD4Hep import UseDD4Hep

# first ocnfigure input
options.data_type = "Upgrade"
options.simulation = True
options.input_raw_format = 0.5
options.set_input_and_conds_from_testfiledb(
    "upgrade_Sept2022_minbias_0fb_md_xdigi")
if UseDD4Hep:
    options.geometry_version = 'run3/before-rich1-geom-update-26052022'
options.evt_max = 5
config = configure_input(options)

# now create control flow
hlt2_tracks = make_hlt2_tracks_without_UT(
    light_reco=True, fast_reco=False, use_pr_kf=True)
# track conversion to v3 for muon / calo
persisted_tracks = get_persistable_tracks_per_type(hlt2_tracks)
tracks_v3, tracks_rels = convert_tracks_to_v3_from_v1(persisted_tracks)

# PVs
pvs = make_reco_pvs(hlt2_tracks['Velo'], persistable_location('PVs'))

locations = []
locations.append(tracks_v3["Long"].location)
locations.append(pvs["v3"].location)

encoding_key = int(
    register_encoding_dictionary("PackedObjectLocations", sorted(locations)),
    16)

track_packer = SOATrackPacker(
    InputName=[tracks_v3["Long"]],
    EnableCheck=True,
    OutputLevel=2,
    EncodingKey=encoding_key)

pv_packer = SOAPVPacker(
    InputName=[pvs["v3"]],
    EnableCheck=True,
    OutputLevel=2,
    EncodingKey=encoding_key)

# Writer
bank_writer = HltPackedBufferWriter(
    PackedContainers=[
        track_packer.OutputName.location, pv_packer.OutputName.location
    ],
    OutputLevel=2,
    SourceID="Hlt2")

# Decoder
bank_decoder = HltPackedBufferDecoder(
    RawBanks=bank_writer.OutputView,
    OutputLevel=2,
    SourceID="Hlt2",
    DecReports=default_raw_banks("HltDecReports"))

# finally run the test
cf_node = CompositeNode(
    'Seq',
    combine_logic=NodeLogic.LAZY_AND,
    children=[track_packer, pv_packer, bank_writer, bank_decoder],
    force_order=True)
config.update(configure(options, cf_node))
