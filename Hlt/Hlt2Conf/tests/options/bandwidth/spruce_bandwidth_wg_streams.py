###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Test option for the Sprucing bandwidth test in LHCbPR for sprucing of the Full stream

The streaming configuration in this test is taking from the current sprucing production options

To launch it in Moore, run with
    ./run gaudirun.py spruce_bandwidth_input.py spruce_bandwidth_wg_streams.py
'''

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.sprucing_settings.Sprucing_production_PP_24c1 import make_excl_spruce_prod_streams

import logging
import json
from PRConfig.bandwidth_helpers import FileNameHelper
log = logging.getLogger()

options.input_process = 'Hlt2'

STREAM_CONFIG = "wg"
fname_helper = FileNameHelper(process="spruce")
fname_helper.make_tmp_dirs()
options.output_file = fname_helper.mdfdst_fname_for_Moore(
    stream_config=STREAM_CONFIG, ext=".dst")
options.output_type = 'ROOT'
options.output_manifest_file = fname_helper.tck(stream_config=STREAM_CONFIG)


def make_module_streams():

    real_streams = make_excl_spruce_prod_streams()

    # Write out stream configuration to JSON file for use later in the test
    with open(
            fname_helper.stream_config_json_path(stream_config=STREAM_CONFIG),
            'w') as f:
        json.dump({
            stream.name: [line.name for line in stream.lines]
            for stream in real_streams.streams
        }, f)

    return real_streams


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=True, spruce=True):
    config = run_moore(options, make_module_streams, public_tools)
