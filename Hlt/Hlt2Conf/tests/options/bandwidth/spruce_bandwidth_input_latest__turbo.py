###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Configure input files for the spruce_latest bandwidth tests for the Turbo stream

This is needed for the qmtest in Moore.
For the test in LHCbPR, the input files are configured by
$PRCONFIGROOT/python/MooreTests/run_bandwidth_test_jobs.py

'''
from Moore import options
from PRConfig.bandwidth_helpers import FileNameHelper

stream = 'turbo'
stream_config = 'production'

fname_helper = FileNameHelper(process="hlt2")
options.input_files = [
    fname_helper.mdfdst_fname_for_Moore(stream_config=stream_config).format(
        stream=stream)
]
options.input_manifest_file = fname_helper.tck(stream_config=stream_config)
