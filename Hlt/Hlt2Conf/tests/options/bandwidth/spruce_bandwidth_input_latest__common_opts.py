###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Common options for sprucing the 3 HLT2 output streams
'''
from Moore import options
# HLT2 has been run on the below TestFileDB sample, so we can take its conds.
options.set_conds_from_testfiledb('expected_2024_min_bias_hlt1_filtered')
options.evt_max = 100
options.n_threads = 1
options.input_raw_format = 0.5
options.input_type = 'MDF'
