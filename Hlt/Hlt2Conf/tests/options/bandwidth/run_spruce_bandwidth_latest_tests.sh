#!/bin/bash
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euxo pipefail

TEST_PATH_PREFIX='$HLT2CONFROOT/tests/options/bandwidth/'
for STREAM_CONFIG in wg wgpass turcal; do
    if [ $STREAM_CONFIG = "wg" ]; then
        HLT2_OUTPUT_STREAM="full"
    elif [ $STREAM_CONFIG = "wgpass" ]; then
        HLT2_OUTPUT_STREAM="turbo"
    elif [ $STREAM_CONFIG = "turcal" ]; then
        HLT2_OUTPUT_STREAM="turcal"
    fi

    gaudirun.py \
        "${TEST_PATH_PREFIX}spruce_bandwidth_input_latest__common_opts.py" \
        "${TEST_PATH_PREFIX}spruce_bandwidth_input_latest__${HLT2_OUTPUT_STREAM}.py" \
        "${TEST_PATH_PREFIX}spruce_bandwidth_${STREAM_CONFIG}_streams.py"
done