###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Demonstrate filtering on HLT1 decisions written by Allen.
"""
from Moore import options, run_moore
from Moore.lines import Hlt2Line

from RecoConf.global_tools import stateProvider_with_simplified_geom
from PyConf.application import configure_input, create_or_reuse_rootIOAlg


def filter_hlt1_line(name="Hlt2FilterHlt1", prescale=1):
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=[],
        hlt1_filter_code="Hlt1TrackMVA.*Decision")


def make_lines():
    return [filter_hlt1_line()]


# precreate RootIOAlg as we only have lines using the unpacker machinery which relies on it to exist
config = configure_input(options)
create_or_reuse_rootIOAlg(options)

public_tools = [stateProvider_with_simplified_geom()]
run_moore(options, make_lines, public_tools)
