###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
    Verifies the following, per WG in the turbo regex dictionary from Sprucing_production_{latest}:
        - All full-stream lines (if applicable) fail the turbo regex
        - All turbo-stream lines succeed the turbo regex.
        - All Hlt2 lines exist in either full-stream or turbo-stream.
    NB: This Skips: [SL, DetachedDiLepton, Topo].
        - No turbo lines thus no regex thus no test.
    NB: It's very important this is kept up to date with the latest Sprucing production.
        Currently uses: Sprucing_production_PP_24c1
"""
import re
from Hlt2Conf.lines.bandq import (hlt2_full_lines as bandq_full_lines,
                                  hlt2_turbo_lines as bandq_turbo_lines,
                                  all_lines as bandq_all_lines)
from Hlt2Conf.lines.bnoc import (hlt2_lines as bnoc_turbo_lines, full_lines as
                                 bnoc_full_lines, all_lines as bnoc_all_lines)
from Hlt2Conf.lines.b_to_charmonia import all_lines as b2cc_turbo_lines
from Hlt2Conf.lines.b_to_open_charm import (all_lines as b2oc_turbo_lines,
                                            all_calib_lines as b2oc_full_lines,
                                            all_lines as b2oc_all_lines)
from Hlt2Conf.lines.charm import all_lines as charm_turbo_lines
from Hlt2Conf.lines.charmonium_to_dimuon_detached import (
    all_lines as cc_to_dimu_det_full_lines, all_lines as
    cc_to_dimu_det_all_lines)
from Hlt2Conf.lines.charmonium_to_dimuon import (
    full_lines as cc_to_dimu_prompt_full_lines, turbo_lines as
    cc_to_dimu_prompt_turbo_lines, all_lines as cc_to_dimu_prompt_all_lines)
from Hlt2Conf.lines.ift import ift_full_lines, ift_turbo_lines, all_lines as ift_all_lines
from Hlt2Conf.lines.rd import (full_lines as rd_full_lines, turbo_lines as
                               rd_turbo_lines, all_lines as rd_all_lines)
from Hlt2Conf.lines.qee import (hlt2_full_lines as qee_full_lines,
                                hlt2_turbo_lines as qee_turbo_lines, all_lines
                                as qee_all_lines)

# This needs to be kept up to date for the test to make sense.
from Hlt2Conf.sprucing_settings.Sprucing_production_PP_24c1 import turbolinedict as turbo_streaming

linedicts = {  #WG : (turbo_linedict, full_linedict, all_lines_linedict)
    "b2oc": (b2oc_turbo_lines, b2oc_full_lines, b2oc_all_lines),
    "bandq": ({
        **bandq_turbo_lines,
        **cc_to_dimu_prompt_turbo_lines
    }, {
        **bandq_full_lines,
        **cc_to_dimu_det_full_lines,
        **cc_to_dimu_prompt_full_lines
    }, {
        **bandq_all_lines,
        **cc_to_dimu_prompt_all_lines,
        **cc_to_dimu_det_all_lines
    }),
    "b2cc": (b2cc_turbo_lines, {}, {}),
    "bnoc": (bnoc_turbo_lines, bnoc_full_lines, bnoc_all_lines),
    "charm": (charm_turbo_lines, {}, {}),
    "qee": (qee_turbo_lines, qee_full_lines, qee_all_lines),
    "rd": (rd_turbo_lines, rd_full_lines, rd_all_lines),
    "ift": (ift_turbo_lines, ift_full_lines, ift_all_lines)
    # Skip [SL, DetachedDiLepton, Topo]. no turbo lines thus no regex thus no test.
}


def _extract_lines(linedicts):
    extract = lambda linedict: [linename for linename in linedict.keys()]
    return (extract(linedict) for linedict in linedicts)


def _filter_lines(lines, regexes):
    passed = []
    for rgx in regexes:
        rgx = rgx.replace('Decision', '')
        passed += [line for line in lines if re.search(rgx, line)]
    return passed


def main():
    failures = {wg: [] for wg in linedicts.keys()}

    lines = {wg: _extract_lines(dicts) for wg, dicts in linedicts.items()}
    for wg, lines in lines.items():
        turbo_lines, full_lines, all_lines = lines
        lines_left_out = [
            line for line in all_lines
            if (line not in turbo_lines and line not in full_lines)
        ]
        if lines_left_out:
            print(
                f"ERROR:: Some lines in {wg} all_lines are not in either full_lines or turbo_lines."
            )
            failures[wg] += lines_left_out
        else:
            print(
                f"All lines in {wg} all_lines exist in full_lines or turbo_lines"
            )

        regexes = turbo_streaming[wg]
        turbo_filter = lambda lines: _filter_lines(lines=lines, regexes=regexes)
        successful_turbo_lines, successful_full_lines = turbo_filter(
            turbo_lines), turbo_filter(full_lines)

        if successful_full_lines:
            print(
                f"ERROR:: Some {wg} full-stream lines pass the turbo regexes {regexes}."
            )
            failures[wg] += successful_full_lines
        else:
            print(
                f"No {wg} full-stream lines pass the turbo regexes {regexes}.")

        failed_turbo_lines = [
            line for line in turbo_lines if line not in successful_turbo_lines
        ]
        if failed_turbo_lines:
            print(
                f"ERROR:: Some {wg} turbo-stream lines fail the turbo regexes {regexes}."
            )
            failures[wg] += failed_turbo_lines
        else:
            print(
                f"All {wg} turbo-stream lines pass the turbo regex {regexes}.")

    has_failed = False
    for wg, failed_lines in failures.items():
        if failed_lines:
            print(
                f"\nERROR:: {wg} has the following {len(failed_lines)} failed lines:\n  {failed_lines}  \n"
            )
            has_failed = True
    if has_failed:
        raise RuntimeError("Some lines failed one of the line tests.\n")
    print("SUCCESS:: All lines passed the tests.")


main()
