Running HLT2 over MC with UT in the reconstruction
==================================================

Using the UT in the 2024 reconstruction is as simple as swapping the
``hlt2_reconstruction`` sequence from ``make_light_reco_pr_kf_without_UT`` to
``make_light_reco_pr_kf``. An example for running HLT2 in this configuration can
be found below:

.. literalinclude:: ../../Hlt/Hlt2Conf/options/hlt2_pp_thor_with_UT.py

If lots of the options here are unfamiliar to you, please review earlier
sections of the documentation.