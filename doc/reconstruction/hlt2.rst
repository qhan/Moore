HLT2 Reconstruction
===================

The HLT2 full event reconstruction consists of three major steps: the track reconstruction of charged
particles, the reconstruction of neutral particles, and particle identification (PID). The HLT2 track
reconstruction exploits the full information from the tracking sub-detectors, performing additional
steps of the pattern recognition which are not possible in HLT1 due to strict time constraints. As a
result high-quality long and downstream tracks are found with the most precise momentum estimate
achievable. Similarly, the most precise neutral cluster reconstruction algorithms are executed in
the HLT2 reconstruction. Finally, in addition to the muon identification available in HLT1, HLT2
exploits the full particle identification from the RICH detectors and calorimeter system.

The track reconstruction of charged particles
---------------------------------------------

The goal is to reconstruct all tracks without a minimal :math:`p_T` requirement. This is particularly
important for the study of the decays of lighter particles, such as charmed or strange hadrons, whose
abundance means that only some of the fully reconstructed and exclusively selected final states fit
into the available trigger bandwidth.

Often, not all of the decay products of a charm- or strange-hadron decay pass the harsh
requirements of HLT1, particularly for decays into three or more final-state particles.
Therefore, to efficiently trigger these decays, it is necessary to also reconstruct all stable daughter
tracks within the LHCb acceptance.

In a first step, the :doc:`track reconstruction of HLT1 <hlt1>` is repeated. A second step is then used to
reconstruct tracks which had not been found in HLT1 due to reconstruction requirements.
A similar procedure as in HLT1 is employed: the VELO tracks are extrapolated through the magnet
to the SciFi, where a wider search window than in HLT1 is defined. In addition, a standalone search
for tracks in the SciFi is performed, and these standalone tracks are then combined with VELO tracks to
form long tracks. The redundancy of the two long-track algorithms increases the efficiency by a few percent.

Tracks produced in the decays of long-lived particles like :math:`\Lambda` or :math:`K_S^0`
that predominantly decay outside the VELO acceptance are reconstructed using SciFi segments
that are extrapolated backwards through the magnetic field and combined with hits in the UT.

The next step in the reconstruction chain is a Kalman filter to obtain the optimal parameter estimate
and to determine the quality of a track candidate to reject fake tracks. These fakes result from
random combinations of hits or a mismatch of track segments upstream and downstream of the
magnet.

After the removal of fake tracks, the remaining tracks are filtered to remove so-called clones.
Clones can be created inside a single pattern-recognition algorithm or, more commonly, originate
from the redundancy in the pattern-recognition algorithms. Two tracks are defined as clones of
each other if they share enough hits in each subdetector. Only the subdetectors where both tracks
have hits are considered. The track with more hits in total is kept and the other is discarded. This
final list of tracks is subsequently used to select events.

A detailed documentation of the configuration in Moore can be found here: :doc:`Configuration of the track reconstruction in HLT2 <../recoconf/hlt2_tracking>`.

RICH reconstruction
-------------------

.. todo:: Please fill

Calorimeter reconstruction
--------------------------

.. todo:: Please fill

Differences and similarities to HLT1
------------------------------------

The main differences and similarities between the reconstruction in HLT1 and in HLT2 are listed.

- The algorithms (including their settings) to reconstruct primary vertices, VELO- and upstream tracks are the same in HLT1 and HLT2
- The forward track reconstruction in HLT1 uses :math:`p_T` filtered upstream tracks as input, while the forward tracking in HLT2 runs with all VELO tracks
- Muon identification is the same
- Due to timing constraints, HLT1 cannot run particle identification algorithms for hadrons, electrons and photons (RICH, Calorimeter)
- SciFi tracks, Downstream tracks and long tracks from track matching are also only available in HLT2
- A speed optimized fit of long tracks is performed in HLT1, while a offline quality track fit, as well as the removal of clone and the rejection of fake tracks is done in HLT2

Further reading
---------------

`Expression of Interest for an LHCb Upgrade <https://cds.cern.ch/record/1100545>`_

`Letter of Intent for the LHCb Upgrade <https://cds.cern.ch/record/1333091>`_

`Framework TDR for the LHCb Upgrade <https://cds.cern.ch/record/1443882>`_

`LHCb VELO Upgrade Technical Design Report  <https://cds.cern.ch/record/1624070>`_

`LHCb PID Upgrade Technical Design Report <https://cds.cern.ch/record/1624074>`_

`LHCb Tracker Upgrade Technical Design Report  <https://cds.cern.ch/record/1647400>`_

`LHCb Trigger and Online Upgrade Technical Design Report <https://cds.cern.ch/record/1701361>`_

`Upgrade Software and Computing <https://cds.cern.ch/record/2310827>`_

`Computing Model of the Upgrade LHCb experiment <https://cds.cern.ch/record/2319756>`_

`LHCb SMOG Upgrade <https://cds.cern.ch/record/2673690>`_
